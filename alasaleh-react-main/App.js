/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component, useState, useContext} from 'react';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import RNRestart from 'react-native-restart';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  NativeModules,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import Routes from './src/Utils/Routes';
import {createStackNavigator} from '@react-navigation/stack';
import {HomeContainer} from './src/Containers';
import {ScaledSheet} from 'react-native-size-matters';
import {headerColor} from './src/Theme/colors';
import {createDrawerNavigator} from '@react-navigation/drawer';
import 'react-native-gesture-handler';
import AppContext from './src/Utils/AppContext';
import Storage from './src/Utils/storage';
console.disableYellowBox = true;

const Drawer = createDrawerNavigator();

const App = () => {
  const [setting2value, setSetting2value] = useState(false);
  const toggleSetting2 = () => {
    Storage.storeData('langv', setting2value);
    Storage.retrieveData('langv').then((res) => {
      setSetting2value(!res);
    });
  };
  const userSettings = {
    setting2name: setting2value,
    toggleSetting2,
  };
  return (
    <SafeAreaView style={styles.container}>
      <AppContext.Provider value={userSettings}>
        <StatusBar backgroundColor={headerColor} />
        <NavigationContainer>
          <Routes />
        </NavigationContainer>
      </AppContext.Provider>
    </SafeAreaView>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    margin: 0,
    padding: 0,
  },
});

export default App;
