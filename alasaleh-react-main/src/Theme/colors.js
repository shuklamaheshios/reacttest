const headerColor = '#1B2224';
const whiteColor = '#fff';
const greenColor = '#009760';
const blackColor = '#222';
const lightGray = '#BEBEBE';
const redColor = '#DC3545';
const blueColor = '#17A2B8';
const parrotColor = '#4EA41A';
const grayColor = '#E0E0E0';

export {
  headerColor,
  whiteColor,
  greenColor,
  blackColor,
  lightGray,
  redColor,
  blueColor,
  parrotColor,
  grayColor,
};
