import React, {useState, useContext} from 'react';
import {Text, View, TouchableOpacity, ScrollView, Modal} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import {ScaledSheet} from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {DrawerHeader, MainHeader} from '../Components';
import {blackColor, greenColor, whiteColor} from '../Theme/colors';
import AppContext from '../Utils/AppContext';
import Storage from '../Utils/storage';
function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const CreditCardScreen = ({
  handleChange,
  genToken,
  details,
  navigate,
  goBack,
  SaveOrder,
}) => {
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };

  const [price, setprice] = useState('');
  const [modalVisible, setModalVisible] = useState(false);
  Storage.retrieveData('price').then((resp) => {
    setprice(resp);
  });

  return (
    <View style={styles.container}>
      {/* <DrawerHeader
        goBack={goBack}
        title={myContext.setting2name ? 'بطاقة ائتمان' : 'Credit Card'}
      /> */}
      <MainHeader home={false} navigate={navigate} />
      <Modal
        visible={modalVisible}
        transparent={true}
        style={{height: 50, width: 50, padding: 0}}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>
              {myContext.setting2name ? 'تم التسديد' : 'Payment Completed'}
            </Text>

            <Text style={styles.modaldescription}>
              {myContext.setting2name
                ? 'يرجى التحقق من عنوان بريدك الإلكتروني الذي أرسلناه إليك'
                : 'Please check your email address we have sent you invoice'}
            </Text>

            <TouchableOpacity onPress={() => genToken()}>
              <Text style={styles.okText}>OK</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
      <ScrollView style={{flex: 1}}>
        <View style={styles.headingContainer}>
          <Text style={styles.totalText}>
            {myContext.setting2name
              ? `إجمالي الفاتورة ${price} دولارًا`
              : `Total Bill $ ${price}`}
          </Text>
        </View>
        <View
          style={{
            flexDirection: `${myContext.setting2name ? 'row-reverse' : 'row'}`,
            alignItems: 'center',
            padding: 15,
            marginTop: 50,
          }}>
          <View editable={false} style={styles.cardInputIcon}>
            <Icon name="credit-card" size={22} color={blackColor} />
          </View>
          <TextInput
            style={styles.cardInput}
            placeholder="Card Number"
            onChangeText={(value) =>
              handleChange('cardNumber', value)
            }></TextInput>
        </View>

        <View style={styles.bottomInputContainer}>
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <TextInput
              style={styles.inputBottom}
              placeholder={
                myContext.setting2name ? 'شهر انتهاء الصلاحية' : 'Expiry Month'
              }
              onChangeText={(value) => handleChange('expM', value)}
            />
            <TextInput
              style={styles.inputBottom}
              placeholder={
                myContext.setting2name ? 'سنة انتهاء الصلاحية' : 'Expiry Year'
              }
              onChangeText={(value) => handleChange('expY', value)}
            />
          </View>

          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <TextInput
              style={styles.inputBottom}
              placeholder="CVC"
              onChangeText={(value) => handleChange('cvc', value)}
            />
            {/* <TextInput
              style={styles.inputBottom}
              placeholder={
                myContext.setting2name ? 'الرمز البريدي' : 'Postal Code'
              }
              onChangeText={(value) => handleChange('postal', value)}
            /> */}
          </View>
        </View>

        <View style={styles.nextBtnContainer}>
          <TouchableOpacity
            style={styles.nextBtn}
            onPress={(() => setModalVisible(true), SaveOrder())}>
            <Text style={styles.nextText}>Proceed</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => goBack()}
            style={styles.otherContainer}>
            <Icon name="arrow-left" size={20} />
            <Text style={{marginLeft: 20}}>
              {myContext.setting2name
                ? 'استخدام مهتود إكمال الشراء الأخرى'
                : 'Use Other Payment Mehtod'}
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
  headingContainer: {
    marginTop: '10@s',
  },
  totalText: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: '17@s',
  },
  cardInputIcon: {
    width: '10%',
    borderBottomColor: blackColor,
    borderBottomWidth: 1,
    textAlignVertical: 'center',
    height: '42@s',
    justifyContent: 'center',
  },
  cardInput: {
    flexDirection: 'row',
    borderBottomColor: blackColor,
    borderBottomWidth: 1,
    width: '90%',
    fontSize: '17@s',
  },

  cardInputIconView: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  bottomInputContainer: {
    justifyContent: 'space-between',
    padding: '15@s',
  },

  inputBottom: {
    borderBottomColor: blackColor,
    borderBottomWidth: 1,
    width: '48.33337%',
    fontSize: '16@s',
    paddingBottom: '2@s',
  },
  nextBtnContainer: {
    padding: '35@s',
  },
  nextBtn: {
    backgroundColor: greenColor,
    alignItems: 'center',
    padding: '10@s',
    borderRadius: '5@s',
  },
  nextText: {
    color: whiteColor,
    fontSize: '14@s',
  },

  otherContainer: {
    padding: '20@s',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '20@s',
  },

  modalView: {
    backgroundColor: 'white',
    borderRadius: 2,
    padding: 20,
    shadowColor: '#000',
    width: '90%',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },

  modalText: {
    marginBottom: 15,
    color: blackColor,
    fontSize: '16@s',
    fontWeight: 'bold',
  },
  modaldescription: {
    fontSize: '14@s',
    fontWeight: 'normal',
    color: blackColor,
  },

  okText: {
    color: greenColor,
    fontSize: '16@s',
    textAlign: 'right',
  },
});

export default CreditCardScreen;
