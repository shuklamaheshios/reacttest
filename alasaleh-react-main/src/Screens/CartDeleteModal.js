import React, {useState, useContext} from 'react';
import {
  Alert,
  Image,
  Modal,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
} from 'react-native';
import Toast from 'react-native-simple-toast';
import Storage from '../Utils/storage';
import config from '../Utils/config';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {ScaledSheet} from 'react-native-size-matters';
import AppContext from '../Utils/AppContext';
import {blackColor, greenColor, lightGray, whiteColor} from '../Theme/colors';
import maxum from '../Assets/images/maxum.png';

const CartDeleteModal = ({
  navigate,
  modalVisible,
  setModalVisible,
  img,
  item,
  cartToken,
}) => {
  // const [modalVisible, setModalVisible] = useState(false);
  const myContext = useContext(AppContext);
  // console.log("The Car Delete ois: ", item)

  // let token;
  // await Storage.retrieveData('cartData').then((resp) => {
  //   token = resp;
  // });
  // console.log('the cart token: ', cartToken);
  const delete_product = async () => {
    setModalVisible(!modalVisible);
    await fetch(`${config.apiUrl}/carts/remove_item_from_cart`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        cartKey: cartToken.cartKey,
        productID: item.productID,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('Remove', responseJson);
        Toast.show(responseJson.message);
        navigate('Home');

        // responseJson['item_in_cart'].map(item => this.getProducts(item.Name))
      })
      .catch((e) => {
        console.log('error is', e);
      });
  };
  return (
    <View>
      <Modal animationType="slide" transparent={true} visible={modalVisible}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View
              style={
                myContext.setting2name ? styles.headerArabic : styles.header
              }>
              {/* <Image source={maxum} style={styles.image} /> */}
              <Image
                style={styles.image}
                source={{
                  uri: `${config.url}/public/images/product/${img}`,
                }}
              />
              <Text numberOfLines={2} style={styles.heeading}>
                {myContext.setting2name ? item?.name_arabic : item?.Name}
              </Text>
            </View>

            <TouchableOpacity style={styles.removeBtn} onPress={delete_product}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}>
                <Text style={styles.textStyle}>
                  {myContext.setting2name ? 'إزالة' : 'Remove'}
                </Text>
                <View>
                  <Icon name="chevron-right" />
                </View>
              </View>

              <View style={styles.divider}></View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = ScaledSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    padding: 20,
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  modalView: {
    backgroundColor: 'white',
    // padding: 35,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: blackColor,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },

  header: {
    backgroundColor: greenColor,
    padding: '10@s',
    flexDirection: 'row',
    alignItems: 'center',
  },

  headerArabic: {
    backgroundColor: greenColor,
    padding: '10@s',
    flexDirection: 'row-reverse',
    alignItems: 'center',
  },
  image: {
    height: '50@s',
    width: '50@s',
    borderRadius: '100@s',
  },
  heeading: {
    color: whiteColor,
    fontSize: '14@s',
    marginLeft: '10@s',
    width: '75%',
  },

  removeBtn: {
    padding: '10@s',
  },

  divider: {
    borderTopColor: lightGray,
    borderTopWidth: 1,
    width: '100%',
    marginTop: '7@s',
    marginBottom: '7@s',
  },
});

export default CartDeleteModal;
