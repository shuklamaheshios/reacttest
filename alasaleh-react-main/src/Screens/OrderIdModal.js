import React, {useState} from 'react';
import {View, Modal, Text, FlatList, TouchableOpacity} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {
  DrawerHeader,
  MyOrderCard,
  OrderDetailsCard,
  OrderIdCard,
} from '../Components';
import {blackColor, grayColor, greenColor, whiteColor} from '../Theme/colors';
import WhyOrderCancelModal from './WhyOrderCancelModal';

const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    reference_no: 'Aljazera ',
    shipping_cost: 126,
    total_tax: 12,
    coupon_discount: 100,
    total_price: 1230,
    updated_at: '20/10/2020',
  },
];

const OrderDetailsModal = ({modalVisible, setModalVisible, list}) => {
  const renderItem = ({item}) => <OrderIdCard list={item} />;
  const [whyCancelModal, setWhyCancelModal] = useState(false);
  return (
    <View style={styles.centeredView}>
      <WhyOrderCancelModal
        modalVisible={whyCancelModal}
        setModalVisible={setWhyCancelModal}
        otherMOdal={setModalVisible}
        list={list}
      />
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
        }}>
        <View style={styles.modalViewMain}>
          <View style={styles.modalView}>
            <View style={styles.orderDetailsHeader}>
              <Text
                style={{color: whiteColor, fontWeight: 'bold', fontSize: 18}}>
                {list.reference_no}
              </Text>
            </View>

            <View style={{padding: 10}}>
              <OrderIdCard
                cancelModal={setWhyCancelModal}
                setModalVisible={setModalVisible}
                list={list}
              />
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = ScaledSheet.create({
  centeredView: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  orderDetailsHeader: {
    backgroundColor: greenColor,
    padding: '10@s',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  modalViewMain: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalView: {
    // flex: 1,
    margin: 20,
    backgroundColor: whiteColor,
    padding: 0,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
});

export default OrderDetailsModal;
