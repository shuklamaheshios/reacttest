import React, {useContext, useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {DrawerHeader, MainHeader} from '../Components';
import {blackColor} from '../Theme/colors';

import AppContext from '../Utils/AppContext';

function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const OrderListScreen = ({goBack, navigate}) => {
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };
  return (
    <View style={styles.container}>
      {/* <DrawerHeader
        title={myContext.setting2name ? 'لائحة الطلبات' : 'Order List'}
        goBack={goBack}
      /> */}

      <MainHeader navigate={navigate} />
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Icon name="frown" size={30} color={blackColor} />
        <TouchableOpacity>
          <Text style={styles.textColor}>
            {myContext.setting2name
              ? 'لا توجد بيانات للعرض'
              : 'no data to show'}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },

  textColor: {
    color: blackColor,
    fontSize: '13@s',
    marginTop: '10@s',
    textAlign: 'center',
  },

  loginText: {
    color: blackColor,
    fontSize: '13@s',
    textAlign: 'center',
  },
});

export default OrderListScreen;
