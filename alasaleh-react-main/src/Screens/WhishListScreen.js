import React, {useState, useContext} from 'react';
import {View, Text, TouchableOpacity, FlatList} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {DrawerHeader, MainHeader, WhishListCard} from '../Components';
import {blackColor} from '../Theme/colors';
import AppContext from '../Utils/AppContext';
import axios from 'axios';
import Storage from '../Utils/storage';
import config from '../Utils/config';
import Toast from 'react-native-simple-toast';
const DATA = [
  {
    id: '1',
    title: 'Salman',
  },
  {
    id: '2',
    title: 'Salman',
  },
  {
    id: '3',
    title: 'Salman',
  },
  {
    id: '4',
    title: 'Salman',
  },
  {
    id: '5',
    title: 'Salman',
  },
];

function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const WhishListScreen = ({goBack, navigate, list, removeWish}) => {
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };
  let token = null;
  let user = null;
  const addtocart = async (item) => {
    let pr = 0;
    let cartDet = null;
    await Storage.retrieveData('cartDataDetails').then((resp) => {
      cartDet = resp;
    });
    if (cartDet) {
      cartDet.push(item);
      Storage.storeData('cartDataDetails', cartDet);
    } else {
      Storage.storeData('cartDataDetails', [cartDet]);
    }
    await Storage.retrieveData('cartData').then((resp) => {
      token = resp;
    });
    await Storage.retrieveData('user').then((resp) => {
      user = resp;
    });

    if (token === null) {
      await fetch(`${config.apiUrl}/carts`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      })
        .then((response) => response.json())
        .then((responseJson) => {
          Storage.storeData('cartData', responseJson);
          token = responseJson;
        });
    }
    if (item.promotion === '1' || item.promotion === 1) {
      pr = parseFloat(item.promotion_price).toFixed(2);
    } else {
      const k = (parseFloat(item.price) * parseFloat(item.discount)) / 100;
      const calculated = item.price - k;
      let p = calculated.toFixed(2);
      if (p > calculated) {
        p = p - 0.01;
      }
      pr = p;
    }
    let toCount = 0;
    await fetch(
      `${config.apiUrl}/carts/show_cart_data/${token.cartToken}`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          cartKey: token.cartKey,
        }),
      },
    )
      .then((response) => response.json())
      .then((responseJson) => {
        responseJson['item_in_cart'].map((cartItem) => {
          if (cartItem.productID === item.id) {
            toCount = parseInt(cartItem.Quantity);
            console.log('toCount.id', cartItem?.Quantity);
          }
        });

        console.log('cart', responseJson['item_in_cart']);
        // responseJson['item_in_cart'].map(item => this.getProducts(item.Name))
      })
      .catch((e) => {
        console.log('error is', e);
        this.setState({
          loader: false,
        });
      });
    let qti = 1;
    if (toCount !== 0) {
      qti = toCount + 1;
    }
    await fetch(
      `${config.apiUrl}/carts/add_to_cart/${token.cartToken}`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          cartKey: token.cartKey,
          productID: item.id,
          quantity: qti.toString(),
          price: pr,
          image: item.image,
        }),
      },
    )
      .then((response) => response.json())
      .then((res) => {
        Toast.show(res.message);
      });
  };
  const renderItem = ({item}) => (
    <WhishListCard
      title={myContext.setting2name ? item.name_arabic : item.name}
      price={item.price}
      id={item.id}
      removeWish={removeWish}
      img={item.image}
      qty={item.qty}
      addtocart={addtocart}
      product={item}
    />
  );
  return (
    <View style={styles.container}>
      {/* <DrawerHeader
        title={myContext.setting2name ? 'قائمة الرغبات' : 'Whis List'}
        goBack={goBack}
      /> */}
      <MainHeader home={false} navigate={navigate} />
      {list.length === 0 ? (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Icon name="frown" size={30} color={blackColor} />
          <Text style={styles.textColor}>
            {myContext.setting2name
              ? 'يرجى تسجيل الدخول إلى حسابك'
              : 'Please Login To Your Account'}
          </Text>
          <TouchableOpacity onPress={() => navigate('Login')}>
            <Text style={styles.textColor}>
              {myContext.setting2name
                ? 'انقر هنا لتسجيل الدخول'
                : 'Click Here To Login'}
            </Text>
          </TouchableOpacity>
        </View>
      ) : (
        <View style={{flex: 1}}>
          <FlatList data={list} renderItem={renderItem} />
        </View>
      )}
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },

  textColor: {
    color: blackColor,
    fontSize: '13@s',
    marginTop: '10@s',
    textAlign: 'center',
  },

  loginText: {
    color: blackColor,
    fontSize: '13@s',
    textAlign: 'center',
  },
});

export default WhishListScreen;
