import React, {useState, useContext} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {DrawerHeader, MainHeader} from '../Components';
import {blackColor} from '../Theme/colors';
import AppContext from '../Utils/AppContext';

function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const SavedScreen = ({goBack, navigate}) => {
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };
  return (
    <View style={styles.container}>
      {/* <DrawerHeader
        title={myContext.setting2name ? 'تم الحفظ' : 'Saved'}
        goBack={goBack}
      /> */}
      <MainHeader navigate={navigate} />
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Icon name="frown" size={30} color={blackColor} />
        <TouchableOpacity onPress={() => navigate('Login')}>
          <Text style={styles.textColor}>
            {myContext.setting2name
              ? 'الرجاء تسجيل الدخول إلى حسابك'
              : 'Please Login your Account'}
          </Text>
          <Text style={styles.loginText}>
            {myContext.setting2name
              ? 'انقر هنا لتسجيل الدخول'
              : 'Click Here to Login'}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },

  textColor: {
    color: blackColor,
    fontSize: '13@s',
    marginTop: '10@s',
    textAlign: 'center',
  },

  loginText: {
    color: blackColor,
    fontSize: '13@s',
    textAlign: 'center',
  },
});

export default SavedScreen;
