import React, {useContext, useState} from 'react';
import {Image, Text, View, TouchableOpacity, Button} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import RadioButton from 'react-native-radio-button';
import payPal from '../Assets/images/paypal_icon.png';
import cards_icon from '../Assets/images/secure-stripe-payment.png';
import {DrawerHeader, MainHeader} from '../Components';
import {blackColor, greenColor, redColor, whiteColor} from '../Theme/colors';
import AppContext from '../Utils/AppContext';
import Storage from '../Utils/storage';
import {set} from 'react-native-reanimated';

function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const PaymentMethodScreen = ({
  goBack,
  navigate,
  details,
  changeRadio,
  setStripe,
  checkPoints,
  SaveOrder,
}) => {
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };

  const [value, setValue] = useState(0);
  const [fcheck, setfcheck] = useState(true);
  const [scheck, setscheck] = useState(false);
  const [tcheck, settcheck] = useState(false);
  // if(fcheck){
  //   getPaypal()
  // }
  // else if(scheck){
  //   getCreditCard()
  // }
  const radiocheck = () => {
    setfcheck(true);
    setscheck(false);
    settcheck(false);
    changeRadio('paypal');
  };
  const radiocheck1 = () => {
    setscheck(true);
    setfcheck(false);
    settcheck(false);
    changeRadio('credit');
  };
  const radiocheck2 = () => {
    settcheck(true);
    setscheck(false);
    setfcheck(false);
    changeRadio('points');
  };
  const navstore = () => {
    Storage.storeData('billing', details);
    if (tcheck) {
      SaveOrder(myContext.setting2name);
    } else if (scheck) {
      navigate('StripeCard', details);
    } else {
      navigate('PaypalPayment', details);
    }
  };
  return (
    <View style={styles.container}>
      <MainHeader home={false} navigate={navigate} />
      <View style={styles.body}>
        <Text style={styles.paymentHeading}>
          {' '}
          {myContext.setting2name
            ? 'حدد نوع إكمال الشراء المفضل لديك'
            : 'Select Payment Method'}
        </Text>

        <View style={
            myContext.setting2name
              ? styles.radioContainerUrdu
              : styles.radioContainer
          }>
          <View>
            <RadioButton
              animation={'bounceIn'}
              isSelected={fcheck}
              innerColor={greenColor}
              outerColor={greenColor}
              onPress={radiocheck}
              size={16}
            />
          </View>

          <View
            style={
              myContext.setting2name
                ? styles.textContainerUrdu
                : styles.textContainer
            }>
            <Text style={styles.paymentText}>
              {myContext.setting2name
                ? 'PayPal أو الفاتورة أو الخصم المباشر أو بطاقة الائتمان'
                : 'PayPal, Invoice, direct debit or credit card'}
            </Text>
            <Image source={payPal} style={styles.image} />
          </View>
        </View>
        <View
          style={
            myContext.setting2name
              ? styles.radioContainerUrdu
              : styles.radioContainer
          }>
          <View>
            <RadioButton
              animation={'bounceIn'}
              isSelected={scheck}
              innerColor={greenColor}
              outerColor={greenColor}
              onPress={radiocheck1}
              size={16}
            />
          </View>

          <View
            style={
              myContext.setting2name
                ? styles.textContainerUrdu
                : styles.textContainer
            }>
            <Text style={styles.paymentText}>
              {myContext.setting2name ? 'بطاقة ائتمان' : 'Credit Card'}
            </Text>
            <Image source={cards_icon} style={styles.image} />
          </View>
        </View>

        {checkPoints ? (
          <View
            style={
              myContext.setting2name
                ? styles.radioContainerUrdu
                : styles.radioContainer
            }>
            <RadioButton
              animation={'bounceIn'}
              isSelected={tcheck}
              innerColor={greenColor}
              outerColor={greenColor}
              onPress={radiocheck2}
              size={16}
            />

            <View
              style={
                myContext.setting2name
                  ? styles.textContainerUrdu
                  : styles.textContainer
              }>
              <Text style={styles.paymentText}>
                {myContext.setting2name
                  ? 'بطاقة ائتمان'
                  : ' Pay Through Refferal Points'}
              </Text>
            </View>
          </View>
        ) : null}

        <View style={styles.nextBtnContainer}>
          <TouchableOpacity onPress={() => navstore()} style={styles.nextBtn}>
            <Text style={styles.nextText}>
              {myContext.setting2name ? 'احفظ التغييرات' : 'Next'}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
  body: {
    padding: '10@s',
  },
  paymentHeading: {
    fontSize: '14@s',
    color: '#333',
    fontWeight: 'bold',
    marginBottom: '20@s',
    textAlign: 'center',
  },

  radioContainer: {
    padding: '15@s',
    flexDirection: 'row',
    justifyContent: 'flex-start',

    borderColor: blackColor,
    borderWidth: 1,
  },

  radioContainerUrdu: {
    padding: '15@s',
    flexDirection: 'row-reverse',
    justifyContent: 'flex-start',

    borderColor: blackColor,
    borderWidth: 1,
  },
  textContainer: {
    marginLeft: '20@s',
    height: '80@s',
    justifyContent: 'space-between',
  },

  textContainerUrdu: {
    marginRight: '20@s',
    height: '80@s',
    justifyContent: 'space-between',
  },

  paymentText: {
    fontSize: '13@s',
    color: blackColor,
  },

  image: {
    height: '100@s',
    width: '200@s',
    padding: '10@s',
    resizeMode: 'center',
  },

  nextBtnContainer: {
    padding: '25@s',
  },
  nextBtn: {
    backgroundColor: greenColor,
    alignItems: 'center',
    padding: '10@s',
    borderRadius: '5@s',
  },
  nextText: {
    color: whiteColor,
    fontSize: '14@s',
  },
});

export default PaymentMethodScreen;
