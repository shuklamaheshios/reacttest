import React, {useState} from 'react';
import {View, FlatList} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {DrawerHeader, MainHeader, MyOrderCard} from '../Components';
import {redColor, whiteColor} from '../Theme/colors';
import WhyOrderCancelModal from './WhyOrderCancelModal';

const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'First Item',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'Second Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'Third Item',
  },
  {
    id: '1',
    title: 'Third Item',
  },
  {
    id: '586',
    title: 'Third Item',
  },
];

const MyOrderScreen = ({goBack, navigate, orederList}) => {
  const renderItem = ({item}) => <MyOrderCard list={item} />;

  return (
    <View style={styles.container}>
      <MainHeader navigate={navigate} />
      <View style={styles.body}>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={orederList}
          renderItem={renderItem}
        />
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
  body: {
    padding: '5@s',
    flex: 1,
  },
});

export default MyOrderScreen;
