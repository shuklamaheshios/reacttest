import React, {useContext, useState} from 'react';
import {
  View,
  Modal,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  TextInput,
  ScrollView,
} from 'react-native';
import RadioButton from 'react-native-radio-button';
import AppContext from '../Utils/AppContext';
import {ScaledSheet} from 'react-native-size-matters';
import {blackColor, greenColor, whiteColor} from '../Theme/colors';
import axios from 'axios';
import Storage from '../Utils/storage';
import config from '../Utils/config';
const WhyOrderCancelModal = ({
  modalVisible,
  setModalVisible,
  otherMOdal,
  list,
}) => {
  const myContext = useContext(AppContext);
  const [fcheck, setfcheck] = useState(false);
  const [scheck, setscheck] = useState(false);
  const [tcheck, settcheck] = useState(false);
  const [ccheck, setccheck] = useState(false);
  const [describeText, setDescribeText] = useState('xyz');

  // const [modalVisible, setModalVisible] = useState(true);

  const radiocheck = () => {
    setfcheck(true);
    setscheck(false);
    settcheck(false);
    setccheck(false);
    // changeRadio('paypal');
  };
  const radiocheck1 = () => {
    setscheck(true);
    setfcheck(false);
    settcheck(false);
    setccheck(false);
    // changeRadio('credit');
  };
  const radiocheck2 = () => {
    settcheck(true);
    setscheck(false);
    setfcheck(false);
    setccheck(false);
    // changeRadio('points');
  };

  const radiocheck3 = () => {
    setccheck(true);
    settcheck(false);
    setscheck(false);
    setfcheck(false);

    // changeRadio('points');
  };

  const cancelOrder = () => {
    Storage.retrieveData('user').then((resp) => {
      // const params = {
      //   order_id: list.id,
      // };

      let reason = 'other';
      if (scheck) {
        reason = 'A cheaper alternative is available for a less price';
      } else if (fcheck) {
        reason = 'Product is not required any more, i changed my mind';
      } else if (tcheck) {
        reason =
          'The price of the product has fallen due to coupon/discount and i want to get it at a lesser coupon/discount price';
      } else if (ccheck) {
        reason =
          'A bad review from friends/relatives aftering ordering the product';
      }
      const params = {
        order_id: list.id,
        cancel_reason: reason,
        cancel_note: describeText,
      };
      axios
        .post(
          `${config.apiUrl}/send_order_cancel_request`,
          params,
          {
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              Authorization: `Bearer ${resp.token}`,
            },
          },
        )
        // .post(`${config.apiUrl}/send_order_cancel_request`, params)
        .then((res) => {
          // setOrderDetails(res.data.data);
          console.log(res.data);
        })
        .catch((error) => {
          console.error(error);
        });
    });
  };
  return (
    <View style={styles.centeredView}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <ScrollView>
              <View style={styles.header}>
                <Text style={styles.modalText}>Why Order cancel</Text>
              </View>
              <View
                style={
                  myContext.setting2name
                    ? styles.radioContainerUrdu
                    : styles.radioContainer
                }>
                <View>
                  <RadioButton
                    animation={'bounceIn'}
                    isSelected={fcheck}
                    innerColor={greenColor}
                    outerColor={greenColor}
                    onPress={radiocheck}
                    size={16}
                  />
                </View>

                <View
                  style={
                    myContext.setting2name
                      ? styles.textContainerUrdu
                      : styles.textContainer
                  }>
                  <Text style={styles.paymentText}>
                    {myContext.setting2name
                      ? 'لم يعد المنتج مطلوبًا ، لقد غيرت رأيي'
                      : 'Product is not required any more, i changed my mind'}
                  </Text>
                  {/* <Image source={payPal} style={styles.image} /> */}
                </View>
              </View>

              <View
                style={
                  myContext.setting2name
                    ? styles.radioContainerUrdu
                    : styles.radioContainer
                }>
                <View>
                  <RadioButton
                    animation={'bounceIn'}
                    isSelected={scheck}
                    innerColor={greenColor}
                    outerColor={greenColor}
                    onPress={radiocheck1}
                    size={16}
                  />
                </View>

                <View
                  style={
                    myContext.setting2name
                      ? styles.textContainerUrdu
                      : styles.textContainer
                  }>
                  <Text style={styles.paymentText}>
                    {myContext.setting2name
                      ? 'يتوفر بديل أرخص بسعر أقل'
                      : 'A cheaper alternative is available for a less price'}
                  </Text>
                  {/* <Image source={cards_icon} style={styles.image} /> */}
                </View>
              </View>

              <View
                style={
                  myContext.setting2name
                    ? styles.radioContainerUrdu
                    : styles.radioContainer
                }>
                <View>
                  <RadioButton
                    animation={'bounceIn'}
                    isSelected={tcheck}
                    innerColor={greenColor}
                    outerColor={greenColor}
                    onPress={radiocheck2}
                    size={16}
                  />
                </View>

                <View
                  style={
                    myContext.setting2name
                      ? styles.textContainerUrdu
                      : styles.textContainer
                  }>
                  <Text style={styles.paymentText}>
                    {myContext.setting2name
                      ? 'انخفض سعر المنتج بسبب القسيمة / الخصم وأرغب في الحصول عليه بسعر كوبون / خصم أقل'
                      : 'The price of the product has fallen due to coupon/discount and i want to get it at a lesser coupon/discount price'}
                  </Text>
                  {/* <Image source={cards_icon} style={styles.image} /> */}
                </View>
              </View>

              <View
                style={
                  myContext.setting2name
                    ? styles.radioContainerUrdu
                    : styles.radioContainer
                }>
                <View>
                  <RadioButton
                    animation={'bounceIn'}
                    isSelected={ccheck}
                    innerColor={greenColor}
                    outerColor={greenColor}
                    onPress={radiocheck3}
                    size={16}
                  />
                </View>

                <View
                  style={
                    myContext.setting2name
                      ? styles.textContainerUrdu
                      : styles.textContainer
                  }>
                  <Text style={styles.paymentText}>
                    {myContext.setting2name
                      ? 'مراجعة سيئة من الأصدقاء / الأقارب بعد طلب المنتج'
                      : 'A bad review from friends/relatives aftering ordering the product'}
                  </Text>
                  {/* <Image source={cards_icon} style={styles.image} /> */}
                </View>
              </View>
              <View style={styles.textAreaContainer}>
                <Text style={styles.otherText}>
                  Other, please describe your reason in few words
                </Text>

                <TextInput
                  onChangeText={(value) => setDescribeText(value)}
                  style={styles.input}
                />
              </View>

              <View style={styles.nextBtnContainer}>
                <TouchableOpacity
                  onPress={() => {
                    setModalVisible(!modalVisible),
                      otherMOdal(false),
                      cancelOrder();
                  }}
                  style={styles.nextBtn}>
                  <Text style={styles.nextText}>Submit</Text>
                </TouchableOpacity>
              </View>
            </ScrollView>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = ScaledSheet.create({
  centeredView: {
    justifyContent: 'center',
  },

  header: {
    borderBottomColor: blackColor,
    borderBottomWidth: '0.2@s',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    fontSize: '16@s',
    // lineHeight: '18@s',
    marginLeft: '15@s',
  },

  // Radio Button
  radioContainer: {
    padding: '15@s',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },

  radioContainerUrdu: {
    padding: '15@s',
    flexDirection: 'row-reverse',
    justifyContent: 'flex-start',
  },
  paymentText: {
    fontSize: '14@s',
    color: blackColor,
    marginLeft: '10@s',
  },

  textContainerUrdu: {
    marginRight: '20@s',
    height: '80@s',
    justifyContent: 'space-between',
  },

  textAreaContainer: {
    padding: '10@s',
  },

  otherText: {
    fontSize: '12@s',
    fontWeight: 'bold',
  },

  input: {
    height: '100@s',
    borderColor: blackColor,
    borderWidth: 1,
    textAlignVertical: 'top',
    color: blackColor,
    fontSize: '12@s',
    marginTop: '5@s',
  },

  nextBtnContainer: {
    padding: '13@s',
    width: '70%',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  nextBtn: {
    backgroundColor: greenColor,
    alignItems: 'center',
    padding: '10@s',
    borderRadius: '5@s',
  },
  nextText: {
    color: whiteColor,
    fontSize: '14@s',
  },
});

export default WhyOrderCancelModal;
