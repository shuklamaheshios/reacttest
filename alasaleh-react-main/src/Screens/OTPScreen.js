import React, {useContext, useState} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import Timer from 'react-compound-timer';
import {EmptyHeader, Input, MainHeader} from '../Components';
import {
  blackColor,
  blueColor,
  greenColor,
  redColor,
  whiteColor,
} from '../Theme/colors';
import AppContext from '../Utils/AppContext';

function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const OTPScreen = ({goBack, handleChange, signIn, navigate}) => {
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };
  return (
    <View style={styles.container}>
      {/* <EmptyHeader goBack={goBack} /> */}
      <MainHeader navigate={navigate} />
      <View style={styles.body}>
        <View style={styles.headingContainer}>
          <Text style={styles.headingText}>
            {myContext.setting2name ? 'أدخل كود OPT' : 'Enter OPT Code'}
          </Text>
        </View>
        <View style={styles.descriptionContainer}>
          <Text style={styles.descriptionText}>
            {myContext.setting2name
              ? 'لقد أرسلنا رمز OPT على البريد الإلكتروني الخاص بك. الرجاء التحقق من ذلك. شكر'
              : 'We have send OPT code at your given email. please verify it. Thanks'}
          </Text>
          <Text style={styles.descriptionText}>i.e : example@example.com</Text>
        </View>

        <View>
          <Input
            label="Enter OPT verification Code"
            placeHolder="0000000"
            type="number"
            onChangeText={handleChange}
            name="otp"
          />
        </View>

        <View>
          <Timer
            initialTime={40 * 1000}
            direction="backward"
            timeToUpdate={10}
            // checkpoints={[
            //   {
            //     time: 0,

            //   },
            // ]}
          >
            <View style={styles.timerContainer}>
              <Text style={styles.resendtext}>
                {myContext.setting2name ? 'إعادة إرسال' : 'Resend'}
              </Text>
              <Text style={styles.resendtext}>00 :</Text>
              <Text style={{justifyContent: 'flex-end', textAlign: 'right'}}>
                <Timer.Seconds />
              </Text>
            </View>
          </Timer>
        </View>
        <View style={styles.nextBtnContainer}>
          <TouchableOpacity style={styles.nextBtn} onPress={() => signIn()}>
            <Text style={styles.nextText}>
              {myContext.setting2name
                ? 'انشاء حساب جديد'
                : 'CREATE NEW ACCOUNT'}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },

  body: {},

  headingContainer: {
    padding: '15@s',
  },

  headingText: {
    fontSize: '18@s',
    color: blackColor,
    fontWeight: 'bold',
  },

  descriptionContainer: {
    padding: '20@s',
  },

  descriptionText: {
    color: blueColor,
  },

  nextBtnContainer: {
    padding: '13@s',
  },

  nextBtn: {
    backgroundColor: greenColor,
    alignItems: 'center',
    padding: '10@s',
    borderRadius: '5@s',
  },
  nextText: {
    color: whiteColor,
  },

  timerContainer: {
    padding: '15@s',
    justifyContent: 'flex-end',
    flexDirection: 'row',
  },

  resendtext: {
    marginRight: '6@s',
    color: blackColor,
  },
});

export default OTPScreen;
