import React, {useState, useContext} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import TextBox from 'react-native-password-eye';
import {DrawerHeader, MainHeader} from '../Components';
import {
  blackColor,
  greenColor,
  lightGray,
  redColor,
  whiteColor,
} from '../Theme/colors';
import {ScrollView} from 'react-native-gesture-handler';
import AppContext from '../Utils/AppContext';
function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const UpdatePasswordScreen = ({
  goBack,
  navigate,
  handleChange,
  updatePassword,
}) => {
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };
  return (
    <View style={styles.container}>
      <MainHeader home={false} navigate={navigate} />
      <ScrollView>
        <View style={{marginBottom: 10, marginTop: 10}}>
          <Text style={styles.changePasswordHeading}>Change Password*</Text>
        </View>
        <View>
          <Text style={styles.inputLabel}>
            {myContext.setting2name ? 'كلمة السر الحالية' : 'Current Password*'}
          </Text>
          <TextBox
            placeholder="Enter Current Password"
            onChangeText={(text) => handleChange('old', text)}
            containerStyles={styles.customeInput}
            inputStyle={styles.inputStyle}
            placeholderTextColor="#333333"
            secureTextEntry={true}></TextBox>
        </View>

        <View>
          <Text style={styles.inputLabel}>
            {myContext.setting2name ? 'كلمة مرور جديدة*' : 'New Password*'}
          </Text>
          <TextBox
            placeholder="Enter New Password"
            onChangeText={(text) => handleChange('new', text)}
            containerStyles={styles.customeInput}
            inputStyle={styles.inputStyle}
            placeholderTextColor="#333333"
            secureTextEntry={true}></TextBox>
        </View>

        <View>
          <Text style={styles.inputLabel}>
            {myContext.setting2name
              ? 'تأكيد كلمة المرور*'
              : 'Confirm Password*'}
          </Text>
          <TextBox
            placeholder="Enter Confirm Password"
            onChangeText={(text) => handleChange('confirm', text)}
            containerStyles={styles.customeInput}
            inputStyle={styles.inputStyle}
            placeholderTextColor="#333333"
            secureTextEntry={true}></TextBox>
        </View>

        <View style={styles.nextBtnContainer}>
          <TouchableOpacity
            style={styles.nextBtn}
            onPress={() => updatePassword()}>
            <Text style={styles.nextText}>
              {myContext.setting2name ? 'تحديث' : 'Update'}
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.nextBtnCancel}
            onPress={() => goBack()}>
            <Text style={styles.nextText}>
              {myContext.setting2name ? 'إلغاء' : 'Cancel'}
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },

  changePasswordHeading: {
    color: blackColor,
    fontSize: '16@s',
    fontWeight: 'bold',
    marginLeft: '13@s',
  },

  inputLabel: {
    color: blackColor,
    fontSize: '14@s',
    marginLeft: '13@s',
    marginTop: '5@s',
  },
  customeInput: {
    borderColor: blackColor,
    borderWidth: 1,
    paddingRight: '10@s',
    color: blackColor,
    height: '100@s',
    marginLeft: '13@s',
    height: '35@s',
    marginRight: '13@s',
    marginTop: '5@s',
  },

  inputStyle: {
    paddingLeft: '5@s',
    paddingRight: '10@s',
    color: blackColor,
    fontSize: '12@s',
  },

  nextBtnContainer: {
    padding: '40@s',
  },
  nextBtn: {
    backgroundColor: greenColor,
    alignItems: 'center',
    padding: '10@s',
    marginBottom: '15@s',
    borderRadius: '5@s',
  },
  nextText: {
    color: whiteColor,
    fontSize: '14@s',
  },

  nextBtnCancel: {
    backgroundColor: redColor,
    alignItems: 'center',
    padding: '10@s',
    marginBottom: '15@s',
    borderRadius: '5@s',
  },
});
export default UpdatePasswordScreen;
