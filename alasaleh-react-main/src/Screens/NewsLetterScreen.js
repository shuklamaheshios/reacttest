import React, {useState, useContext} from 'react';
import {ScrollView, Text, View, TouchableOpacity} from 'react-native';
import {sub} from 'react-native-reanimated';
import {ScaledSheet} from 'react-native-size-matters';
import {DrawerHeader, Input, MainHeader} from '../Components';
import {greenColor, whiteColor} from '../Theme/colors';

import AppContext from '../Utils/AppContext';

function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const NewsLetterScreen = ({goBack, handleChange, submit, navigate}) => {
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };
  return (
    <View style={styles.container}>
      {/* <DrawerHeader
        title={myContext.setting2name ? 'النشرة الإخبارية' : 'NewsLetter'}
        goBack={goBack}
      /> */}

      <MainHeader navigate={navigate} />
      <ScrollView style={styles.scrollView}>
        <View
          style={{
            paddingLeft: 10,
            paddingTop: 10,
            paddingRight: 10,
            paddingBottom: 20,
          }}>
          <Text style={styles.headingText}>
            {myContext.setting2name
              ? 'اشترك في النشرة الإخبارية المجانية للعسلة'
              : 'Subscribe to the free news letter of Al - Aslah'}
          </Text>
        </View>

        <Input
          label={
            myContext.setting2name
              ? 'العنوان البريد الإلكتروني'
              : 'E-mail Address'
          }
          placeHolder="Enter Email Address"
          name="email"
          onChangeText={handleChange}
        />

        <View style={styles.submitBtnContainer}>
          <TouchableOpacity style={styles.submitBtn} onPress={() => submit()}>
            <Text style={styles.submitText}>
              {myContext.setting2name ? 'إرسال' : 'Submit'}
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },

  submitBtnContainer: {
    paddingTop: '15@s',
    paddingLeft: '30@s',
    paddingRight: '30@s',
  },
  submitBtn: {
    backgroundColor: greenColor,
    alignItems: 'center',
    padding: '10@s',
    borderRadius: '5@s',
  },

  submitText: {
    color: whiteColor,
    fontSize: '14@s',
  },

  scrollView: {
    flex: 1,
  },

  headingText: {
    color: '#333333',
    fontSize: '14@s',
  },
});

export default NewsLetterScreen;
