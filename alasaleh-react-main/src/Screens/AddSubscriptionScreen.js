import React from 'react';
import AddSubscriptionView from '../Components/AddSubscriptionView';
import {ScaledSheet} from 'react-native-size-matters';
import {whiteColor} from '../Theme/colors';
import {Button, SafeAreaView, ActivityIndicator, View} from 'react-native';
import {CustomLoader} from '../Components';
import Storage from '../Utils/storage';
import config from '../Utils/config';
import Toast from 'react-native-simple-toast';
const STRIPE_ERROR = 'Payment service error. Try again later.';
const SERVER_ERROR = 'Server error. Try again later.';

const TEST_STRIPE_API_KEY = 'pk_test_51HKytdHp3lInSknoIcvvUvTfnwaw40jfH3LBLNCLEX19zG7yw1Dja2q8CAapdsvEHINHPGeseqgImgr444TSEFLO00rXyHVJmu'
/**
 * The method sends HTTP requests to the Stripe API.
 * It's necessary to manually send the payment data
 * to Stripe because using Stripe Elements in React Native apps
 * isn't possible.
 *
 * @param creditCardData the credit card data
 * @return Promise with the Stripe data
 */
const getCreditCardToken = (creditCardData, apiKey) => {
  const card = {
    'card[number]': creditCardData.values.number.replace(/ /g, ''),
    'card[exp_month]': creditCardData.values.expiry.split('/')[0],
    'card[exp_year]': creditCardData.values.expiry.split('/')[1],
    'card[cvc]': creditCardData.values.cvc
  };

  return fetch('https://api.stripe.com/v1/tokens', {
    headers: {
      // Use the correct MIME type for your server
      Accept: 'application/json',
      // Use the correct Content Type to send data in request body
      'Content-Type': 'application/x-www-form-urlencoded',
      // Use the Stripe publishable key as Bearer
      Authorization: `Bearer ${apiKey}`
    },
    // Use a proper HTTP method
    method: 'post',
    // Format the credit card data to a string of key-value pairs
    // divided by &
    body: Object.keys(card)
      .map(key => key + '=' + card[key])
      .join('&')
  }).then(response => response.json());
};



/**
 * The main class that submits the credit card data and
 * handles the response from Stripe.
 */
export default class AddSubscription extends React.Component {
  static navigationOptions = {
    title: 'Subscription page',
  };

  constructor(props) {
    super(props);
    this.state = {
      submitted: false,
      error: null
    }
  }

  async componentDidMount() {
    const { route } = this.props;
    // this.setState({
    //   apiKey: route.params.cardInfo.key,
    // });

    Storage.retrieveData('Stripe').then((resp) => {
      this.setState({
        apiKey: resp.key,
      });
    });

    Storage.retrieveData('cartData').then((res) => {
      this.setState({
        cart: res,
      });
    });

    Storage.retrieveData('user').then((resp) => {
      this.setState({
        userto: resp.token,
      });
    });
    Storage.retrieveData('userinfo').then((resp) => {
      this.setState({
        userinfo: resp,
      });
    });

    Storage.retrieveData('coupon').then((resp) => {
      this.setState({
        coupenCode: resp,
      });
    });
  }

  /**
   * The method imitates a request to our server.
   *
   * @param creditCardToken
   * @return {Promise<Response>}
   */
  checkoutWithStripe = async (creditCardToken) => {
    const { navigation } = this.props;
    let self = this
    self.setState({
      loader: true,
    });

    // console.log(this.state.cart,this.state.userinfo,this.state.apiKey,)
    const params = {
      stripeToken: creditCardToken.id,
      cart_token: this.state.cart.cartToken,
      cartKey: this.state.cart.cartKey,
      email: this.state.userinfo.email,
      address: this.state.userinfo.address,
      city: this.state.userinfo.city,
      state: this.state.userinfo.state ? this.state.userinfo.state : 'state',
      postal_code: this.state.userinfo.zip_code,
      "apartment": this.state.userinfo.apartment,
      lat: parseFloat(this.state.userinfo.latitude),
      long: parseFloat(this.state.userinfo.longitude),
      "coupon_code": this.state.coupenCode,
      save_account_info: '0',
      payment_method: 'points',
    };
    console.log('params', params)
    await fetch(`${config.apiUrl}/pay_with_stripe`, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${this.state.userto}`,
      },
      // Use a proper HTTP method
      method: 'post',
      body: JSON.stringify(params)
    }).then((response) => {
      console.log('received response ', response)
      return response.json()
    })
      .then((res) => {
        console.log('response : ', res)
        if (res.status) {
          Toast.show(res.message)
          self.setState({
            loader: false
          });
          Storage.removeData('cartData');
          navigation.navigate('MyOrder');
        } else {
          self.setState({
            loader: false,
          });
          Toast.show(res.message || "Error while processing payment");
        }

      })
      .catch((error) => {
        console.log('payment error', error)
        self.setState({
          loader: false,
        });
        Toast.show(error);
      });
  };

  // Handles submitting the payment request
  onSubmit = async (creditCardInput) => {
    const { navigation } = this.props;
    // Disable the Submit button after the request is sent
    this.setState({ submitted: true });
    let creditCardToken;

    try {
      // Create a credit card token
      // TODO: Enable the following line once our version of the code is running on the required sub domain
      // creditCardToken = await getCreditCardToken(creditCardInput, this.state.apiKey);

      creditCardToken = await getCreditCardToken(creditCardInput, this.state.apiKey);
      if (creditCardToken.error) {
        // Reset the state if Stripe responds with an error
        // Set submitted to false to let the user subscribe again
        this.setState({ submitted: false, error: STRIPE_ERROR });
        return;
      }
    } catch (e) {
      // Reset the state if the request was sent with an error
      // Set submitted to false to let the user subscribe again
      this.setState({ submitted: false, error: STRIPE_ERROR });
      return;
    }

    // Send a request to your server with the received credit card token
    const { error } = await this.checkoutWithStripe(creditCardToken);
    // Handle any errors from your server
    if (error) {
      this.setState({ submitted: false, error: SERVER_ERROR });
    } else {
      this.setState({ submitted: false, error: null });
      navigation.navigate('Home')
    }
  };

  render() {
    const { submitted, error } = this.state;
    return (
      <SafeAreaView style={styles.safeAreaContainer}>
      { this.state.loader ? (
        <CustomLoader />
      ) : (
      <AddSubscriptionView
        error={error}
        submitted={submitted}
        onSubmit={this.onSubmit}
      />)}
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  safeAreaContainer: {
    flex: 1,
    backgroundColor: whiteColor,
  },
});