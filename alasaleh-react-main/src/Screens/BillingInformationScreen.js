import React, { useState, useContext } from 'react';
import {
  ScrollView,
  View,
  Text,
  Modal,
  TouchableOpacity,
  TouchableHighlight,
  TextInput,
} from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import { DrawerHeader, MainHeader, ProfileInput } from '../Components';
import { blackColor, greenColor, whiteColor } from '../Theme/colors';
import AppContext from '../Utils/AppContext';

function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const BillingInformationScreen = ({
  goBack,
  navigate,
  handlechange,
  validateCoupenCode,
  user,
  latlng,
  handlelatlng,
  locationAddress,
  getLocation,
}) => {
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };
  const skip = { value: 'skip' }
  const [modalVisible, setModalVisible] = useState(false);
  console.log("The User is:: ", user)


  const next = () => {
    // alert (user.phone)
    setModalVisible(!modalVisible)              
  }
  return (
    <View style={styles.container}>
      <Modal
        visible={modalVisible}
        transparent={true}
        style={{ height: 50, width: 50 }}>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>
              {myContext.setting2name
                ? 'هل لديك قسيمة؟'
                : 'Do You have a Coupon?'}
            </Text>
            <TextInput
              placeholder="Enter Coupon Code"
              style={styles.modalInput}
              onChangeText={(value) => {
                handlechange('coupenCode', value);
              }}
            />
            <Text style={styles.modalDescription}>
              {myContext.setting2name
                ? 'أدخل رمز قسيمة الحسم إن وجد'
                : 'If you have a discount coupon code then enter & validate to get discount'}
            </Text>
            <View style={styles.nextBtnContainer}>
              <TouchableOpacity
                onPress={() => {
                  alert (user.phone)
                  navigate('Billing', user),
                    setModalVisible(!modalVisible),
                    modalVisible ? validateCoupenCode() : null;
                }}
                style={styles.nextBtn}>
                <Text style={styles.nextText}>
                  {myContext.setting2name ? 'تحقق' : 'VALIDATE'}
                </Text>
              </TouchableOpacity>
            </View>

            <View style={styles.nextBtnContainer}>
              <TouchableOpacity
                onPress={() => {
                  navigate('Billing', user), setModalVisible(!modalVisible);
                }}
                style={styles.skipBtn}>
                <Text style={styles.skipText}>
                  {myContext.setting2name ? 'تحقق' : 'SKIP    >>'}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
      {/* <DrawerHeader
        title={
          myContext.setting2name ? 'معلومات الفواتير' : 'Billing Information'
        }
        goBack={goBack}
      /> */}
      <MainHeader home={false} navigate={navigate} />
            {/* error */}

      <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }}>
        <View style={{ padding: 10 }}>
          <View style={styles.personalLContainer}>
            <Text style={styles.personalText}>
              {myContext.setting2name ? 'شخصي' : 'Personal'}
            </Text>
          </View>
          <View style={styles.inputContainer}>
            <View style={styles.inputContainerInner}>
              <ProfileInput
                type="text"
                placeholder="Name"
                label={myContext.setting2name ? 'الاسم الاول' : 'First Name'}
                defaultValue={user.name}
                name="name"
                onChangeText={handlechange}
                // edit={false}
              />
            </View>
            <View style={styles.inputContainerInner}>
              <ProfileInput
                type="text"
                placeholder="Last Name"
                label={myContext.setting2name ? 'الكنية' : 'Last Name'}
                defaultValue={user.last_name}
                name="last_name"
                onChangeText={handlechange}
                // edit={false}
              />
            </View>
          </View>
          <View>
            {/* <TouchableOpacity onPress={() => navigate('AddMobile')}> */}
              <ProfileInput
                type="number"
                placeholder="null"
                label={myContext.setting2name ? 'هاتف' : 'Phone'}
                defaultValue={user.phone}
                name="phone"
                // edit={false}
                onChangeText={handlechange}
              />
            {/* </TouchableOpacity> */}
          </View>
          <View>
            <ProfileInput
              type="email"
              placeholder="email@email.com"
              label={
                myContext.setting2name
                  ? 'العنوان البريد الإلكتروني'
                  : 'Email Address'
              }
              defaultValue={user.email}
              name="email"
              onChangeText={handlechange}
              // edit={false}
            />
          </View>
          <View style={styles.personalLContainer}>
            <Text style={styles.personalText}>
              {myContext.setting2name ? 'العنوان الشحن' : 'Shipping Address'}
            </Text>
          </View>

          <TouchableOpacity
            onPress={() => navigate('ShippingMap', { handlelatlng, getLocation })}
            style={{ marginBottom: 10, marginTop: 10 }}>
            <ProfileInput
              label={myContext.setting2name ? 'العنوان' : 'Address'}
              placeholder={myContext.setting2name ? 'العنوان' : ' Address'}
              icon="map-marker-alt"
              // value={`${locationAddress}`}
              edit={false}
              defaultValue={user.address}
              name="address"

            />
          </TouchableOpacity>
          {/* <View>
            <ProfileInput
              type="text"
              placeholder="Apartment"
              label={myContext.setting2name ? 'عنوان' : 'Apartment'}
              defaultValue={user.address}
              name="address"
              onChangeText={handlechange}
            />
          </View> */}
          <View>
            <ProfileInput
              type="text"
              placeholder="null"
              label={myContext.setting2name ? 'رقم الشقة' : 'Apartment'}
              defaultValue={user.apartment}
              name="apartment"
              onChangeText={handlechange}
              // edit={false}
            />
          </View>
          <View style={styles.inputContainer}>
            <View style={styles.inputContainerInner}>
              <ProfileInput
                type="text"
                placeholder="null"
                label={myContext.setting2name ? 'الولاية' : 'State'}
                defaultValue={user.state}
                name="state"
                onChangeText={handlechange}
                // edit={false}
              />
            </View>
            <View style={styles.inputContainerInner}>
              <ProfileInput
                type="text"
                placeholder="null"
                label={myContext.setting2name ? 'مدينة' : 'City'}
                defaultValue={user.city}
                name="city"
                onChangeText={handlechange}
                // edit={false}
              />
            </View>
          </View>
          <View>
            <ProfileInput
              type="number"
              placeholder="null"
              label={myContext.setting2name ? 'رمز بريدي' : 'Zip Code'}
              defaultValue={user.zip_code}
              name="zip_code"
              onChangeText={handlechange}
              // edit={false}
            />
          </View>
          <View>
            <ProfileInput
              type="text"
              placeholder="null"
              label={myContext.setting2name ? 'البلد' : 'Country'}
              defaultValue={user.country}
              name="country"
              onChangeText={handlechange}
              // edit={false}
            />
          </View>
          <View style={styles.inputContainer}>
            <View style={styles.inputContainerInner}>
              <ProfileInput
                type="text"
                placeholder="null"
                label={myContext.setting2name ? 'خط العرض' : 'Latitude'}
                // defaultValue={`${latlng.lat}`}
                defaultValue={user.latitude}
                name="latitude"
                value={latlng.lat}
                // edit={false}
              />
            </View>
            <View style={styles.inputContainerInner}>
              <ProfileInput
                type="number"
                placeholder="0.00"
                label={myContext.setting2name ? 'خط الطول' : 'Latitude'}
                // defaultValue={`${latlng.lng}`}
                defaultValue={user.longitude}
                name="longitude"
                // edit={false}
              />
            </View>
          </View>

          <View style={styles.nextBtnContainer}>
            <TouchableOpacity
              onPress={() => {
                next()
              }}
              style={styles.nextBtn}>
              <Text style={styles.nextText}>
                {myContext.setting2name ? 'التالي' : 'Next'}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
      {/* error */}
    </View >
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },

  header: {
    backgroundColor: greenColor,
    height: '130@s',
    justifyContent: 'center',
  },

  headerBody: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: '20@s',
    paddingRight: '20@s',
  },

  headerName: {
    fontSize: '18@s',
    color: whiteColor,
  },

  headerEmail: {
    fontSize: '14@s',
    color: whiteColor,
    marginLeft: '10@s',
  },
  headerImage: {
    height: '65@s',
    width: '65@s',
    borderRadius: 50,
    borderColor: whiteColor,
    borderWidth: 2,
  },

  personalLContainer: {
    borderBottomColor: blackColor,
    borderBottomWidth: 1,
    paddingBottom: '5@s',
  },

  personalText: {
    color: greenColor,
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: '10@s',
  },

  inputContainerInner: {
    width: '49%',
  },

  nextBtnContainer: {
    padding: '25@s',
  },
  nextBtn: {
    backgroundColor: greenColor,
    alignItems: 'center',
    padding: '6@s',
    borderRadius: '5@s',
  },
  nextText: {
    color: whiteColor,
    fontSize: '14@s',
  },

  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 2,
    padding: 35,
    shadowColor: '#000',
    width: '90%',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    color: blackColor,
    fontSize: '14@s',
  },

  modalInput: {
    padding: '8@s',
    borderColor: blackColor,
    borderWidth: 1,
    marginBottom: '10@s',
  },

  modalDescription: {
    color: blackColor,
    marginTop: '20@s',
    marginBottom: '20@s',
  },
  nextBtnContainer: {
    padding: '13@s',
  },
  nextBtn: {
    backgroundColor: greenColor,
    alignItems: 'center',
    padding: '10@s',
    borderRadius: '5@s',
  },
  nextText: {
    color: whiteColor,
    fontSize: '14@s',
  },

  skipBtn: {
    backgroundColor: whiteColor,
    alignItems: 'center',
    padding: '10@s',
    borderRadius: '5@s',
  },

  skipText: {
    color: blackColor,
    fontSize: '14@s',
  },
});
export default BillingInformationScreen;
