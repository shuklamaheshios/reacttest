import React, { useState, useContext, useEffect } from 'react';
import {
  Alert,
  Image,
  ImageBackground,
  Modal,
  Text,
  TouchableOpacity,
  View,
  
} from 'react-native';
import logo_grey from '../Assets/images/logo_grey.png';
import close_black from '../Assets/images/close_black.png';
import unchecked_icon from '../Assets/images/unchecked_icon.png';
import { ScaledSheet } from 'react-native-size-matters';
import AppContext from '../Utils/AppContext';
import { blackColor, greenColor, whiteColor } from '../Theme/colors';
import AsyncStorage from '@react-native-community/async-storage';

const firstlogin = 'firstOpen'


function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const StartModal = () => {
  
  useEffect(() => {
    readData()
  }, [])

  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const [modalVisible, setModalVisible] = useState(false);

  const handleClick = () => {
    myContext.toggleSetting2();
    saveData(false)
    forceUpdate();
  };


   

  async function saveData(bool)  {
    setModalVisible(bool)
    try {
      await AsyncStorage.setItem(firstlogin, bool?'1':'0')
      // alert('Data successfully saved')
    } catch (e) {
      // alert('Failed to save the data to the storage')
    }
  }

  const readData = async () => {
    try {
      let userAge   = await AsyncStorage.getItem(firstlogin)
      const s = parseInt(userAge)
      // alert (userAge)
      
        setModalVisible(userAge == 0 ?false:true)
      
    } catch (e) {
      // alert('Failed to fetch the data from storage')
    }
  }







  return (
    <View style={styles.centeredView}>
      <Modal
        // animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}
      >
        <View style={styles.centeredView}>
          <ImageBackground
            resizeMode="center"
            source={logo_grey}
            style={styles.modalView}>
            <View style={styles.header}>
              <Text style={styles.headerText}>Select Language</Text>
              <TouchableOpacity style={styles.closeImageContainer}
                onPress={() => saveData(false)}
              >
                <Image style={styles.closeImage} source={close_black} />
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              onPress={() => saveData(false)}
              style={styles.selctBtn}>
              <Text style={styles.selectText}>English</Text>
              <Image style={styles.checkImage} source={unchecked_icon} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => handleClick()}
              // onPress={(handleClick, () => setModalVisible(false))}
              style={styles.selctBtn}>
              <Text style={styles.selectText}>Arabic</Text>
              <Image style={styles.checkImage} source={unchecked_icon} />
            </TouchableOpacity>

            <View style={styles.footer}>
              <Text style={styles.welcomeText}>Welcome To</Text>
              <Text style={styles.alashleh}>Alasaleh</Text>
            </View>
          </ImageBackground>
        </View>
      </Modal>
    </View>
  );
};

const styles = ScaledSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.6)',
  },
  modalView: {
    margin: 20,

    backgroundColor: whiteColor,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  header: {
    backgroundColor: greenColor,
    padding: '10@s',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
  },
  headerText: {
    color: whiteColor,
    fontSize: '12@s',
  },
  closeImageContainer: {
    padding: '10@s',

    position: 'absolute',
    top: '-17@s',
    right: '-15@s',
    backgroundColor: 'rgba(0,0,0,0)',
  },
  closeImage: {
    height: '20@s',
    width: '20@s',
  },
  selctBtn: {
    padding: '12@s',
    borderBottomColor: blackColor,
    borderBottomWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  checkImage: {
    height: '20@s',
    width: '20@s',
  },
  selectText: {
    color: greenColor,
    fontSize: '14@s',
  },
  footer: {
    padding: '10@s',
    justifyContent: 'center',
    alignItems: 'center',
  },
  welcomeText: {
    fontSize: '20@s',
    fontWeight: '700',
    color: greenColor,
  },
  alashleh: {
    fontSize: '24@s',
    fontWeight: 'bold',
    color: greenColor,
  },
});

export default StartModal;
