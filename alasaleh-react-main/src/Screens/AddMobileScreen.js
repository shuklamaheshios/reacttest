import React, {useState, useContext} from 'react';
import {
  Text,
  TextInput,
  View,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {DrawerHeader, MainHeader} from '../Components';
import {blackColor, greenColor, whiteColor} from '../Theme/colors';

import AppContext from '../Utils/AppContext';

function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const AddMobileScreen = ({goBack, handleChange, genrateOtp, navigate}) => {
  const [searchTerm, setSearchTerm] = useState('');
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };
  return (
    <View style={styles.container}>
      {/* <DrawerHeader goBack={goBack} title="Add Mobile" /> */}
      <MainHeader home={false} navigate={navigate} />
      <ScrollView style={{flex: 1}}>
        <View style={styles.body}>
          <Text style={styles.bodyHeading}>
            {myContext.setting2name ? 'ما هو رقمك' : "What's Your Number"}
          </Text>
          <Text style={styles.bodyDescription}>
            {myContext.setting2name
              ? 'أدخل رقم الهاتف المحمول للمتابعة'
              : 'Enter mobile number to continue'}
          </Text>
          <TextInput
            placeholder="Your number"
            name="number"
            onChangeText={handleChange}
            style={styles.input}
          />

          <View style={styles.nextBtnContainer}>
            <TouchableOpacity
              style={styles.nextBtn}
              onPress={() => genrateOtp()}>
              <Text style={styles.nextText}>
                {myContext.setting2name ? 'إرسال' : 'Send'}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },

  body: {
    padding: '10@s',
  },

  bodyHeading: {
    fontSize: '24@s',
    fontWeight: 'bold',
  },

  bodyDescription: {
    fontSize: '12@s',
    marginLeft: '5@s',
  },

  input: {
    borderBottomColor: blackColor,
    borderBottomWidth: 1,
    paddingBottom: '-1@s',
    paddingLeft: '10@s',
    fontSize: '12@s',
    marginTop: '10@s',
  },

  nextBtnContainer: {
    padding: '13@s',
    marginTop: '20@s',
  },
  nextBtn: {
    backgroundColor: greenColor,
    alignItems: 'center',
    padding: '10@s',
    borderRadius: '5@s',
  },
  nextText: {
    color: whiteColor,
    fontSize: '15@s',
  },
});

export default AddMobileScreen;
