import React, {useState, useContext} from 'react';
import {View, Text, FlatList} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import FlatGrid from 'react-native-super-grid';
import {MainHeader} from '../Components';
import Product from '../Components/Product';
import {whiteColor} from '../Theme/colors';
import AppContext from '../Utils/AppContext';

function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const ProductByCategoryScreen = ({navigate, data}) => {
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };
  const renderItem = ({item}) => (
    <Product
      navigate={navigate}
      title={myContext.setting2name ? item.name_arabic : item.name}
      image={item.img_path}
      id={item.id}
    />
  );
  return (
    <View style={{flex: 1}}>
      <View style={{flex: 1}}>
        <FlatList
          numColumns={4}
          // contentContainerStyle={{flexDirection: 'row', flexWrap: 'wrap'}}
          data={data}
          style={styles.gridView}
          renderItem={renderItem}
        />
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: whiteColor,
  },
  gridView: {
    flex: 1,
  },
});
export default ProductByCategoryScreen;
