import React, {useContext, useState} from 'react';
import {Text, View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {DrawerHeader, MainHeader} from '../Components';
import {
  blackColor,
  blueColor,
  greenColor,
  lightGray,
  parrotColor,
  redColor,
  whiteColor,
} from '../Theme/colors';
import AppContext from '../Utils/AppContext';

function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const ReferelPointsScreen = ({goBack, navigate, referpoints}) => {
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };

  return (
    <View style={styles.container}>
      {/* <DrawerHeader
        goBack={goBack}
        navigate={navigate}
        title={
          myContext.setting2name
            ? 'النقاط ومجموع المكافآت'
            : 'Points & Referral Amount'
        }
      /> */}

      <MainHeader navigate={navigate} />
      <View style={styles.header}>
        <View style={[styles.headerItem, styles.parrotColor]}>
          <View style={{padding: 10}}>
            <Text style={styles.headerColor}>
              {myContext.setting2name ? 'مجموع النقاط' : 'Total Points'}
            </Text>
          </View>
          <View style={styles.headerItemline}></View>
          <View style={{padding: 10}}>
            <Text style={styles.headerColor}>{referpoints.points}</Text>
          </View>
        </View>

        <View style={[styles.headerItem, styles.blueColor]}>
          <View style={{padding: 10}}>
            <Text style={styles.headerColor}>
              {myContext.setting2name ? 'مبلغ النقاط' : 'Points Amount'}
            </Text>
          </View>
          <View style={styles.headerItemline}></View>
          <View style={{padding: 10}}>
            <Text style={styles.headerColor}>${referpoints.amount}</Text>
          </View>
        </View>

        <View style={[styles.headerItem, styles.redColor]}>
          <View style={{padding: 10}}>
            <Text numberOfLines={1} style={styles.headerColor}>
              {myContext.setting2name ? `مبلغ الإحا` : `Referral Amount`}
            </Text>
          </View>
          <View style={styles.headerItemline}></View>
          <View style={{padding: 10}}>
            <Text style={styles.headerColor}>${referpoints.refferd}</Text>
          </View>
        </View>
      </View>
      <View style={styles.pointDescription}>
        <Text style={styles.descriptiontext}>
          {myContext.setting2name
            ? `إذا قمت بتقديم طلب بقيمة ${referpoints?.infoData?.points_to_dollar} دولارًا على الأقل ، فستحصل على نقطة واحدة`
            : `If you place an order of atleast ${referpoints?.infoData?.points_to_dollar} you will get 1 point`}
        </Text>
      </View>

      <View style={[styles.pointDescription, styles.blueColor]}>
        <Text style={styles.descriptiontext}>
          {myContext.setting2name
            ? 'عند شرائك ب $1 ستحصل على 1 نقطة'
            : 'if you have 30 points you will get 1 dollar'}
        </Text>
      </View>

      <View style={[styles.pointDescription, styles.redColor]}>
        <Text style={styles.descriptiontext}>
          {myContext.setting2name
            ? 'قم بدعوة صديق واحصل على 5 دولارات هدية'
            : 'Reffer a friend and get 5 Dollars'}
        </Text>
      </View>

      <View style={[styles.pointDescription, styles.blueColor]}>
        <Text style={styles.descriptiontext}>
          {myContext.setting2name
            ? `إذا كان لديك مبلغ كاف من النقاط ومبلغ إعادة البيع وهو أقل من ذلك أو يساوي
            أو مبلغ الطلب. يمكنك الدفع باستخدام النقاط الخاصة بك ومبلغ الإحالة`
            : `If you enough points amount and refferel amount which is less then or equal
          or order amount. You can checkout using your points and referral amount`}
        </Text>
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },

  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: '10@s',
  },

  headerItem: {
    width: '30%',
    height: '80@s',
    justifyContent: 'space-between',
    backgroundColor: greenColor,
  },

  headerItemline: {
    height: '0.5@s',
    backgroundColor: blackColor,
  },

  headerColor: {
    color: whiteColor,
    textAlign: 'center',
    fontSize: '10@s',
  },

  parrotColor: {
    backgroundColor: parrotColor,
  },
  blueColor: {
    backgroundColor: blueColor,
  },

  redColor: {
    backgroundColor: redColor,
  },

  pointDescription: {
    backgroundColor: parrotColor,
    margin: '12@s',
    padding: '10@s',
    color: whiteColor,
    borderRadius: '5@s',
  },

  descriptiontext: {
    fontSize: '14@s',
    color: whiteColor,
    textAlign: 'center',
  },
});

export default ReferelPointsScreen;
