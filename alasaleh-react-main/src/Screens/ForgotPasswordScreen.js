import React, {useState, useContext, useEffect} from 'react';
import {Text, View, TouchableOpacity, Image} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {EmptyHeader, Input, MainHeader} from '../Components';
import {greenColor, lightGray, whiteColor} from '../Theme/colors';
import registerImage from '../Assets/images/pencil.png';
import lockImage from '../Assets/images/lock_black.png';
import AppContext from '../Utils/AppContext';

const ForgotPasswordScreen = ({navigate, goBack, handleChange, submit}) => {
  const myContext = useContext(AppContext);
  return (
    <View style={styles.container}>
      <EmptyHeader goBack={goBack} />

      {/* <MainHeader navigate={navigate} /> */}
      <View style={styles.body}>
        <View style={{paddingLeft: 15, marginBottom: 12, marginTop: 12}}>
          <Text style={styles.heading}>
            {myContext.setting2name
              ? 'إعادة تعيين كلمة المرور'
              : 'Reset Password'}
          </Text>
        </View>
        <View>
          <Input
            placeHolder="example@example.com"
            label={
              myContext.setting2name ? 'عنوان بريد الكتروني' : 'E-mail Address'
            }
            name="email"
            onChangeText={handleChange}
          />
        </View>
        <View style={styles.nextBtnContainer}>
          <TouchableOpacity style={styles.nextBtn} onPress={() => submit()}>
            <Text style={styles.nextText}>
              {myContext.setting2name
                ? 'إرسال رابط إعادة تعيين كلمة السر'
                : 'Send Password Reset Link'}
            </Text>
          </TouchableOpacity>
        </View>

        <View style={styles.footerBottom}>
          <TouchableOpacity
            onPress={() => navigate('Login')}
            style={styles.authBtn}>
            <Image style={styles.image} source={lockImage} />
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                width: '100%',
              }}>
              <Text>{myContext.setting2name ? 'تسجيل الدخول' : 'LOGIN'}</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigate('Register')}
            style={styles.authBtn}>
            <Image style={styles.image} source={registerImage} />
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                width: '100%',
              }}>
              <Text>{myContext.setting2name ? 'تسجيل' : 'REGISTER'}</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
  body: {
    padding: '10@s',
  },
  heading: {
    fontSize: '18@s',
    lineHeight: '20@s',
    fontWeight: 'bold',
  },

  nextBtnContainer: {
    padding: '13@s',
    marginTop: '20@s',
  },
  nextBtn: {
    backgroundColor: greenColor,
    alignItems: 'center',
    padding: '10@s',
    borderRadius: '5@s',
  },
  nextText: {
    color: whiteColor,
    fontSize: '15@s',
  },

  footerBottom: {
    flexDirection: 'row',
    paddingLeft: '18@s',
    paddingRight: '20@s',
    justifyContent: 'space-between',
    marginBottom: '20@s',
  },
  authBtn: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: lightGray,
    width: '48%',
    padding: '10@s',
    justifyContent: 'space-between',
  },
  image: {
    height: '16@s',
    width: '16@s',
    resizeMode: 'center',
  },
});

export default ForgotPasswordScreen;
