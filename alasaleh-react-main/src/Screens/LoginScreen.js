import React, {useState, useContext} from 'react';
import {
  ScrollView,
  View,
  TouchableOpacity,
  Text,
  Image,
  ToastAndroid,
} from 'react-native';
import TextBox from 'react-native-password-eye';
import {ScaledSheet} from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Input, EmptyHeader, MainHeader} from '../Components';
import registerImage from '../Assets/images/pencil.png';
import {
  blackColor,
  blueColor,
  greenColor,
  lightGray,
  redColor,
  whiteColor,
} from '../Theme/colors';

import AppContext from '../Utils/AppContext';

function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const LoginScreen = ({
  navigate,
  goBack,
  handleChange,
  login,
  googleLogin,
  facebookLogin,
}) => {
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };
  const [password, setPassword] = useState('');

  return (
    <View style={styles.container}>
      <EmptyHeader goBack={goBack} />
      {/* <MainHeader home={false} navigate={navigate} /> */}
      <ScrollView style={{flex: 1, marginTop: 10}}>
        <View style={styles.loginHeader}>
          <View style={styles.loginIconContainer}>
            <Icon name="user" size={90} color={whiteColor} />
            <Icon
              name="caret-down"
              color={greenColor}
              size={50}
              style={styles.downArrow}
            />
          </View>
          <Text style={styles.loginHeading}>
            {myContext.setting2name
              ? 'تسجيل الدخول إلى حسابك'
              : 'Login to your account'}
          </Text>
        </View>
        <Input
          type="email"
          label={
            myContext.setting2name
              ? 'أدخل البريد الإلكتروني أو رقم الهاتف'
              : 'Enter Email or Phone number'
          }
          s
          placeHolder="e.g example@example.com"
          onChangeText={handleChange}
          name={'email'}
        />

        <View>
          <Text style={styles.inputLabel}>Enter Password</Text>
          <TextBox
            placeholder="***********"
            onChangeText={(text) => handleChange('password', text)}
            containerStyles={styles.customeInput}
            inputStyle={styles.inputStyle}
            secureTextEntry={true}></TextBox>
        </View>

        <View style={styles.nextBtnContainer}>
          <TouchableOpacity style={styles.nextBtn} onPress={() => login()}>
            <Text style={styles.nextText}>
              {myContext.setting2name ? 'تسجيل الدخول' : 'LOGIN'}
            </Text>
          </TouchableOpacity>
        </View>
{/* 
        <View style={styles.orContainer}>
          <Text style={styles.orText}>
            {myContext.setting2name
              ? 'أو تسجيل الدخول باستخدام ملف التعريف الاجتماعي الخاص بك'
              : 'or Login with Your Social Profile'}
          </Text>
        </View>

        <View style={styles.footer}>
          <TouchableOpacity
            style={styles.socialFaceButton}
            onPress={() => facebookLogin()}>
            <Icon name="facebook-square" size={22} color={whiteColor} />
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                width: '100%',
              }}>
              <Text style={{color: whiteColor}}>
                {myContext.setting2name ? 'تسجيل الدخول' : 'Login'}
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.socialGoogleBtn}
            onPress={() => googleLogin()}>
            <Icon name="google" size={22} color={whiteColor} />
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                width: '100%',
              }}>
              <Text style={{color: whiteColor}}>
                {myContext.setting2name ? 'تسجيل الدخول' : 'Sign in'}
              </Text>
            </View>
          </TouchableOpacity>
        </View> */}
            {/* removed */}

        <TouchableOpacity
          onPress={() => navigate('ForgotPasword')}
          style={styles.forgetPasswrodContainer}>
          <Text style={styles.forgetPasswrodText}>Forget Password?</Text>
        </TouchableOpacity>

        <View style={styles.footerBottom}>
          <TouchableOpacity
            onPress={() => navigate('Register')}
            style={styles.authBtn}>
            <Image style={styles.image} source={registerImage} />
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                width: '100%',
              }}>
              <Text>{myContext.setting2name ? 'تسجيل' : 'REGISTER'}</Text>
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },

  loginHeader: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: '30@s',
  },

  loginIconContainer: {
    backgroundColor: greenColor,
    padding: '20@s',
    borderRadius: 100,
    height: '110@s',
    width: '110@s',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: '10@s',
    position: 'relative',
  },

  downArrow: {
    position: 'absolute',
    bottom: '-24@s',
    left: '63%',
  },

  orContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: '10@s',
  },

  orText: {
    color: blackColor,
    fontSize: '12@s',
  },

  loginHeading: {
    color: blackColor,
    fontSize: '18@s',
  },
  nextBtnContainer: {
    padding: '13@s',
  },
  nextBtn: {
    backgroundColor: greenColor,
    alignItems: 'center',
    padding: '10@s',
    borderRadius: '5@s',
  },
  nextText: {
    color: whiteColor,
    fontSize: '15@s',
  },

  footer: {
    flexDirection: 'row',
    paddingLeft: '25@s',
    paddingRight: '25@s',
    justifyContent: 'space-between',
    marginBottom: '20@s',
  },

  footerBottom: {
    flexDirection: 'row',
    paddingLeft: '28@s',
    paddingRight: '30@s',
    justifyContent: 'center',
    marginBottom: '20@s',
  },

  authBtn: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: lightGray,
    width: '48%',
    padding: '10@s',
    justifyContent: 'space-between',
  },

  socialFaceButton: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#166FE5',
    width: '48%',
    padding: '10@s',
    justifyContent: 'space-between',
    borderRadius: '4@s',
  },

  socialGoogleBtn: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: redColor,
    width: '48%',
    padding: '10@s',
    justifyContent: 'space-between',
    borderRadius: '4@s',
  },

  authText: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: '13@s',
  },

  checkBoxContainer: {
    padding: '15@s',
  },
  checkBoxText: {
    color: blackColor,
    fontSize: '12@s',
    fontWeight: 'bold',
  },
  image: {
    height: '16@s',
    width: '16@s',
    resizeMode: 'center',
  },

  customeInput: {
    borderColor: blackColor,
    borderWidth: 1,
    paddingRight: '10@s',
    color: blackColor,
    height: '100@s',
    marginLeft: '13@s',
    height: '35@s',
    marginRight: '13@s',
    marginTop: '5@s',
  },

  inputStyle: {
    paddingLeft: '5@s',
    paddingRight: '10@s',
    color: blackColor,
    fontSize: '12@s',
  },
  inputLabel: {
    color: blackColor,
    fontSize: '14@s',
    marginLeft: '13@s',
    marginTop: '5@s',
  },

  forgetPasswrodText: {
    color: blackColor,
    textAlign: 'center',
    fontSize: '12@s',
    lineHeight: '17@s',
  },
  forgetPasswrodContainer: {
    padding: '10@s',
  },
});

export default LoginScreen;
