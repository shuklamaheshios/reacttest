import React from 'react';
import {Text, View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {DrawerHeader, MainHeader} from '../Components';
import {redColor} from '../Theme/colors';

const ManageCreditCard = ({goBack, navigate}) => {
  return (
    <View style={styles.container}>
      {/* <DrawerHeader title="Manage credit cards" goBack={goBack} /> */}
      <MainHeader home={false} navigate={navigate} />
      <View style={styles.body}>
        <Text>
          Here you can find an overview of all credit cards that you saved
          during previous checouts
        </Text>
        <View style={{flex: 1, justifyContent: 'center'}}>
          <View style={styles.noCreditCardView}>
            <Text style={{color: '#333', fontSize: 16}}>
              No credit card found
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
  body: {
    flex: 1,

    padding: '10@s',
  },
  noCreditCardView: {
    padding: '10@s',
    backgroundColor: 'rgba(255,0,0, 0.2)',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default ManageCreditCard;
