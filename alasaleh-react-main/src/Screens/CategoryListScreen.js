import React, {useState, useEffect, useContext} from 'react';
import {
  FlatList,
  SafeAreaView,
  Text,
  View,
  ActivityIndicator,
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {ScaledSheet} from 'react-native-size-matters';
import FlatGrid from 'react-native-super-grid';
import {MainHeader, Product} from '../Components';
import config from '../Utils/config';
import ProductByCategoryScreen from './ProductByCategoryScreen';
import AppContext from '../Utils/AppContext';
import ProductCardListScreen from './ProductCardListScreen';
const DATA = [
  {
    id: '1',
    title: 'First Item',
    image: true,
    button: true,
  },
  {
    id: '3',
    title: 'Second Item Second Item',
    image: true,
    button: false,
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'Third Item',
    image: true,
  },
  {
    id: '4',
    title: 'Third Item',
    image: true,
  },
  {
    id: '5',
    title: 'Third Item',
    image: true,
  },
  {
    id: '6',
    title: 'Third Item',
    image: true,
  },
  {
    id: '7',
    title: 'Third Item',
    image: true,
  },
];
function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}
const CategoryListScreen = ({productList, navigate}) => {
  // console.log('main', productList);
  const [data, setdata] = useState(DATA);
  const [prod, setprod] = useState(DATA);
  const [wish, setWishList] = useState();
  // const [id, setid] = useState(0);
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };
  const [subList, setSubList] = useState();
  const [loader, setloader] = useState(true);
  useEffect(() => {
    console.log(productList[0]);
    productList[0].button = true;
    handleColor(productList[0].id);
  }, []);
  const handleColor = async (id) => {
    // setid(id)
    setloader(true);
    productList.map((data, index) => {
      if (data.id === id) {
        data.button = true;
      } else {
        data.button = false;
      }
    });
    await getProducts(id);
    await fetch(`${config.apiUrl}/get_last_sub_categories/${id}`)
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson.data);
        setdata(responseJson.data);
        setloader(false);
      })
      .catch((error) => {
        console.error(error);
      });
    productList.map((data, index) => {
      if (data.button) {
        for (const key in data.list) {
          if (data.list.hasOwnProperty(key)) {
            const element = data.list[key];
          }
        }
      }
    });
    forceUpdate();
  };
  const getProducts = async (id) => {
    await fetch(
      `${config.apiUrl}/search_by_category/category_id/${id}`,
    )
      .then((response) => response.json())
      .then((responseJson) => {
        setprod(responseJson.data.lims_product_list);
        setloader(false);
      })
      .catch((error) => {
        console.error(error);
      });
  };
  const ch = (w) => {
    setWishList(w);
  };
  const renderItem = ({item}) => (
    <Product
      handleColor={handleColor}
      title={myContext.setting2name ? item.name_arabic : item.name}
      button={item.button}
      image={item.img_path}
      id={item.id}
    />
  );
  return (
    <View style={styles.container}>
      <MainHeader home={false} navigate={navigate} wishCount={wish} />
      <SafeAreaView style={styles.listItem}>
        <FlatList
          data={productList}
          inverted={myContext.setting2name ? true : false}
          renderItem={renderItem}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
        />
      </SafeAreaView>
      <ScrollView>
        {loader ? (
          <View
            style={{
              height: 520,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <ActivityIndicator
              size="large"
              color="#00FF00"
              animating={loader}
            />
          </View>
        ) : data.length === 0 ? (
          <ProductCardListScreen
            products={prod}
            check={true}
            navigate={navigate}
            ch={ch}
          />
        ) : (
          <ProductByCategoryScreen data={data} navigate={navigate} />
        )}
      </ScrollView>
    </View>
  );
};
const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
  gridView: {
    flexGrow: 1,
  },
  listItem: {
    marginTop: '-14@s',
  },
});
export default CategoryListScreen;
