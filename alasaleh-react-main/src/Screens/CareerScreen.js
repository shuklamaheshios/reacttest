import React, {useState, useContext, useEffect} from 'react';
import {Text, View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {ScaledSheet} from 'react-native-size-matters';
import {DrawerHeader, MainHeader} from '../Components';
import {blackColor, whiteColor} from '../Theme/colors';
import AppContext from '../Utils/AppContext';

const CareerScreen = ({goBack, data, navigate}) => {
  const myContext = useContext(AppContext);
  let ebody = data.body?.replace(/(<([^>]+)>)/gi, '');
  let ebodyo = ebody?.replace(/\&nbsp;/g, '');

  let abody = data.arabic_body?.replace(/(<([^>]+)>)/gi, '');
  let abodyo = abody?.replace(/\&nbsp;/g, '');

  return (
    <View style={styles.container}>
      {/* <DrawerHeader
        title={myContext.setting2name ? 'إعادة مال' : data.page_title}
        goBack={goBack}
      /> */}

      <MainHeader home={false} navigate={navigate} />
      <ScrollView style={styles.body}>
        <Text style={styles.heading}>
          {myContext.setting2name ? data.arabic_title : data.page_title}
        </Text>
        <Text style={styles.description}>
          {myContext.setting2name ? abodyo : ebodyo}
        </Text>
      </ScrollView>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: whiteColor,
  },

  body: {
    padding: '10@s',
  },

  heading: {
    color: blackColor,
    marginBottom: '30@s',
    fontSize: '14@s',
    fontWeight: 'bold',
  },
  description: {
    marginBottom: '30@s',
    color: blackColor,
  },
});

export default CareerScreen;
