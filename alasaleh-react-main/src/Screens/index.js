import HomeScreen from './HomeScreen';
import CartScreen from './CartScreen';
import RegisterScreen from './RegisterScreen';
import LoginScreen from './LoginScreen';
import ProductByCategoryScreen from './ProductByCategoryScreen';
import ProductCardListScreen from './ProductCardListScreen';
import CategoryListScreen from './CategoryListScreen';
import AboutUsScreen from './AboutUsScreen';
import RefundScreen from './RefundScreen';
import PrivacyPloicyScreen from './PrivacyPloicyScreen';
import CareerScreen from './CareerScreen';
import ShippingPolicyScreen from './ShippingPolicyScreen';
import HelpAndSupport from './HelpAndSupport';
import NewsLetterScreen from './NewsLetterScreen';
import SavedScreen from './SavedScreen';
import Refferel from './Refferel';
import MyAcountScreen from './MyAcountScreen';
import OTPScreen from './OTPScreen';
import UpdatePasswordScreen from './UpdatePasswordScreen';
import OrderListScreen from './OrderListScreen';
import WhishListScreen from './WhishListScreen';
import ProfileScreen from './ProfileScreen';
import PaymentMethodScreen from './PaymentMethodScreen';
import BillingInformationScreen from './BillingInformationScreen';
import BillingInvoiceScreen from './BillingInvoiceScreen';
import MyOrderScreen from './MyOrderScreen';
import CreditCardScreen from './CreditCardScreen';
import MapViewScreen from './MapViewScreen';
import SplashScreen from './SplashScreen';
import ManageCreditCard from './ManageCreditCard';
import AddMobileScreen from './AddMobileScreen';
import OrderDetailsModal from './OrderDetailsModal';
import OrderIdModal from './OrderIdModal';
import ShippingMapScreen from './ShippingMapScreen';
import TermsAndContionScreen from './TermsAndContionScreen';
import ForgotPasswordScreen from './ForgotPasswordScreen';
import CartDeleteModal from './CartDeleteModal';
import WhyOrderCancelModal from './WhyOrderCancelModal';
import StartModal from './StartModal';
import config from '../Utils/config';

export {
  HomeScreen,
  CartScreen,
  RegisterScreen,
  LoginScreen,
  ProductByCategoryScreen,
  ProductCardListScreen,
  CategoryListScreen,
  AboutUsScreen,
  RefundScreen,
  PrivacyPloicyScreen,
  CareerScreen,
  ShippingPolicyScreen,
  HelpAndSupport,
  NewsLetterScreen,
  SavedScreen,
  Refferel,
  MyAcountScreen,
  OTPScreen,
  UpdatePasswordScreen,
  OrderListScreen,
  WhishListScreen,
  ProfileScreen,
  PaymentMethodScreen,
  BillingInformationScreen,
  BillingInvoiceScreen,
  MyOrderScreen,
  CreditCardScreen,
  MapViewScreen,
  SplashScreen,
  ManageCreditCard,
  AddMobileScreen,
  OrderDetailsModal,
  OrderIdModal,
  ShippingMapScreen,
  TermsAndContionScreen,
  ForgotPasswordScreen,
  CartDeleteModal,
  WhyOrderCancelModal,
  StartModal,
};
