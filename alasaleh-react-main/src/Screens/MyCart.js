import React, {useState, useContext} from 'react';
import {View, Text, FlatList, Alert} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {ScaledSheet} from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {DrawerHeader, MainHeader, MyCartCard} from '../Components';
import {blackColor, greenColor, whiteColor} from '../Theme/colors';
import AppContext from '../Utils/AppContext';
import {TOTAL_AMOUNT} from '../Utils/ComonWords';
import Storage from '../Utils/storage';
import config from '../Utils/config';
import Toast from 'react-native-simple-toast';
import CartDeleteModal from './CartDeleteModal';

function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}
const MyCart = ({
  goBack,
  navigate,
  list,
  totalprice,
  updateCart,
  cartToken,
  count,
}) => {
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };
  const add = (item, c) => {
    // let op = item.price / item.Quantity;
    console.log('item', item);
    if (!item.dq) {
      item.dq = 1;
    }

    if (c == false) {
      item.Quantity = parseInt(item.Quantity) + 1;
      item.Qun = item.Qun + 1;
      item.dq = item.dq + 1;
      if (item.enable_free_product_option === 1) {
        if (
          item.Quantity === item.no_of_products_buy ||
          item.Qun === item.no_of_products_buy
        ) {
          item.Qun = 0;

          item.Quantity = parseInt(item.Quantity) + item.no_of_free_products;
        }
        // else if (item.Quantity % item.no_of_products_buy !== 0) {
        //   item.Quantity = parseInt(item.Quantity) + 1;
        // }
      }

      // item.price = op * item.Quantity;
      // console.log('itemsssswwwww', item);
      updateCart(item, true);
      forceUpdate();
    } else if (c) {
      if (item.Quantity > 1) {
        // let price = item.price / item.Quantity;
        // item.price = item.price - price;

        // item.Qun = item.Qun + 1;
        if (item.Qun < 0) {
          item.Qun = 0;
        }
        item.dq = item.dq - 1;

        if (item.enable_free_product_option === 1) {
          if (item.Quantity === item.no_of_products_buy || item.Qun === 0) {
            item.Qun = 1;
            item.Quantity = parseInt(item.Quantity) - 1;

            item.Quantity = parseInt(item.Quantity) - item.no_of_free_products;
          } else {
            item.Qun = 0;
            item.Quantity = parseInt(item.Quantity) - 1;
          }
          // else if (item.Quantity % item.no_of_products_buy !== 0) {
          //   item.Quantity = parseInt(item.Quantity) - 1;
          // }
        } else {
          item.Quantity = parseInt(item.Quantity) - 1;
        }
      } else if (item.Quantity == 1) {
        Alert.alert(
          'Alert',
          'Are you sure to remove this item from Cart?',
          [
            {
              text: 'Cancel',
              onPress: () => console.log('Cancel Pressed'),
              style: 'cancel',
            },
            {
              text: 'OK',
              onPress: () => {
                delete_product(item);
              },
            },
          ],
          {cancelable: false},
        );
      }
      updateCart(item, false);
      forceUpdate();
    }
  };
  const delete_product = async (item) => {
    await fetch(`${config.apiUrl}/carts/remove_item_from_cart`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        cartKey: cartToken.cartKey,
        productID: item.productID,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        Toast.show(responseJson.message);
        navigate('Home');
        // responseJson['item_in_cart'].map(item => this.getProducts(item.Name))
      })
      .catch((e) => {
        console.log('error is', e);
      });
  };
  const sub = (item) => {
    alert('aay');
    let op = item.price / item.Quantity;
    item.Quantity = parseInt(item.Quantity) - 1;
    item.price = op / item.Quantity;
    forceUpdate();
  };
  const renderItem = ({item}) => (
    <MyCartCard
      title={item.Name}
      price={item.price}
      item={item}
      updateCart={updateCart}
      add={add}
      sub={sub}
      img={item.image}
      cartToken={cartToken}
      navigate={navigate}
    />
  );

  const navigationHeader = async () => {
    let isLogin = null;
    await Storage.retrieveData('user').then((resp) => {
      isLogin = resp;
    });

    if (isLogin) {
      if (totalprice >= 20) {
        navigate('BillingInformation');
      } else {
        if (myContext.setting2name) {
          Toast.show('يجب أن يكون الطلب 20 دولارًا على الأقل');
        } else {
          Toast.show('Order Must Be of atleast $20');
        }
      }
    } else {
      navigate('Login');
    }
  };

  return (
    <View style={styles.container}>
      <MainHeader home={false} navigate={navigate} count={count} />

      <View style={{flex: 1}}>
        <View style={{flex: 1}}>
          {true ? (
            <View style={{flex: 1}}>
              <FlatList
                data={list}
                renderItem={renderItem}
                keyExtractor={(item) => item.id}
              />
              <View>
                <View
                  style={
                    myContext.setting2name
                      ? styles.ammountContainerUrdu
                      : styles.ammountContainer
                  }>
                  <Text style={styles.ammountText}>
                    {myContext.setting2name ? TOTAL_AMOUNT : 'Sub Total'}
                  </Text>
                  <Text style={styles.ammountText}>
                    ${totalprice.toFixed(2)}
                  </Text>
                </View>
                <View style={{padding: 10}}>
                  <TouchableOpacity
                    onPress={() => navigationHeader()}
                    style={styles.checkOutContainer}>
                    <View style={styles.checkoutTextContaienr}>
                      <Text style={styles.checkoutBtntext}>
                        {myContext.setting2name ? 'إكمال الشراء' : 'Checkout'}
                      </Text>
                      <Text style={styles.checkoutBtntext}>
                        ${totalprice.toFixed(2)}
                      </Text>
                    </View>
                    <View style={styles.divider}></View>
                    <View style={styles.arrowContainer}>
                      <Icon size={24} name="arrow-right" color={whiteColor} />
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          ) : (
            <View
              style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <Icon name="frown" size={30} color={blackColor} />
              <Text style={styles.textColor}>
                {myContext.setting2name ? 'البطاقه خاليه' : 'Cart is empty'}
              </Text>
            </View>
          )}
        </View>
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },

  textColor: {
    color: blackColor,
    fontSize: '13@s',
    marginTop: '10@s',
  },

  myCartBody: {
    flex: 1,
  },

  ammountContainer: {
    flexDirection: 'row',
    padding: '10@s',
    justifyContent: 'space-between',
    backgroundColor: whiteColor,
    borderTopColor: blackColor,
    borderTopWidth: 2,
    borderBottomColor: blackColor,
    borderBottomWidth: 1,
  },

  ammountContainerUrdu: {
    flexDirection: 'row-reverse',
    padding: '10@s',
    justifyContent: 'space-between',
    backgroundColor: whiteColor,
    borderTopColor: blackColor,
    borderTopWidth: 2,
    borderBottomColor: blackColor,
    borderBottomWidth: 1,
  },

  ammountText: {
    color: blackColor,
    fontWeight: 'bold',
    fontSize: '14@s',
  },

  checkOutContainer: {
    flexDirection: 'row',
    backgroundColor: greenColor,
    borderRadius: '5@s',
  },

  checkoutTextContaienr: {
    padding: '13@s',
    width: '75%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  divider: {
    height: '100%',
    width: '1@s',
    backgroundColor: blackColor,
  },

  arrowContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '25%',
  },

  checkoutBtntext: {
    color: whiteColor,
    fontWeight: 'bold',
    fontSize: '12@s',
  },
});

export default MyCart;
