import React, {useContext, useState} from 'react';
import {Text, View, TouchableOpacity, ScrollView} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {DrawerHeader, Input, MainHeader} from '../Components';
import {blackColor, greenColor, whiteColor} from '../Theme/colors';
import AppContext from '../Utils/AppContext';

function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const HelpAndSupport = ({goBack, handleChange, submit, navigate}) => {
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };
  return (
    <View style={styles.container}>
      {/* <DrawerHeader
        title={myContext.setting2name ? 'مساعدة' : 'Help & Support'}
        goBack={goBack}
      /> */}

      <MainHeader home={false} navigate={navigate} />
      <ScrollView style={{flex: 1}}>
        <View style={styles.headingContainer}>
          <View
            style={{
              paddingLeft: 20,
              paddingRight: 20,
              paddingTop: 10,
              paddingBottom: 20,
            }}>
            <Text style={styles.headingContainerText}>
              {myContext.setting2name ? 'اسألنا سؤال' : 'Ask us question'}
            </Text>
          </View>
          <Input
            secureTextEntry={false}
            type="text"
            label={myContext.setting2name ? 'اسمك*' : 'Your Name*'}
            name="name"
            onChangeText={handleChange}
          />
          <Input
            secureTextEntry={false}
            type="text"
            label={
              myContext.setting2name
                ? 'العنوان البريد الإلكتروني*'
                : 'E-mail Address'
            }
            placeHolder="example@example.com"
            name="email"
            onChangeText={handleChange}
          />
          <Input
            secureTextEntry={false}
            type="text"
            label={myContext.setting2name ? 'هاتف' : 'Phone'}
            placeHolder="Enter Phone"
            name="phone"
            onChangeText={handleChange}
          />

          <Input
            secureTextEntry={false}
            type="text"
            label={myContext.setting2name ? 'رسالة' : 'Message'}
            placeHolder="Enter Message"
            customeStyle
            name="message"
            onChangeText={handleChange}
          />

          <View style={styles.submitBtnContainer}>
            <TouchableOpacity style={styles.submitBtn} onPress={() => submit()}>
              <Text style={styles.submitText}>
                {myContext.setting2name ? 'إرسال' : 'Submit'}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: whiteColor,
  },

  headingContainer: {
    color: blackColor,
    marginBottom: '30@s',
    fontWeight: 'bold',
  },

  submitBtnContainer: {
    paddingTop: '15@s',
    paddingLeft: '30@s',
    paddingRight: '30@s',
  },
  submitBtn: {
    backgroundColor: greenColor,
    alignItems: 'center',
    padding: '10@s',
    borderRadius: '5@s',
  },

  submitText: {
    color: whiteColor,
    fontSize: '14@s',
  },
});

export default HelpAndSupport;
