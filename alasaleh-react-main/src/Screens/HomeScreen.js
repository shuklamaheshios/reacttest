import React, {useState, useContext, useCallback,useEffect} from 'react';
import {
  View,
  ScrollView,
  Image,
  Text,
  TouchableOpacity,
  Alert,
  Linking,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';

import {MainHeader, MainSlider, SelectLangunageModal} from '../Components';
import {blackColor, greenColor, lightGray, grayColor} from '../Theme/colors';

import payment from '../Assets/images/payment.png';
import AppContext from '../Utils/AppContext';
import facebook from '../Assets/images/facebook.jpg';
import instagram from '../Assets/images/new_instagram.jpg';
import youtube from '../Assets/images/new_youtube.png';

import usps from '../Assets/images/usps.png';
import ups from '../Assets/images/ups.jpg';
import fedex from '../Assets/images/fedex.png';
import ontrac from '../Assets/images/ontrac.png';

import sliderImage from '../Assets/images/sliderImage.jpg';
import CardsContaiener from '../Components/CardsContainer';
import {OrderDetailsModal, OrderIdModal} from '.';
import StartModal from './StartModal';
import Storage from '../Utils/storage';


const HomeScreen = ({
  navigate,
  parentCategory,
  flashOffersList,
  homePageProducts,
  update,
  count,
  addLocalCart,
  addinWishL,
  removeWishL,
  wishCount,
}) => {







  const myContext = useContext(AppContext);
  const supportedURLF = 'https://www.facebook.com/Alasalehonlineshop';
  const supportedURLY =
    'https://www.youtube.com/channel/UCbrO9KLeTX8oIogycOMFjzw';
  const supportedURLI = 'https://www.instagram.com/alasaleh.marketplace/';
  const OpenURLButtonF = ({url, children}) => {
    const handlePress = useCallback(async () => {
      // Checking if the link is supported for links with custom URL scheme.
      const supported = await Linking.canOpenURL(url);

      if (supported) {
        // Opening the link with some app, if the URL scheme is "http" the web link should be opened
        // by some browser in the mobile
        await Linking.openURL(url);
      } else {
        Alert.alert(`Don't know how to open this URL: ${url}`);
      }
    }, [url]);
    return (
      <TouchableOpacity onPress={handlePress}>
        <Image
          resizeMethod="scale"
          resizeMode="stretch"
          style={styles.socialImage}
          source={facebook}
        />
      </TouchableOpacity>
    );
  };
  const OpenURLButtonY = ({url, children}) => {
    const handlePress = useCallback(async () => {
      // Checking if the link is supported for links with custom URL scheme.
      const supported = await Linking.canOpenURL(url);

      if (supported) {
        // Opening the link with some app, if the URL scheme is "http" the web link should be opened
        // by some browser in the mobile
        await Linking.openURL(url);
      } else {
        Alert.alert(`Don't know how to open this URL: ${url}`);
      }
    }, [url]);
    return (
      <TouchableOpacity onPress={handlePress}>
        <Image
          resizeMethod="scale"
          resizeMode="stretch"
          style={styles.socialImage}
          source={youtube}
        />
      </TouchableOpacity>
    );
  };
  const OpenURLButtonI = ({url, children}) => {
    const handlePress = useCallback(async () => {
      // Checking if the link is supported for links with custom URL scheme.
      const supported = await Linking.canOpenURL(url);

      if (supported) {
        // Opening the link with some app, if the URL scheme is "http" the web link should be opened
        // by some browser in the mobile
        await Linking.openURL(url);
      } else {
        Alert.alert(`Don't know how to open this URL: ${url}`);
      }
    }, [url]);
    return (
      <TouchableOpacity onPress={handlePress}>
        <Image
          resizeMethod="scale"
          resizeMode="stretch"
          style={styles.socialImage}
          source={instagram}
        />
      </TouchableOpacity>
    );
  };
  return (
    <View style={{flex: 1}}>
      <MainHeader
        home={true}
        navigate={navigate}
        count={count}
        wishCount={wishCount}
      />
      <ScrollView style={{flex: 1}}>
        <MainSlider homePageProducts={homePageProducts} />
        <View style={styles.container}>
          <View
            style={[
              myContext.setting2name
                ? styles.itemContainerUrdu
                : styles.itemContainer,
            ]}>
            {parentCategory.map((item, index) => (
              <TouchableOpacity
                key={index}
                onPress={() => navigate('Category', item.id)}
                style={styles.itemList}>
                <Image
                  source={{
                    uri: `${config.url}/public` + item.img_path,
                  }}
                  style={styles.image}
                />
                <Text style={styles.itemListText}>
                  {[myContext.setting2name ? item.name_arabic : item.name]}
                </Text>
              </TouchableOpacity>
            ))}
          </View>

          <CardsContaiener
            stock={false}
            title={myContext.setting2name ? 'عروض فلاش' : 'Flash Offers'}
            navigate={navigate}
            flashOffersList={homePageProducts.promotional_products}
            update={update}
            addLocalCart={addLocalCart}
            addinWishL={addinWishL}
            removeWishL={removeWishL}
            count={count}
          />

          {myContext.setting2name ? (
            <View style={styles.imageContainer}>
              <Image
                style={styles.imageFooter}
                source={{
                  uri: `${config.url}/public/${homePageProducts?.ads_home_ar?.ads_0?.Image}`,
                }}
              />
            </View>
          ) : (
            <View style={styles.imageContainer}>
              <Image
                style={styles.imageFooter}
                source={{
                  uri: `${config.url}/public/${homePageProducts.ads_home_en?.ads_0?.Image}`,
                }}
              />
              <Image
                style={styles.imageFooter}
                source={{
                  uri: `${config.url}/public/${homePageProducts?.ads_home_en?.ads_1?.Image}`,
                }}
              />
            </View>
          )}

          <CardsContaiener
            stock={false}
            title={
              myContext.setting2name ? 'عروض ترويجية' : 'Promotional Offers'
            }
            navigate={navigate}
            flashOffersList={homePageProducts.featured_products}
            addLocalCart={addLocalCart}
            addinWishL={addinWishL}
            removeWishL={removeWishL}
            update={update}
          />
        </View>

        <StartModal />

        <View style={styles.socialImageContainer}>
          <OpenURLButtonF url={supportedURLF} />
          <OpenURLButtonI url={supportedURLI} />
          <OpenURLButtonY url={supportedURLY} />

          {/* <Image
            resizeMethod="scale"
            resizeMode="stretch"
            style={styles.socialImage}
            source={instagram}
          />
          <Image
            resizeMethod="scale"
            resizeMode="stretch"
            style={styles.socialImage}
            source={youtube}
          /> */}
        </View>

        <View style={styles.copyRightContainer}>
          {myContext.setting2name ? (
            <Text style={{fontSize: 13, color: 'grey'}}>
              {' '}
              {'\u00A9'} جميع الحقوق محفوظة لموقع الاصالة 2020
            </Text>
          ) : (
            <Text style={{fontSize: 13, color: 'grey'}}>
              {' '}
              {'\u00A9'} Copy Right 2020 Al Asalah. All Rights Reserved
            </Text>
          )}
          <View style={styles.companyContainer}>
            <Image source={usps} style={styles.companyLogo} />
            <Image source={ups} style={styles.companyLogo} />
            <Image source={fedex} style={styles.companyLogo} />
            <Image source={ontrac} style={styles.companyLogo} />
          </View>
          <View style={styles.payMentContainer}>
            <Image
              resizeMethod="resize"
              resizeMode="contain"
              style={styles.paymentImage}
              source={payment}
            />
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    padding: '8@s',
    marginTop: '5@s',
  },
  itemContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: '5@s',
    flexWrap: 'wrap',
  },

  itemContainerUrdu: {
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    marginBottom: '5@s',
    flexWrap: 'wrap',
  },

  itemContainerUrdu: {
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    marginBottom: '5@s',
    flexWrap: 'wrap',
  },
  image: {
    width: '65@s',
    height: '65@s',
    borderRadius: 50,
    borderColor: greenColor,
    borderWidth: 2,
  },
  itemList: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: 7,
  },
  itemListText: {
    marginTop: 7,
    width: '70@s',
    textAlign: 'center',
    color: 'grey',
    fontSize: '10@s',
    fontWeight: '600',
  },

  copyRightContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: '10@s',
  },

  payMentContainer: {
    flexDirection: 'row',
    paddingLeft: '20@s',
    paddingRight: '20@s',
  },

  paymentImage: {
    height: '25@s',
    // resizeMode: 'contain',
    marginTop: '10@s',
    marginBottom: '10@s',
  },

  imageContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: '10@s',
    marginTop: '-15@s',
  },

  imageFooter: {
    height: '100@s',
    width: '49%',
  },

  socialImage: {
    height: '35@s',
    width: '35@s',
    borderRadius: 50,
    marginLeft: '5@s',
    marginRight: '5@s',
  },

  socialImageContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: '10@s',
    marginTop: '10@s',
  },

  companyContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '10@s',
    marginBottom: '10@s',
  },

  companyLogo: {
    width: '40@s',
    height: '40@s',
    marginLeft: '10@s',
    marginRight: '10@s',
  },
});

export default HomeScreen;
