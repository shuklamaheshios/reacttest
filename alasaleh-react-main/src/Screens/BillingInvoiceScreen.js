import React, {useState, useContext} from 'react';
import {Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import TextBox from 'react-native-password-eye';
import {ScaledSheet} from 'react-native-size-matters';
import {DrawerHeader, MainHeader} from '../Components';
import {blackColor, greenColor, whiteColor} from '../Theme/colors';
import AppContext from '../Utils/AppContext';
import {SHIPPING_AMOUNT, TAX} from '../Utils/ComonWords';

function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const BillingInvoiceScreen = ({goBack, navigate, inovice, SaveOrder}) => {
  console.log('inovice cont', inovice);
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };

  const lang = myContext.setting2name;
  return (
    <View style={styles.container}>
      {/* <DrawerHeader
        title={myContext.setting2name ? 'فاتورة الفواتير' : 'Billing Invoice'}
        goBack={goBack}
      /> */}

      <MainHeader home={false} navigate={navigate} />
      <View style={{padding: 10}}>
        <Text style={{color: blackColor, fontSize: 20, fontWeight: 'bold'}}>
          Billing Invoice
        </Text>
      </View>
      <View style={styles.header}>
        <Text style={styles.headerText}>
          {myContext.setting2name ? 'فاتورة' : 'Invoice'}
        </Text>
      </View>

      <View style={styles.body}>
        <View style={styles.bodyLeft}>
          <Text style={styles.bodyHeading}>
            {myContext.setting2name ? 'نوع' : 'Type'}
          </Text>
          <Text style={styles.bodyItem}>
            {' '}
            {myContext.setting2name
              ? 'المجموع الفرعي'
              : 'Cart Subtotal'}{' '}
          </Text>
          <Text style={styles.bodyItem}>
            {lang ? SHIPPING_AMOUNT : 'Shipping Amount'}
          </Text>
          <Text style={styles.bodyItem}>{lang ? TAX : 'Tax'}</Text>
          <Text style={styles.bodyItem}>
            {lang ? 'خصم القسيمة' : 'Coupon Discount'}
          </Text>
          <Text style={styles.bodyItem}>{lang ? 'توصيل' : 'Delivery'}</Text>
          <Text style={styles.bodyItem}>
            {lang ? 'المسافة الكلية' : 'Total Distance'}
          </Text>
        </View>

        <View style={styles.bodyLeft}>
          <Text style={styles.bodyHeading}>
            {myContext.setting2name ? 'كمية' : 'Amount'}
          </Text>
          <Text style={styles.bodyItem}>${inovice.subtotal}</Text>
          <Text style={styles.bodyItem}>${inovice.total_shipping_amount}</Text>
          <Text style={styles.bodyItem}>${inovice.total_tax}</Text>
          <Text style={styles.bodyItem}>${inovice.coupon_discount}</Text>
          <Text style={styles.bodyItem}>{inovice.delivery_day}</Text>
          <Text style={styles.bodyItem}>{inovice.distance}</Text>
        </View>
        <View></View>
      </View>

      <View style={styles.amountContainer}>
        <View style={styles.bodyLeft}>
          <Text style={styles.amountHeading}>
            {myContext.setting2name
              ? 'إجمالي المبلغ المستحق'
              : 'Total Payable Amount'}
          </Text>
        </View>

        <View style={styles.bodyLeft}>
          <Text style={styles.amountHeading}>${inovice.total_amount}</Text>
        </View>
        <View></View>
      </View>

      <View style={{padding: 30}}>
        <View style={styles.nextBtnContainer}>
          <TouchableOpacity
            style={styles.nextBtn}
            onPress={() => navigate('Payment')}>
            <Text style={styles.nextText}>
              {myContext.setting2name ? 'التالي' : 'Next'}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },

  header: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: '10@s',
  },

  headerText: {
    color: blackColor,
    fontWeight: 'bold',
    fontSize: '16@s',
  },

  body: {
    flexDirection: 'row',
  },

  bodyLeft: {
    width: '50%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  bodyHeading: {
    textAlign: 'center',
    fontSize: '14@s',
    color: blackColor,
    fontWeight: 'bold',
  },
  bodyItem: {
    color: blackColor,
    marginTop: '5@s',
    marginBottom: '5@s',
    fontSize: '13@s',
  },
  amountContainer: {
    marginTop: '15@s',
    flexDirection: 'row',
    borderTopWidth: 1,
    borderTopColor: blackColor,
    borderBottomWidth: 1,
    borderBottomColor: blackColor,
    paddingTop: '10@s',
    paddingBottom: '15@s',
  },

  amountHeading: {
    color: blackColor,
    fontSize: '14@s',
    fontWeight: 'bold',
  },
  nextBtnContainer: {
    padding: '13@s',
  },
  nextBtn: {
    backgroundColor: greenColor,
    alignItems: 'center',
    padding: '10@s',
    borderRadius: '5@s',
  },
  nextText: {
    color: whiteColor,
    fontSize: '14@s',
  },
});

export default BillingInvoiceScreen;
