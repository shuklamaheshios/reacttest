import React, { useState, useContext } from 'react';
import { Image, ScrollView, Text, View } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/FontAwesome';

import DrawerHeader from '../Components/DrawerHeader';
import { blackColor, blueColor, greenColor, lightGray } from '../Theme/colors';

import sliderImage from '../Assets/images/sliderImage.jpg';
import bestPrice from '../Assets/images/best_prices_icon.png';
import greenProfile from '../Assets/images/profile_green_bg.png';
import AppContext from '../Utils/AppContext';
import { MainHeader } from '../Components';
import HTML from 'react-native-render-html';
function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const AboutUsScreen = ({ goBack, navigate, aboutData }) => {
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };
  console.log("About Data:............", aboutData)
  return (
    <View style={styles.container}>
      {/* <DrawerHeader
        title={myContext.setting2name ? 'معلومات عنا' : 'About Us'}
        goBack={goBack}
      /> */}

      <MainHeader home={false} navigate={navigate} />
      <ScrollView>

        <View style={styles.body}>
          <View style={{ width: '90%', alignSelf: 'center' }}>
            <Text style={{
              color: 'black', fontSize: 18, fontWeight: '700', alignSelf: myContext.setting2name ? 'flex-end' : 'flex-start'
            }}>{myContext.setting2name ? aboutData?.arabic_title : aboutData?.page_title}</Text>
          </View>
          <View style={{ width: '90%', alignSelf: 'center', marginTop: 20, marginBottom: 20 }}>
            <HTML html={myContext.setting2name ? aboutData?.arabic_body : aboutData?.body} />
            {/* <Text style={{

            }}>{myContext.setting2name ? aboutData?.arabic_body : aboutData?.body}</Text> */}
          </View>




          <View style={styles.centerLine} />

          <View style={styles.itemHeader}>
            <Text style={styles.itemHeaderHeading}>Contact Us</Text>
            <Text numberOfLines={1} style={styles.itemHeaderDescription}>
              1701 30th St, Backersfield, CA 93301
            </Text>
          </View>

          <View>
            <View style={styles.footerItem}>
              <Icon size={20} name="phone" color="#333333" />
              <Text style={styles.footerItemText}>(844) 4AL-Asaleh</Text>
            </View>

            <View style={styles.footerItem}>
              <Icon size={20} name="phone" color="#333333" />
              <Text style={styles.footerItemText}>2725 425 (844)</Text>
            </View>

            <View style={styles.footerItem}>
              <Icon size={20} name="envelope" color={greenColor} />
              <Text style={styles.envelopText}>rami@alasaleh.com</Text>
            </View>

            <View style={styles.footerItem}>
              <Icon size={20} name="globe" color={blueColor} />
              <Text style={styles.globText}>www.alasaleh.com</Text>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },

  headerImage: {
    width: '100%',
    height: '160@s',
  },
  body: {
    padding: '10@s',
  },
  bodyHeading: {
    color: greenColor,
    fontSize: '14@s',
    fontWeight: 'bold',
  },

  visionContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomColor: lightGray,
    borderBottomWidth: 1,
    paddingBottom: '10@s',
    marginTop: '20@s',
  },
  paragraph: {
    width: '45%',
    color: blackColor,
  },

  centerLine: {
    borderBottomWidth: 1,
    borderBottomColor: lightGray,
    marginTop: 20,
    marginBottom: 20
  },

  visionText: {
    color: blackColor,
    fontSize: '15@s',
    fontWeight: 'bold',
    marginBottom: '5@s',
  },
  itemHeader: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '10@s',
    marginBottom: '10@s',
  },

  itemHeaderHeading: {
    color: blackColor,
    fontWeight: 'bold',
    fontSize: '15@s',
    marginBottom: 15,
  },

  itemHeaderDescription: {
    color: blackColor,
    width: '90%',
    textAlign: 'center',
  },

  aboutImage: {
    height: '50@s',
    width: '50@s',
    borderRadius: 100,
  },

  itemService: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '33.333339%',
    justifyContent: 'center',
  },

  itemServiceContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
  },

  itemServiceHeading: {
    width: '45%',
    marginLeft: '10@s',
    color: greenColor,
    fontWeight: 'bold',
    fontSize: '10@s',
  },

  profileImage: {
    height: '60@s',
    width: '60@s',
    borderRadius: 100,
  },

  profileContainer: {
    marginTop: '10@s',
    borderBottomColor: lightGray,
    borderBottomWidth: 1,
  },

  profileContainerItems: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: '10@s',
    marginBottom: '10@s',
  },
  profileHeading: {
    color: greenColor,
    fontSize: '15@s',
    fontWeight: 'bold',
  },

  headingContainer: {
    marginLeft: '10@s',
  },

  profilePosition: {
    color: '#333333',
  },

  footerItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: '10@s',
    marginBottom: '10@s',
  },

  footerItemText: {
    marginLeft: '10@s',
    fontSize: '14@s',
    color: blackColor,
  },

  envelopText: {
    marginLeft: '10@s',
    fontSize: '14@s',
    color: greenColor,
  },

  globText: {
    marginLeft: '10@s',
    fontSize: '14@s',
    color: blueColor,
  },
});

export default AboutUsScreen;
