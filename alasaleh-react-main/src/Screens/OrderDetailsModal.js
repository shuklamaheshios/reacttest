import React, {useState} from 'react';
import {View, Modal, Text, FlatList, TouchableOpacity} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {DrawerHeader, MyOrderCard, OrderDetailsCard} from '../Components';
import {grayColor, greenColor, whiteColor} from '../Theme/colors';

const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    reference_no: 'Aljazera ',
    shipping_cost: 126,
    total_tax: 12,
    coupon_discount: 100,
    total_price: 1230,
    updated_at: '20/10/2020',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    reference_no: 'Aljazera ',
    total_tax: 12,
    coupon_discount: 100,
    total_price: 2000,
    updated_at: '20/10/2020',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    reference_no: 'Aljazera ',
    shipping_cost: 125,
    total_tax: 12,
    coupon_discount: 100,
    total_price: 3000,
    updated_at: '20/10/2020',
  },
  {
    id: '1',
    reference_no: 'Aljazera ',
    shipping_cost: 124,
    total_tax: 12,
    total_price: 4000,
    updated_at: '20/10/2020',
  },
  {
    id: '586',
    reference_no: 'Aljazera ',
    shipping_cost: 123,
    total_tax: 12,
    coupon_discount: 100,
    total_price: 5000,
    updated_at: '20/10/2020',
  },
];

const OrderDetailsModal = ({modalVisible, setModalVisible, id, list}) => {
  const renderItem = ({item}) => <OrderDetailsCard list={item} />;
  return (
    <View style={styles.centeredView}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
        }}>
        <View style={styles.modalViewMain}>
          <View style={styles.modalView}>
            <View style={styles.orderDetailsHeader}>
              <Text></Text>
              <Text
                style={{color: whiteColor, fontWeight: 'bold', fontSize: 20}}>
                Order Details
              </Text>
              <TouchableOpacity
                onPress={() => setModalVisible(false)}
                style={{alignSelf: 'flex-end'}}>
                <Text style={{color: whiteColor, fontSize: 16}}>Close</Text>
              </TouchableOpacity>
            </View>
            <View style={{padding: 10, flex: 1}}>
              <FlatList
                showsVerticalScrollIndicator={false}
                renderItem={renderItem}
                data={list}
              />
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = ScaledSheet.create({
  centeredView: {
    // flex: 1,
  },

  orderDetailsHeader: {
    backgroundColor: greenColor,
    padding: '10@s',
    alignItems: 'center',
    justifyContent: 'space-between',

    flexDirection: 'row',
  },
  modalViewMain: {
    flex: 1,
  },
  modalView: {
    flex: 1,
    margin: 20,
    backgroundColor: '#E5E5E5',
    borderRadius: 20,
    padding: 0,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
});

export default OrderDetailsModal;
