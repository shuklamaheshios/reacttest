import React, {useState, useContext} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {ScaledSheet} from 'react-native-size-matters';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';

import {greenColor, whiteColor} from '../Theme/colors';
import AppContext from '../Utils/AppContext';

function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const MapViewScreen = ({
  navigate,
  goBack,
  handleChange,
  getLocation,
  la,
  ln,
}) => {
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };
  const [lat, setLat] = useState(la);
  const [lng, setLng] = useState(ln);
  const [region, setRegion] = React.useState({
    latitude: lat,
    longitude: lng,
    latitudeDelta: 1.0922,
    longitudeDelta: 1.0421,
  });
  const setLatLng = () => {
    handleChange('lat', lat);
    handleChange('lng', lng);
    getLocation(lat, lng);
    navigate('Profile', {lat: lat, lng: lng});
  };

  return (
    <View style={[styles.container, myStyle.myContainer]}>
      <View style={{flex: 1}}>
        <MapView
          ref={(ref) => (myMap = ref)}
          provider={PROVIDER_GOOGLE}
          style={[styles.map, myStyle.myMap]}
          showsUserLocation={true}
          showsMyLocationButton={true}
          // mapType="standar"
          loadingIndicatorColor={greenColor}
          loadingEnabled={true}
          region={{
            latitude: lat,
            longitude: lng,
            latitudeDelta: 1.0922,
            longitudeDelta: 1.0421,
          }}
          onPress={(e, sa) => {
            setLat(e.nativeEvent.coordinate.latitude);
            setLng(e.nativeEvent.coordinate.longitude);
          }}
          initialRegion={{
            latitude: lat,
            longitude: lng,
            latitudeDelta: 1.0922,
            longitudeDelta: 1.0421,
          }}>
          <Marker
            coordinate={{
              latitude: lat,
              longitude: lng,
            }}
            draggable
            pinColor={greenColor}
          />
        </MapView>

        <View style={styles.header}>
          <GooglePlacesAutocomplete
            placeholder="Search"
            fetchDetails={true}
            enablePoweredByContainer={false}
            onPress={(data, details = null) => {
              // 'details' is provided when fetchDetails = true
              setLat(details.geometry.location.lat);
              setLng(details.geometry.location.lng);
            }}
            query={{
              key: 'AIzaSyCvypE2o4uA0pvGQxwY4KqfXryigcrDqTQ',
              language: 'en',
            }}
          />
        </View>

        <View style={styles.footer}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TouchableOpacity style={styles.socialFaceButton} onPress={goBack}>
              <Icon name="times" size={22} color={whiteColor} />
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  width: '100%',
                }}>
                <Text style={{color: whiteColor}}>
                  {myContext.setting2name ? 'إلغاء' : 'Cancel'}
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.socialFaceButton}
              onPress={() => setLatLng()}>
              <Icon name="map-marker" size={22} color={whiteColor} />
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  width: '100%',
                }}>
                <Text style={{color: whiteColor}}>
                  {myContext.setting2name ? 'أضف الموقع' : 'Add Location'}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
  map: {
    flex: 1,
    position: 'relative',
  },

  footer: {
    position: 'absolute',
    bottom: '20@s',
    left: '0@s',
    width: '100%',
    height: '50@s',
    padding: '10@s',
  },
  nextBtnContainer: {
    padding: '13@s',
  },
  nextBtn: {
    backgroundColor: greenColor,
    alignItems: 'center',
    padding: '10@s',
    borderRadius: '5@s',
  },
  nextText: {
    color: whiteColor,
    fontSize: '14@s',
  },
  socialFaceButton: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: greenColor,
    width: '48%',
    padding: '10@s',
    justifyContent: 'space-between',
    borderRadius: '4@s',
  },

  header: {
    position: 'relative',
    width: '90%',
    backgroundColor: whiteColor,
    flexDirection: 'row',
    alignSelf: 'center',
    padding: '10@s',
    top: '20@s',
  },

  chipItem: {
    flexDirection: 'row',
    backgroundColor: whiteColor,
    borderRadius: '20@s',
    padding: '8@s',
    width: '100%',
    paddingVertical: '20@s',
    marginRight: '30@s',
    marginVertical: '5@s',
    marginLeft: '7@s',
  },
});

const myStyle = StyleSheet.create({
  myContainer: {
    ...StyleSheet.absoluteFillObject,
  },

  myMap: {
    ...StyleSheet.absoluteFillObject,
  },
});

export default MapViewScreen;
