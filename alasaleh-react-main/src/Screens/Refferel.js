import React, {useState, useContext} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  Modal,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {DrawerHeader, Input, MainHeader} from '../Components';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {
  blackColor,
  greenColor,
  grayColor,
  lightGray,
  whiteColor,
} from '../Theme/colors';
import AppContext from '../Utils/AppContext';
function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}
const Refferel = ({navigate, goBack, sendreferal, handlechange}) => {
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };
  const [modalVisible, setModalVisible] = useState(false);
  return (
    <View style={styles.container}>
      <Modal
        visible={modalVisible}
        transparent={true}
        style={{height: 50, width: 100, padding: 0}}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <View style={styles.modalView}>
            <View style={{padding: 15}}>
              <Text style={styles.moalText}>
                Email will be sent from example@example.com
              </Text>
            </View>

            <Input
              name="email"
              onChangeText={handlechange}
              label="To"
              placeHolder="Enter your friend email address"
            />
            <Input
              name="subject"
              onChangeText={handlechange}
              label="Subject"
              placeHolder="$5 will be sent to your friend"
            />

            <View style={{padding: 15}}>
              <Text style={{marginBottom: 5}}>Note: </Text>
              <TextInput
                placeholder="Your personal message goes here Offer link"
                style={styles.modalInput}
                onChangeText={(value) => handlechange('note', value)}
              />
            </View>

            <View style={styles.nextBtnContainer}>
              <TouchableOpacity
                onPress={() => {
                  sendreferal(), setModalVisible(!modalVisible);
                }}
                style={styles.nextBtn}>
                <Text style={styles.nextText}>Send</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
      {/* <DrawerHeader
        title={myContext.setting2name ? 'الولاية' : 'Refferel'}
        goBack={goBack}
      /> */}

      <MainHeader navigate={navigate} />
      <View style={styles.body}>
        <Text style={styles.refferlHeading}>
          {myContext.setting2name
            ? 'أعط 5 دولارات تحصل على خمسة دولارات'
            : 'GIVE $5, GET $5'}
        </Text>
        <Text style={styles.paragraphColor}>
          {myContext.setting2name
            ? 'امنح الأصدقاء 5 دولارات كخصم على طلبهم الأول ، وستحصل على 5 دولارات عند إجراء عملية شراء'
            : "Give Friends $5 off their first order, and you'll get $5 when they make a purchase"}
        </Text>

        <View>
          <TouchableOpacity
            style={
              myContext.setting2name ? styles.itemListUrdu : styles.itemList
            }
            onPress={() => sendreferal()}>
            <View style={styles.iconCOntainer}>
              <Icon name="envelope" color={whiteColor} size={26} solid />
            </View>
            <View style={styles.line}></View>
            <View
              style={[
                myContext.setting2name
                  ? styles.emailContainerUrdu
                  : styles.emailContainer,
              ]}>
              <Text style={styles.shareBtnColor}>
                {myContext.setting2name
                  ? 'دعوة عن طريق البريد الإلكتروني'
                  : 'Share Via Email'}
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={[
              myContext.setting2name ? styles.itemListUrdu : styles.itemList,
              styles.facebookBackground,
            ]}>
            <View style={styles.iconCOntainer}>
              <Icon name="facebook-f" color={whiteColor} size={26} />
            </View>
            <View style={styles.line}></View>
            <View
              style={[
                myContext.setting2name
                  ? styles.emailContainerUrdu
                  : styles.emailContainer,
              ]}>
              <Text style={styles.shareBtnColor}>
                {myContext.setting2name
                  ? 'مشاركة على الفيسبوك'
                  : 'Share on Facebook'}
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={[
              myContext.setting2name ? styles.itemListUrdu : styles.itemList,
              styles.messageback,
            ]}>
            <View style={styles.iconCOntainer}>
              <Icon name="facebook-messenger" color={whiteColor} size={26} />
            </View>
            <View style={styles.line}></View>
            <View
              style={
                myContext.setting2name
                  ? styles.emailContainerUrdu
                  : styles.emailContainer
              }>
              <Text style={styles.shareBtnColor}>
                {myContext.setting2name
                  ? 'مشاركة في الرسالة'
                  : 'Share on Message'}
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={[
              myContext.setting2name ? styles.itemListUrdu : styles.itemList,
              styles.linkBack,
            ]}>
            <View style={styles.iconCOntainer}>
              <Icon name="share-alt" color={whiteColor} size={26} />
            </View>
            <View style={styles.line}></View>
            <View
              style={
                myContext.setting2name
                  ? styles.emailContainerUrdu
                  : styles.emailContainer
              }>
              <Text style={styles.shareBtnColor}>
                {myContext.setting2name ? 'شارك عبر الرابط' : 'Share via Link'}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },

  body: {
    flex: 1,
    padding: '10@s',
  },

  refferlHeading: {
    fontSize: '18@s',
    fontWeight: 'bold',
    color: grayColor,
  },

  paragraphColor: {
    color: grayColor,
    fontSize: '12@s',
    marginTop: '5@s',
  },

  itemList: {
    backgroundColor: '#ff9700',
    borderRadius: '5@s',
    marginTop: '10@s',
    marginBottom: '10@s',
    flexDirection: 'row',
  },

  itemListUrdu: {
    backgroundColor: '#ff9700',
    borderRadius: '5@s',
    marginTop: '10@s',
    marginBottom: '10@s',
    flexDirection: 'row-reverse',
  },

  line: {
    height: '100%',
    width: '1@s',
    backgroundColor: blackColor,
  },

  iconCOntainer: {
    width: '20%',
    justifyContent: 'center',
    alignItems: 'center',
    padding: '10@s',
  },

  emailContainer: {
    alignItems: 'center',
    paddingLeft: '20@s',
    justifyContent: 'center',
  },

  emailContainerUrdu: {
    alignItems: 'center',
    paddingRight: '20@s',
    justifyContent: 'center',
  },

  shareBtnColor: {
    color: whiteColor,
    fontSize: '14@s',
  },

  facebookBackground: {
    backgroundColor: '#3b5999',
  },

  messageback: {
    backgroundColor: '#0084ff',
  },

  linkBack: {
    backgroundColor: '#6a1b9a',
  },

  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 2,
    padding: 0,
    shadowColor: '#000',
    width: '95%',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    color: '#c3c3c3',
    fontSize: '14@s',
  },

  modalInput: {
    padding: '8@s',
    borderColor: blackColor,
    borderWidth: 1,
    marginBottom: '10@s',
    height: '100@s',
    textAlignVertical: 'top',
  },
  modalDescription: {
    color: blackColor,
    marginTop: '20@s',
    marginBottom: '20@s',
  },
  nextBtnContainer: {
    padding: '13@s',
    width: '70%',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  nextBtn: {
    backgroundColor: greenColor,
    alignItems: 'center',
    padding: '10@s',
    borderRadius: '5@s',
  },
  nextText: {
    color: whiteColor,
    fontSize: '14@s',
  },
});

export default Refferel;
