import React, {useState, useEffect} from 'react';
import {Text, Vibration, View, ActivityIndicator, Image} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import Toast from 'react-native-simple-toast';
import FlatGrid from 'react-native-super-grid';
import {MainHeader, ProductCard} from '../Components';
import {lightGray, whiteColor} from '../Theme/colors';
import IMAGES from '../Assets/images/emoji.png';
import Storage from '../Utils/storage';
import config from '../Utils/config';
import axios from 'axios';

const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'First Item',
    image: true,
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'Second Item',
    image: false,
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'Third Item',
    image: true,
  },
  {
    id: '4',
    title: 'Third Item',
    image: false,
  },
  {
    id: '5',
    title: 'Third Item',
  },
  {
    id: '6',
    title: 'Third Item',
  },
  {
    id: '7',
    title: 'Third Item',
  },
  {
    id: '9',
    title: 'Third Item',
  },
  {
    id: '10',
    title: 'Third Item',
  },
  {
    id: '11',
    title: 'Third Item',
  },
  {
    id: '12',
    title: 'Third Item',
  },
  {
    id: '13',
    title: 'Third Item',
  },
  {
    id: '14',
    title: 'Third Item',
  },
  {
    id: '15',
    title: 'Third Item',
  },
  {
    id: '16',
    title: 'Third Item',
  },

  {
    id: '17',
    title: 'Third Item',
  },
  {
    id: '18',
    title: 'Third Item',
  },
  {
    id: '19',
    title: 'Third Item',
  },
  {
    id: '20',
    title: 'Third Item',
  },
  {
    id: '21',
    title: 'Third Item',
  },
  {
    id: '22',
    title: 'Third Item',
  },
  {
    id: '23',
    title: 'Third Item',
  },
  {
    id: '24',
    title: 'Third Item',
  },
  {
    id: '25',
    title: 'Third Item',
  },
  {
    id: '26',
    title: 'Third Item',
  },
  {
    id: '27',
    title: 'Third Item',
  },
  {
    id: '28',
    title: 'Third Item',
  },
  {
    id: '29',
    title: 'Third Item',
  },

  {
    id: '29',
    title: 'Third Item',
  },
  {
    id: '30',
    title: 'Third Item',
  },
  {
    id: '31',
    title: 'Third Item',
  },
  {
    id: '32',
    title: 'Third Item',
  },
  {
    id: '33',
    title: 'Third Item',
  },
  {
    id: '34',
    title: 'Third Item',
  },
  {
    id: '35',
    title: 'Third Item',
  },
];

function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const ProductCardListScreen = ({
  navigate,
  products,
  check,
  update,
  countCart,
  ch,
}) => {
  const [token, settoken] = useState('');
  const [wishlist, setWishlist] = useState([]);
  const [loader, setloader] = useState(false);
  const [count, setCount] = useState(0);
  const [wishCount, setWishCount] = useState(0);
  const forceUpdate = useForceUpdate();
  useEffect(() => {
    Storage.retrieveData('user').then((resp) => {
      getwish(resp.token);
      settoken(resp.token);
    });
  }, []);
  useEffect(() => {
    getColorForProduct();
    setloader(false);
  }, [wishlist]);
  const getColorForProduct = () => {
    wishlist?.map((item, index) => {
      products.map((product) => {
        if (product.heart !== 'red') {
          if (product.id === parseInt(item.id)) {
            product.heart = 'red';
          } else {
            product.heart = lightGray;
          }
        }
      });
    });
    forceUpdate();
  };
  // const addinWish = (id) => {
  //   setloader(true);
  //   const params = {
  //     product_id: id,
  //   };
  //   axios
  //     .post(`${config.apiUrl}/whishlist`, params, {
  //       headers: {
  //         Accept: 'application/json',
  //         'Content-Type': 'application/json',
  //         Authorization: `Bearer ${token}`,
  //       },
  //     })
  //     .then((res) => {
  //       setloader(false);
  //       Toast.show(res.message, Toast.LONG);
  //     })
  //     .catch((error) => {
  //       console.log('errororor');
  //       console.error(error);
  //     });
  // };
  // const removeWish = (id) => {
  //   setloader(true);
  //   const params = {
  //     product_id: id,
  //   };
  //   axios
  //     .post(`${config.apiUrl}/whishlist/delete`, params, {
  //       headers: {
  //         Accept: 'application/json',
  //         'Content-Type': 'application/json',
  //         Authorization: `Bearer ${token}`,
  //       },
  //     })
  //     .then((res) => {
  //       setloader(false);
  //       Toast.show(res.message, Toast.SHORT);
  //     })
  //     .catch((error) => {
  //       console.log('errororor');
  //       console.error(error);
  //     });
  // };
  const addinWishL = async (id) => {
    let token = '';
    await Storage.retrieveData('user').then((resp) => {
      token = resp.token;
    });
    const params = {
      product_id: id,
    };
    axios
      .post(`${config.apiUrl}/whishlist`, params, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        console.log(res, wishCount);
        Toast.show(res.data.message, Toast.LONG);
        setWishCount(res.data.data.length);
        ch(res.data.data.length);
      })
      .catch((error) => {
        console.error(error);
      });
  };
  const removeWishL = async (id) => {
    let token = '';
    await Storage.retrieveData('user').then((resp) => {
      token = resp.token;
    });
    const params = {
      product_id: id,
    };
    axios
      .post(`${config.apiUrl}/whishlist/delete`, params, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        Toast.show(res.data.message, Toast.SHORT);
        ch(res.data.data.length);
        console.log(res, wishCount);
        if (wishCount !== 0) {
          setWishCount(wishCount - 1);
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };
  const getwish = (t) => {
    setloader(true);
    fetch(`${config.apiUrl}/wishlist/show`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${t}`,
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        setWishlist(responseJson.data);
        setWishCount(responseJson.data.length);
        getColorForProduct();
        setloader(false);
      });
  };
  const handleColor = async (id) => {
    // console.log('jj', wishCount);
    products.map((data, index) => {
      if (data.id === id) {
        if (data.heart === 'red') {
          data.heart = lightGray;
          removeWishL(data.id);
        } else {
          data.heart = 'red';
          addinWishL(data.id);
        }
      }
    });

    forceUpdate();
  };

  const addtocart = async (item) => {
    let token = null;
    let user = null;
    let pr = 0;
    let cartDet = null;
    await Storage.retrieveData('cartDataDetails').then((resp) => {
      cartDet = resp;
    });

    if (cartDet) {
      cartDet.push(item);
      Storage.storeData('cartDataDetails', cartDet);
    } else {
      Storage.storeData('cartDataDetails', [cartDet]);
    }
    await Storage.retrieveData('cartData').then((resp) => {
      token = resp;
    });
    await Storage.retrieveData('user').then((resp) => {
      user = resp;
    });
    if (token === null) {
      await fetch(`${config.apiUrl}/carts`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      })
        .then((response) => response.json())
        .then((responseJson) => {
          Storage.storeData('cartData', responseJson);
          token = responseJson;
        });
    }
    if (item.promotion === '1' || item.promotion === 1) {
      pr = parseFloat(item.promotion_price).toFixed(2);
    } else {
      const k = (parseFloat(item.price) * parseFloat(item.discount)) / 100;
      const calculated = item.price - k;
      let p = calculated.toFixed(2);
      if (p > calculated) {
        p = p - 0.01;
      }
      pr = p;
    }
    let toCount = 0;

    
    await fetch(
      `${config.apiUrl}/carts/show_cart_data/${token.cartToken}`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          cartKey: token.cartKey,
        }),
      },
    )
      .then((response) => response.json())
      .then((responseJson) => {
      // error
  if(responseJson.item_in_cart !== undefined){
      responseJson.item_in_cart.map((cartItem) => {
          if (cartItem.productID === item.id) {
            toCount = parseInt(cartItem.Quantity);
            console.log('toCount.id', cartItem?.Quantity);
          }
        });
      }

      // error

        console.log('cart', responseJson['item_in_cart']);

        // responseJson['item_in_cart'].map(item => this.getProducts(item.Name))
      })
      .catch((e) => {
        console.log('error is', e);
        this.setState({
          loader: false,
        });
      });
    let qti = 1;
    if (toCount !== 0) {
      qti = toCount + 1;
    }
    await fetch(
      `${config.apiUrl}/carts/add_to_cart/${token.cartToken}`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          cartKey: token.cartKey,
          productID: item.id,
          quantity: qti.toString(),
          // price: pr,
          price: item.price,
          image: item.image,
        }),
      },
    )
      .then((response) => response.json())
      .then((responseJson) => {
        Toast.show(responseJson.message);
        update();
      });
  };
  const renderItem = ({item}) => (
    <ProductCard
      handleColor={handleColor}
      item={item}
      navigate={navigate}
      image={item.image}
      addtocart={addtocart}
    />
  );

  return (
    // <View>
    // {loader?<ActivityIndicator size="large" color="#00ff00" animating={loader} />:
    <View style={styles.container}>
      {check ? (
        <></>
      ) : (
        <MainHeader
          home={false}
          navigate={navigate}
          count={countCart}
          wishCount={wishCount}
        />
      )}
      <View style={{flex: 1, marginTop: 20}}>
        {products.length != 0 ? (
          <FlatGrid
            data={products}
            spacing={-2}
            itemDimension={120}
            showsVerticalScrollIndicator={false}
            style={styles.gridView}
            renderItem={renderItem}
          />
        ) : (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: '30%',
            }}>
            <Image source={IMAGES} style={{width: 25, height: 25}} />
            <Text style={{color: 'black', fontSize: 18, marginTop: 15}}>
              Coming Soon
            </Text>
          </View>
        )}
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,

    padding: 0,
  },
  gridView: {
    flex: 1,
    backgroundColor: whiteColor,
    marginRight: '3@s',
    marginLeft: '10@s',
  },
});

export default ProductCardListScreen;
