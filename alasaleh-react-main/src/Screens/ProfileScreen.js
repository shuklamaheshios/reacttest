import React, {useContext, useEffect, useState} from 'react';
import {
  ScrollView,
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import Geocoder from 'react-native-geocoder';
import profile_green from '../Assets/images/profile_green_bg.png';
import {DrawerHeader, MainHeader, ProfileInput} from '../Components';
import {
  blackColor,
  greenColor,
  lightGray,
  redColor,
  whiteColor,
} from '../Theme/colors';

import AppContext from '../Utils/AppContext';
import config from '../Utils/config';

function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const ProfileScreen = ({
  goBack,
  user,
  handleChange,
  saveData,
  navigate,
  so,
  uploadImage,
  latlng,
  handlelatlng,
  newlat,
  locationAddress,
  getData,
  getLocation,
}) => {
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };
  return (
    <View style={styles.container}>
      {/* <DrawerHeader goBack={goBack} /> */}
      <MainHeader navigate={navigate} />
      <ScrollView showsVerticalScrollIndicator={false} style={{flex: 1}}>
        <View style={styles.header}>
          <View style={styles.headerBody}>
            <View>
              <Text numberOfLines={1} style={styles.headerName}>
                {user.name}
              </Text>
              <Text numberOfLines={1} style={styles.headerEmail}>
                {user.email}
              </Text>
            </View>
            {/* <TouchableOpacity onPress={() => uploadImage()}>
              <View>
                <Image
                  style={styles.headerImage}
                  source={
                    so
                      ? so
                      : {
                          uri: `${config.url}/public/images/user/${user.image}`,
                        }
                  }
                />
              </View>
            </TouchableOpacity> */}
            {/* removed */}
          </View>
        </View>
        <View style={{padding: 10}}>
          <View style={styles.personalLContainer}>
            <Text style={styles.personalText}>
              {myContext.setting2name
                ? 'معلومات شخصية'
                : 'Personal Information'}
            </Text>
          </View>
          <View style={styles.inputContainer}>
            <View style={styles.inputContainerInner}>
              <ProfileInput
                type="text"
                placeholder={
                  myContext.setting2name ? 'الاسم الاول' : 'First Name'
                }
                label={myContext.setting2name ? 'الاسم الاول' : 'First Name'}
                defaultValue={user.name}
                name="name"
                onChangeText={handleChange}
              />
            </View>
            <View style={styles.inputContainerInner}>
              <ProfileInput
                type="text"
                placeholder={myContext.setting2name ? 'الكنية' : 'Last Name'}
                label={myContext.setting2name ? 'الكنية' : 'Last Name'}
                defaultValue={user.last_name}
                name="last_name"
                onChangeText={handleChange}
              />
            </View>
          </View>
          <View>
            <ProfileInput
              type="number"
              placeholder="null"
              label={myContext.setting2name ? 'هاتف' : 'Phone'}
              defaultValue={user.phone}
              name="phone"
              onChangeText={handleChange}
            />
          </View>
          <View>
            <ProfileInput
              type="email"
              placeholder="email@email.com"
              label={
                myContext.setting2name
                  ? 'العنوان البريد الإلكتروني'
                  : 'Email address'
              }
              defaultValue={user.email}
              name="email"
              onChangeText={handleChange}
            />
          </View>
          <View style={styles.personalLContainer}>
            <Text style={styles.personalText}>
              {myContext.setting2name ? 'العنوان الشحن' : 'Shipping Address'}
            </Text>
          </View>
          <View>
            <TouchableOpacity
              onPress={() =>
                navigate('GoogleMap', {
                  handlelatlng,
                  getLocation,
                  lat: user.latitude,
                  lon: user.longitude,
                })
              }
              style={{marginBottom: 10}}>
              <ProfileInput
                label={myContext.setting2name ? 'العنوان' : 'Address'}
                placeholder={'null'}
                icon="map-marker-alt"
                defaultValue={user.address}
                name="address"
                type="text"
                // Value={user.address}

                // value={`${locationAddress}`}
                edit={false}
                onChangeText={handleChange}
                

              />
            </TouchableOpacity>
          </View>
          <View>
            <ProfileInput
              type="text"
              placeholder="null"
              label={myContext.setting2name ? 'رقم الشقة' : 'Appartment'}
              defaultValue={user.apartment}
              name="apartment"
              onChangeText={handleChange}
            />
          </View>
          <View style={styles.inputContainer}>
            <View style={styles.inputContainerInner}>
              <ProfileInput
                type="text"
                placeholder="null"
                label={myContext.setting2name ? 'الولاية' : 'State'}
                defaultValue={user.state}
                name="state"
                onChangeText={handleChange}
              />
            </View>
            <View style={styles.inputContainerInner}>
              <ProfileInput
                type="text"
                placeholder="null"
                label={myContext.setting2name ? 'المدينة' : 'City'}
                defaultValue={user.city}
                name="city"
                onChangeText={handleChange}
              />
            </View>
          </View>
          <View>
            <ProfileInput
              type="number"
              placeholder="null"
              label={myContext.setting2name ? 'رمز بريدي' : 'Zip Code'}
              defaultValue={user.zip_code}
              name="zip_code"
              onChangeText={handleChange}
            />
          </View>
          <View>
            <ProfileInput
              type="text"
              placeholder="null"
              label={myContext.setting2name ? 'البلد' : 'Country'}
              defaultValue={user.country}
              name="country"
              onChangeText={handleChange}
            />
          </View>
          <View style={styles.inputContainer}>
            <View style={styles.inputContainerInner}>
              <ProfileInput
                keyboardType="numeric"
                placeholder="0.00"
                label={myContext.setting2name ? 'خط العرض' : 'Latitude'}
                // defaultValue={`${latlng.lat}`}
                defaultValue={user.latitude}
                name="lat"
                value={latlng.lat}
                edit={false}
              />
            </View>
            <View style={styles.inputContainerInner}>
              <ProfileInput
                type="number"
                placeholder="0.00"
                label={myContext.setting2name ? 'خط الطول' : 'Longitude'}
                // defaultValue={`${latlng.lng}`}
                defaultValue={user.longitude}
                name="lng"
                edit={false}
              />
            </View>
          </View>

          <View style={styles.nextBtnContainer}>
            <TouchableOpacity style={styles.cancelBtn} onPress={() => goBack()}>
              <Text style={styles.nextText}>
                {myContext.setting2name ? 'إلغاء' : 'Cancel'}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.nextBtn} onPress={() => saveData()}>
              <Text style={styles.nextText}>
                {myContext.setting2name ? 'احفظ التغييرات' : 'Save Changes'}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },

  header: {
    backgroundColor: greenColor,
    height: '130@s',
    justifyContent: 'center',
  },

  headerBody: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: '20@s',
    paddingRight: '20@s',
  },

  headerName: {
    fontSize: '18@s',
    color: whiteColor,
  },

  headerEmail: {
    fontSize: '14@s',
    color: whiteColor,
    marginLeft: '10@s',
  },
  headerImage: {
    height: '65@s',
    width: '65@s',
    borderRadius: 50,
    borderColor: whiteColor,
    borderWidth: 2,
  },

  personalLContainer: {
    borderBottomColor: blackColor,
    borderBottomWidth: 1,
    paddingBottom: '5@s',
  },

  personalText: {
    color: greenColor,
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: '10@s',
  },

  inputContainerInner: {
    width: '49%',
  },

  nextBtnContainer: {
    padding: '10@s',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  nextBtn: {
    backgroundColor: greenColor,
    alignItems: 'center',
    padding: '6@s',
    borderRadius: '5@s',
    width: '48%',
  },

  cancelBtn: {
    backgroundColor: redColor,
    alignItems: 'center',
    padding: '6@s',
    borderRadius: '5@s',
    width: '48%',
  },
  nextText: {
    color: whiteColor,
    fontSize: '14@s',
  },
});
export default ProfileScreen;
