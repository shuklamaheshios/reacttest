import React, {useContext, useState} from 'react';
import {Image, Text, View, TouchableOpacity} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {ScaledSheet} from 'react-native-size-matters';
import {DrawerHeader, MainHeader} from '../Components';
import profile_green from '../Assets/images/profile_green_bg.png';
import heart_green from '../Assets/images/heart_green_bg.png';
import list_green from '../Assets/images/lists_green_bg.png';
import pay_green from '../Assets/images/pay_green_bg.png';
import creditcard_green_bg from '../Assets/images/creditcard_green_bg.png';
import refrel_green_bg from '../Assets/images/refrel_green_bg.png';
import lock_green_bg from '../Assets/images/lock_green_bg.png';
import logout_green_bg from '../Assets/images/logout_green_bg.png';
import {
  blackColor,
  greenColor,
  lightGray,
  redColor,
  whiteColor,
} from '../Theme/colors';

import AppContext from '../Utils/AppContext';

function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const MyAcountScreen = ({goBack, navigate, logout, userData}) => {
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };
  return (
    <View style={styles.container}>
      <MainHeader home={false} navigate={navigate} />
      <ScrollView style={styles.scrollView}>
        <View style={{flex: 1}}>
          <View style={styles.header}>
            <Text style={styles.myAccountText}>My Account</Text>
            {/* <View
              style={
                myContext.setting2name
                  ? styles.headerBodyUrdu
                  : styles.headerBody
              }>
              <View>
                <Text numberOfLines={1} style={styles.headerName}>
                  {userData.name}
                </Text>
                <Text numberOfLines={1} style={styles.headerEmail}>
                  {userData.email}
                </Text>
              </View>
              <View>
                <Image style={styles.headerImage} source={profile_green} />
              </View>
            </View> */}
          </View>
          <View style={styles.cardContainer}>
            <View style={styles.card}>
              <TouchableOpacity
                onPress={() => navigate('Profile')}
                style={styles.listItem}>
                <Image style={styles.image} source={profile_green} />
                <Text style={styles.itemText}>
                  {myContext.setting2name ? 'معلوماتي الشخصية' : 'My Profile'}
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => navigate('WhishList')}
                style={styles.listItem}>
                <Image style={styles.image} source={heart_green} />
                <Text style={styles.itemText}>
                  {myContext.setting2name ? 'قائمة الرغبات' : 'Wishlist'}
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => navigate('MyOrder')}
                style={styles.listItem}>
                <Image style={styles.image} source={list_green} />
                <Text style={styles.itemText}>
                  {' '}
                  {myContext.setting2name ? 'لائحة الطلبات' : 'Order List'}
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => navigate('Payment')}
                style={styles.listItem}>
                <Image style={styles.image} source={pay_green} />
                <Text style={styles.itemText}>
                  {myContext.setting2name
                    ? 'طريقة إكمال الشراء'
                    : 'Payment Methods'}
                </Text>
              </TouchableOpacity>

              {/* <TouchableOpacity
                onPress={() => navigate('ManageCard')}
                style={styles.listItem}>
                <Image style={styles.image} source={creditcard_green_bg} />
                <Text style={styles.itemText}>
                  {myContext.setting2name
                    ? 'إدارة بطاقات إكمال الشراء'
                    : 'Manage Credit Cards'}
                </Text>
              </TouchableOpacity> */}

              <TouchableOpacity
                onPress={() => navigate('ReferelPoint')}
                style={styles.listItem}>
                <Image style={styles.image} source={refrel_green_bg} />
                <Text style={styles.itemText}>
                  {myContext.setting2name
                    ? 'النقاط ومجموع المكافآت'
                    : 'Points & Referral Amount'}
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => navigate('UpdatePassword')}
                style={styles.listItem}>
                <Image style={styles.image} source={lock_green_bg} />
                <Text style={styles.itemText}>
                  {myContext.setting2name
                    ? 'تغيير كلمة السر'
                    : 'Update Password'}
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => logout()}
                style={styles.listItem}>
                <Image style={styles.image} source={logout_green_bg} />
                <Text style={styles.itemText}>
                  {myContext.setting2name ? 'تسجيل خروج' : 'Logout'}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    position: 'relative',
  },

  header: {
    backgroundColor: greenColor,
    height: '130@s',
    alignItems: 'center',
  },

  myAccountText: {
    color: whiteColor,
    fontSize: '15@s',
    marginTop: '20@s',
  },

  headerBody: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: '20@s',
    paddingRight: '20@s',
  },

  headerBodyUrdu: {
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: '20@s',
    paddingRight: '20@s',
  },

  headerName: {
    fontSize: '18@s',
    color: whiteColor,
  },

  headerEmail: {
    fontSize: '14@s',
    color: whiteColor,
    marginLeft: '10@s',
    width: '220@s',
  },

  cardContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  card: {
    flex: 1,
    width: '95%',
    backgroundColor: whiteColor,
    marginLeft: '10@s',
    marginRight: '10@s',
    borderRadius: '20@s',
    marginTop: '-45@s',
    marginBottom: '10@s',
    borderColor: lightGray,
    borderWidth: 2,
  },

  listItem: {
    borderBottomColor: lightGray,
    borderBottomWidth: 1,
    padding: '15@s',
    flexDirection: 'row',
    alignItems: 'center',
  },

  image: {
    width: '25@s',
    height: '25@s',
    borderRadius: 50,
  },

  itemText: {
    color: blackColor,
    fontSize: '13@s',
    marginLeft: '20@s',
  },

  scrollView: {
    flex: 1,
  },

  headerImage: {
    height: '65@s',
    width: '65@s',
    borderRadius: 50,
    borderColor: whiteColor,
    borderWidth: 2,
  },
});

export default MyAcountScreen;
