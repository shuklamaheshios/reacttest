import React, {useState, useContext} from 'react';
import {ScrollView, View, TouchableOpacity, Text, Image} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import TextBox from 'react-native-password-eye';
import Icon from 'react-native-vector-icons/FontAwesome';
import CheckBox from 'react-native-check-box';
import registerImage from '../Assets/images/pencil.png';
import {Input, EmptyHeader, MainHeader} from '../Components';
import {
  blackColor,
  greenColor,
  lightGray,
  redColor,
  whiteColor,
} from '../Theme/colors';
import AppContext from '../Utils/AppContext';

function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const RegisterScreen = ({
  goBack,
  handleChange,
  navigate,
  genrateOtp,
  signIni,
}) => {
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };
  const [isChecked, setIsChecked] = useState(true);
  return (
    <View style={styles.container}>
      <EmptyHeader
        title={myContext.setting2name ? 'سجل الان' : 'Register Now'}
        goBack={goBack}
      />
      {/* <MainHeader navigate={navigate} /> */}
      <ScrollView style={{flex: 1, marginTop: 10}}>
        <Input
          type="text"
          label={
            myContext.setting2name ? 'أدخل الاسم الأول' : 'Enter First Name'
          }
          placeHolder="Jordan"
          onChangeText={handleChange}
          name="f_name"
        />
        <Input
          secureTextEntry={false}
          type="text"
          label={myContext.setting2name ? 'إدخال اسم آخر' : 'Enter Last Name'}
          placeHolder="Makail"
          onChangeText={handleChange}
          name="l_name"
        />
        <Input
          type="email"
          label={
            myContext.setting2name
              ? 'أدخل العنوان البريد الالكتروني'
              : 'Enter Email Address'
          }
          placeHolder="example@example.com"
          onChangeText={handleChange}
          name="email"
        />
        <View>
          <Text style={styles.inputLabel}>
            {myContext.setting2name ? 'أدخل كلمة المرور' : 'Enter Password'}
          </Text>
          <TextBox
            placeholder="***********"
            onChangeText={(text) => handleChange('password', text)}
            containerStyles={styles.customeInput}
            inputStyle={styles.inputStyle}
            // secureTextEntry={true}

            ></TextBox>
        </View>

        <View>
          <Text style={styles.inputLabel}>
            {myContext.setting2name ? 'تأكيد كلمة المرور' : 'Confirm Password'}
          </Text>
          <TextBox
            placeholder="***********"
            onChangeText={(text) => handleChange('confirm_password', text)}
            containerStyles={styles.customeInput}
            inputStyle={styles.inputStyle}
            // secureTextEntry={true}
            ></TextBox>
        </View>

        <View style={styles.checkBoxContainer}>
          <CheckBox
            onClick={() => setIsChecked(!isChecked)}
            isChecked={isChecked}
            rightText={
              myContext.setting2name
                ? 'وأنا أتفق مع شروط وأحكام'
                : 'I agree With Terms And Conditions'
            }
            rightTextStyle={styles.checkBoxText}
            checkedCheckBoxColor={greenColor}
            checkBoxColor={lightGray}
          />
        </View>

        <View style={styles.nextBtnContainer}>
          <TouchableOpacity style={styles.nextBtn} onPress={() => genrateOtp()}>
            <Text style={styles.nextText}>
              {myContext.setting2name ? 'التالي' : 'Next'}
            </Text>
          </TouchableOpacity>
        </View>
{/* 
        <Text style={styles.orSocialText}>
          {myContext.setting2name
            ? 'أو انضم إلى ملف التعريف الاجتماعي الخاص بك'
            : 'Or join With your Social Profile'}
        </Text>

        <View style={styles.footerSocial}>
          <TouchableOpacity style={styles.socialFaceButton}>
            <Icon name="facebook-square" size={22} color={whiteColor} />
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                width: '100%',
              }}>
              <Text style={{color: whiteColor, fontSize: 11}}>
                {myContext.setting2name
                  ? 'تسجيل الدخول'
                  : 'Register with Facebook'}
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={styles.socialGoogleBtn}>
            <Icon name="google" size={22} color={whiteColor} />
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                width: '100%',
              }}>
              <Text style={{color: whiteColor, fontSize: 11}}>
                {myContext.setting2name
                  ? 'تسجيل الدخول'
                  : 'Register with Google'}
              </Text>
            </View>
          </TouchableOpacity>
        </View> */}
            {/* removed */}

        <View style={styles.footer}>
          <TouchableOpacity
            onPress={() => navigate('Login')}
            style={styles.authBtn}>
            <Icon name="lock" size={22} color={whiteColor} />
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                width: '100%',
              }}>
              <Text style={{color: blackColor}}>
                {myContext.setting2name ? 'تسجيل الدخول' : 'LOGIN'}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
  nextBtnContainer: {
    padding: '13@s',
  },
  nextBtn: {
    backgroundColor: greenColor,
    alignItems: 'center',
    padding: '10@s',
    borderRadius: '5@s',
  },
  nextText: {
    color: whiteColor,
    fontSize: '14@s',
  },

  footer: {
    flexDirection: 'row',
    paddingLeft: '25@s',
    paddingRight: '25@s',
    justifyContent: 'center',
  },

  footerSocial: {
    flexDirection: 'row',
    paddingLeft: '15@s',
    paddingRight: '15@s',
    justifyContent: 'space-between',
    marginBottom: '15@s',
  },

  authBtn: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: lightGray,
    width: '48%',
    padding: '10@s',
    justifyContent: 'space-between',
  },

  authText: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: '13@s',
  },

  checkBoxContainer: {
    padding: '15@s',
  },
  checkBoxText: {
    color: blackColor,
    fontSize: '12@s',
    fontWeight: 'bold',
  },

  customeInput: {
    borderColor: blackColor,
    borderWidth: 1,
    paddingRight: '10@s',
    color: blackColor,
    height: '100@s',
    marginLeft: '13@s',
    height: '35@s',
    marginRight: '13@s',
    marginTop: '5@s',
  },

  inputStyle: {
    paddingLeft: '5@s',
    paddingRight: '10@s',
    color: blackColor,
    fontSize: '12@s',
  },

  inputLabel: {
    color: blackColor,
    fontSize: '14@s',
    marginLeft: '13@s',
    marginTop: '7@s',
    marginRight: '13@s',
  },
  image: {
    height: '16@s',
    width: '16@s',
    resizeMode: 'center',
  },

  socialFaceButton: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#166FE5',
    width: '48%',
    padding: '10@s',
    justifyContent: 'space-between',
    borderRadius: '4@s',
  },

  socialGoogleBtn: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: redColor,
    width: '48%',
    padding: '10@s',
    justifyContent: 'space-between',
    borderRadius: '4@s',
  },

  orSocialText: {
    color: '#333',
    textAlign: 'center',
    marginTop: '10@s',
    marginBottom: '10@s',
    fontSize: '14@s',
  },
});

export default RegisterScreen;
