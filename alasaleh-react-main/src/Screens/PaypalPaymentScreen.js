// import React, { useState } from 'react';
// import {
//   Text,
//   View,
//   StyleSheet,
//   ActivityIndicator,
//   TouchableOpacity
// } from 'react-native';
// import { WebView } from 'react-native-webview';
// import axios from "axios";
// import qs from "qs";
// import { decode, encode } from 'base-64'

// const App = () => {
//   const [isWebViewLoading, SetIsWebViewLoading] = useState(false);
//   const [paypalUrl, setPaypalUrl] = useState('');
//   const [accessToken, setAccessToken] = useState("");

//   //Fix bug btoa
//   useEffect(() => {
//     if (!global.btoa) {
//       global.btoa = encode;
//     }

//     if (!global.atob) {
//       global.atob = decode;
//     }
//   }, [])


//   //When loading paypal page it refirects lots of times. This prop to control start loading only first time
//   const [shouldShowWebViewLoading, setShouldShowWebviewLoading] = useState(true)

//   /*---Paypal checkout section---*/
//   const buyBook = async () => {

//     //Check out https://developer.paypal.com/docs/integration/direct/payments/paypal-payments/# for more detail paypal checkout
//     const dataDetail = {
//       "intent": "sale",
//       "payer": {
//         "payment_method": "paypal"
//       },
//       "transactions": [{
//         "amount": {
//           "currency": "AUD",
//           "total": "26",
//           "details": {
//             "shipping": "6",
//             "subtotal": "20",
//             "shipping_discount": "0",
//             "insurance": "0",
//             "handling_fee": "0",
//             "tax": "0"
//           }
//         },
//         "description": "This is the payment transaction description",
//         "payment_options": {
//           "allowed_payment_method": "IMMEDIATE_PAY"
//         }, "item_list": {
//           "items": [{
//             "name": "Book",
//             "description": "Chasing After The Wind",
//             "quantity": "1",
//             "price": "20",
//             "tax": "0",
//             "sku": "product34",
//             "currency": "AUD"
//           }]
//         }
//       }],
//       "redirect_urls": {
//         "return_url": "https://example.com/",
//         "cancel_url": "https://example.com/"
//       }
//     }

//     const url = `https://api.sandbox.paypal.com/v1/oauth2/token`;

//     const data = {
//       grant_type: 'client_credentials'

//     };

//     const auth = {
//       username: "AVoei4BdvFtP_nB5Ulu0fN0L1FrW_he7DLx2A1Y6TUdOFVwrKed73my0bwlUeAXO0mFJZyAfZ5cGpWwz",  //"your_paypal-app-client-ID",
//       password: "EBAPtB2_JPDoKsftonMYhsF7BWb1SIxCLD6IplXwNKRZEAgD1HOstsQr4Q7PVcT5vmXKcy7GTzY2rp2R"   //"your-paypal-app-secret-ID


//     };

//     const options = {

//       method: 'post',
//       headers: {
//         'content-type': 'application/x-www-form-urlencoded',
//         'Access-Control-Allow-Credentials': true
//       },

//       //Make sure you use the qs.stringify for data
//       data: qs.stringify(data),
//       auth: auth,
//       url,
//     };

//     // Authorise with seller app information (clientId and secret key)
//     axios(options).then(response => {
//       setAccessToken(response.data.access_token)

//       //Resquest payal payment (It will load login page payment detail on the way)
//       axios.post(`https://api.sandbox.paypal.com/v1/payments/payment`, dataDetail,
//         {
//           headers: {
//             'Content-Type': 'application/json',
//             'Authorization': `Bearer ${response.data.access_token}`
//           }
//         }
//       )
//         .then(response => {
//           const { id, links } = response.data
//           const approvalUrl = links.find(data => data.rel == "approval_url").href

//           console.log("response", links)
//           setPaypalUrl(approvalUrl)
//         }).catch(err => {
//           console.log({ ...err })
//         })
//     }).catch(err => {
//       console.log(err)
//     })
//   };

//   /*---End Paypal checkout section---*/

//   onWebviewLoadStart = () => {
//     if (shouldShowWebViewLoading) {
//       SetIsWebViewLoading(true)
//     }
//   }

//   _onNavigationStateChange = (webViewState) => {
//     console.log("webViewState", webViewState)

//     //When the webViewState.title is empty this mean it's in process loading the first paypal page so there is no paypal's loading icon
//     //We show our loading icon then. After that we don't want to show our icon we need to set setShouldShowWebviewLoading to limit it
//     if (webViewState.title == "") {
//       //When the webview get here Don't need our loading anymore because there is one from paypal
//       setShouldShowWebviewLoading(false)
//     }

//     if (webViewState.url.includes('https://example.com/')) {

//       setPaypalUrl(null)
//       const urlArr = webViewState.url.split(/(=|&)/);

//       const paymentId = urlArr[2];
//       const payerId = urlArr[10];

//       axios.post(`https://api.sandbox.paypal.com/v1/payments/payment/${paymentId}/execute`, { payer_id: payerId },
//         {
//           headers: {
//             'Content-Type': 'application/json',
//             'Authorization': `Bearer ${accessToken}`
//           }
//         }
//       )
//         .then(response => {
//           setShouldShowWebviewLoading(true)
//           console.log(response)

//         }).catch(err => {
//           setShouldShowWebviewLoading(true)
//           console.log({ ...err })
//         })

//     }
//   }


//   return (
//     <React.Fragment>
//       <View style={styles.container}>
//         <Text>Paypal in React Native</Text>
//         <TouchableOpacity
//           activeOpacity={0.5}
//           onPress={buyBook}
//           style={
//             styles.btn
//           }>
//           <Text
//             style={{
//               fontSize: 22,
//               fontWeight: '400',
//               textAlign: 'center',
//               color: '#ffffff',
//             }}>
//             BUY NOW
//                       </Text>
//         </TouchableOpacity>
//       </View>
//       {paypalUrl ? (
//         <View style={styles.webview}>
//           <WebView
//             style={{ height: "100%", width: "100%" }}
//             source={{ uri: paypalUrl }}
//             onNavigationStateChange={this._onNavigationStateChange}
//             javaScriptEnabled={true}
//             domStorageEnabled={true}
//             startInLoadingState={false}
//             onLoadStart={onWebviewLoadStart}
//             onLoadEnd={() => SetIsWebViewLoading(false)}
//           />
//         </View>
//       ) : null}
//       {isWebViewLoading ? (
//         <View style={{ ...StyleSheet.absoluteFill, justifyContent: "center", alignItems: "center", backgroundColor: "#ffffff" }}>
//           <ActivityIndicator size="small" color="#A02AE0" />
//         </View>
//       ) : null}
//     </React.Fragment>
//   );
// };

// App.navigationOptions = {
//   title: 'App',
// };

// export default App;

// const styles = StyleSheet.create({
//   container: {
//     width: '100%',
//     height: '100%',
//     justifyContent: "center",
//     alignItems: "center"
//   },
//   webview: {
//     width: '100%',
//     height: '100%',
//     position: 'absolute',
//     top: 0,
//     left: 0,
//     right: 0,
//     bottom: 0,
//   },
//   btn: {
//     paddingVertical: 5,
//     paddingHorizontal: 15,
//     borderRadius: 10,
//     backgroundColor: '#61E786',
//     justifyContent: 'center',
//     alignItems: 'center',
//     alignContent: 'center',
//   },
// });

import React, { useState } from 'react';
import { ScaledSheet } from 'react-native-size-matters';
import { whiteColor } from '../Theme/colors';
import { WebView } from 'react-native-webview';
import { Button, SafeAreaView, ActivityIndicator, View } from 'react-native';
import { CustomLoader } from '../Components';
import Storage from '../Utils/storage';
import config from '../Utils/config';
import Toast from 'react-native-simple-toast';
import axios from "axios";
import qs from "qs";
import {decode, encode} from 'base-64'
const return_url=`${config.apiUrl}/paypal/success`;
const cancel_url=`${config.apiUrl}/paypal/cancel`;
if (!global.btoa) {
    global.btoa = encode;
}

if (!global.atob) {
    global.atob = decode;
}

/**
 * The main class that submits the credit card data and
 * handles the response from Stripe.
 */
export default class PaypalPayment extends React.Component {

    static navigationOptions = {
        title: 'Pay with Paypal',
    };

    constructor(props) {
        super(props);
        
        this.state = {
            submitted: false,
            webViewMounted: false,
            firstTimeLoading: true,
            paymentRequestInProgress: false,
            lastWebviewURL: null,
            loader: false,
            paypalUrl: null,
            error: null
        }
    }

    setPaypalToken = (res) => {
        this.setState({
            paypal_token: res,
        });
    }

    payWithPaypal = async () => {
        let self = this;
        if (self.state.paymentRequestInProgress) {
            return;
        }
        self.state.paymentRequestInProgress=true
        //Check out https://developer.paypal.com/docs/integration/direct/payments/paypal-payments/# for more detail paypal checkout
        
        const dataDetail = {
            "intent": "sale",
            "payer": {
                "payment_method": "paypal"
            },
            "transactions": [{
                "amount": {
                    "currency": "USD",
                    "total": this.state.invoice.total_amount,
                    "details": {
                        "shipping": this.state.invoice.total_shipping_amount,
                        "subtotal": this.state.invoice.subtotal,
                        "tax": this.state.invoice.total_tax
                    }
                },
                "description": "This is the payment transaction description",
                "payment_options": {
                    "allowed_payment_method": "IMMEDIATE_PAY"
                },
                "item_list": {
                    "shipping_address": {
                        "recipient_name": this.state.userinfo.name,
                        "line1": this.state.userinfo.address,
                        "line2": this.state.userinfo.apartment,
                        "city": this.state.userinfo.city,
                        "country_code": "US",
                        "postal_code": this.state.userinfo.zip_code,
                        "phone": this.state.userinfo.phone,
                        "state": this.state.userinfo.state ? this.state.userinfo.state : 'state'
                      }
                }
            }],
            "redirect_urls": {
                "return_url": return_url,
                "cancel_url": cancel_url
            }
        }

        const url = `https://api.sandbox.paypal.com/v1/oauth2/token`;

        const data = {
            grant_type: 'client_credentials'
        };

        const auth = {
            username: self.state.paypal.client_id,
            password: self.state.paypal.secret
        };

        const options = {

            method: 'post',
            headers: {
                'content-type': 'application/x-www-form-urlencoded',
                'Access-Control-Allow-Credentials': true
            },

            //Make sure you use the qs.stringify for data
            data: qs.stringify(data),
            auth: auth,
            url,
        };

        // Authorise with seller app information (clientId and secret key)
        axios(options).then(response => {
            self.setPaypalToken(response.data.access_token)

            //Resquest payal payment (It will load login page payment detail on the way)
            axios.post(`https://api.sandbox.paypal.com/v1/payments/payment`, dataDetail,
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${self.state.paypal_token}`
                    }
                }
            )
                .then(response => {
                    console.log('payment response', response)
                    const { id, links } = response.data
                    const approvalUrl = links.find(data => data.rel == "approval_url").href
                    self.setPaypalUrl(approvalUrl)
                    self.state.paymentRequestInProgress=false
                }).catch(err => {
                    self.state.paymentRequestInProgress=false
                    console.log('error applying the payment', err)
                })
        }).catch(err => {
            self.state.paymentRequestInProgress=false
            console.log('error from payment paypal', err)
        })
    };

    async componentDidMount() {
        const { route } = this.props;

        Storage.retrieveData('Paypal').then((res) => {
            this.setState({
                paypal: res,
            });
        });
        
        Storage.retrieveData('invoice').then((res) => {
            this.setState({
                invoice: res,
            });
        });

        Storage.retrieveData('billing').then((res) => {
            this.setState({
                billing: res,
            });
        });

        Storage.retrieveData('cartData').then((res) => {
            this.setState({
                cart: res,
            });
        });

        Storage.retrieveData('user').then((resp) => {
            this.setState({
                userto: resp.token,
            });
        });

        Storage.retrieveData('userinfo').then((resp) => {
            this.setState({
                userinfo: resp,
            });
        });

        Storage.retrieveData('coupon').then((resp) => {
            this.setState({
                coupenCode: resp,
            });
        });
    }

    setPaypalUrl = (val) => {
        this.setState({
            paypalUrl: val,
        });
    }

    SetIsWebViewLoading = (val) => {
        if (this.state.webViewMounted) {
            this.setState({
                loader: val,
            });
        }
    }

    getParameterByName = (name, url)=> {
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    _onNavigationStateChange = async (webViewState) => {
        let self = this;
        console.log("webViewState", webViewState.url)
        if (self.state.lastWebviewURL === webViewState.url) {
            console.log('no actual change for the webview, exiting _onNavigationStateChange')
            return;
        }
        this.setState({
            lastWebviewURL: webViewState.url
        })
        //When the webViewState.title is empty this mean it's in process loading the first paypal page so there is no paypal's loading icon
        //We show our loading icon then. After that we don't want to show our icon we need to set setShouldShowWebviewLoading to limit it
        
        if (webViewState.url == "about:blank" && this.state.firstTimeLoading === true) {
            await this.payWithPaypal()
        } else if (webViewState.url.includes(cancel_url)) {
            self.setPaypalUrl(null)
            this.setState({
                firstTimeLoading: false
            })
            Toast.show("Payment cancelled!")
        } else if (webViewState.url.includes(return_url)) {
            self.setPaypalUrl(null)
            this.setState({
                firstTimeLoading: false
            })
            
            let paymentId = this.getParameterByName('paymentId', webViewState.url)
            let token = this.getParameterByName('token', webViewState.url)
            let payerId = this.getParameterByName('PayerID', webViewState.url)
            self.checkoutWithPaypal(paymentId, token,payerId)
            return;
        }
    }

    checkoutWithPaypal = async (paymentId, token, payerId) => {
        const { navigation } = this.props;
        let self = this
        self.setState({
          loader: true,
        });
        console.log(this.state.cart)
        const params = {
          paymentId, token, payerId,
          cart_token: this.state.cart.cartToken,
          cartKey: this.state.cart.cartKey,
          email: this.state.userinfo.email,
          address: this.state.userinfo.address,
          city: this.state.userinfo.city,
          state: this.state.userinfo.state ? this.state.userinfo.state : 'state',
          postal_code: this.state.userinfo.zip_code,
          "apartment": this.state.userinfo.apartment,
          lat: parseFloat(this.state.userinfo.latitude),
          long: parseFloat(this.state.userinfo.longitude),
          "coupon_code": this.state.coupenCode,
          save_account_info: '0',
          payment_method: 'paypal',
        };
        await fetch(`${config.apiUrl}/paypal/executePayment`, {
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${this.state.userto}`,
          },
          // Use a proper HTTP method
          method: 'post',
          body: JSON.stringify(params)
        }).then((response) => {
          console.log('received response from paypal payment', response)
          return response.json()
        })
          .then((res) => {
            console.log('response : ', res)
            if (res.status) {
              Toast.show(res.message)
              self.setState({
                loader: false
              });
              Storage.removeData('cartData');
              navigation.navigate('MyOrder');
            } else {
              self.setState({
                loader: false,
              });
              Toast.show(res.message || "Error while processing payment");
            }
    
          })
          .catch((error) => {
            console.log('payment error', error)
            self.setState({
              loader: false,
            });
            Toast.show(error);
          });
      };

    displaySpinner() {
        return (
            <SafeAreaView><CustomLoader /></SafeAreaView>)
    }
    render() {
        const { submitted, error, paypalUrl } = this.state;
        return (
            <SafeAreaView style={styles.safeAreaContainer}>
                { this.state.loader ? (
                    <CustomLoader />
                ) : (
                        <View style={styles.webview}>
                            <WebView
                                style={{ height: "100%", width: "100%" }}
                                source={{ uri: paypalUrl }}
                                onNavigationStateChange={this._onNavigationStateChange}
                                javaScriptEnabled={true}
                                domStorageEnabled={true}
                                startInLoadingState={true}
                                renderLoading={() => {
                                    return this.displaySpinner();
                                  }}
                            />
                        </View>
                    )}
            </SafeAreaView>
        );
    }
}

const styles = ScaledSheet.create({
    safeAreaContainer: {
        flex: 1,
        backgroundColor: whiteColor,
    },
    webview: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },
});