import React, {useState, useContext, useEffect} from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {ScaledSheet} from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/FontAwesome';

import maxum from '../Assets/images/maxum.png';
import {MainHeader} from '../Components';
import CardsContaiener from '../Components/CardsContainer';
import EmptyHeader from '../Components/EmptyHeader';
import {
  blackColor,
  grayColor,
  greenColor,
  lightGray,
  redColor,
  whiteColor,
} from '../Theme/colors';
import config from '../Utils/config';
import AppContext from '../Utils/AppContext';
import {ADD_TO_CART, COMMING_SOOM, OUT_OF_STOCK} from '../Utils/ComonWords';
function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}
const CartScreen = ({
  goBack,
  navigation,
  navigate,
  product,
  countCart,
  addtocart,
  changeWish,
}) => {
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const [stock, setstock] = useState();
  const [price, setPrice] = useState(0);
  const [discount, setDiscountString] = useState();
  const [relatedPro, setRelatedPro] = useState();
  const handleClick = () => {
    product.handleColor(product?.item?.id);
    changeWish();
    forceUpdate();
  };
  // console.log('item free', product);
  useEffect(() => {
    getProducts(product?.item?.category_id);
  }, []);
  useEffect(() => {
    if (product?.item?.qty >= 5) {
      setstock(true);
    } else {
      console.log('false', product?.item?.qty);
      setstock(false);
    }

    if (product?.item?.promotion === 1 || product?.item?.promotion === '1') {
      setPrice(parseFloat(product?.item?.promotion_price).toFixed(2));
    } else {
      const k =
        (parseFloat(product?.item?.price) *
          parseFloat(product?.item?.discount)) /
        100;
      const calculated = product?.item?.price - k;
      let p = calculated.toFixed(2);
      if (p > calculated) {
        p = p - 0.01;
      }
      setPrice(p);
    }
  });

  const getProducts = async (id) => {
    await fetch(
      `${config.apiUrl}/search_by_category/category_id/${id}`,
    )
      .then((response) => response.json())
      .then((responseJson) => {
        // console.log('my resefsdfdsf', responseJson);

        setRelatedPro(responseJson.data.lims_product_list);
        // setloader(false);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const lang = myContext.setting2name;
  return (
    <View style={styles.container}>
      {/* <EmptyHeader goBack={goBack} /> */}
      <MainHeader home={false} navigate={navigate} />
      <View style={styles.bodyContainer}>
        <ScrollView style={styles.scrollView}>
          <View style={styles.offContainer}>
            {/* {console.log('asa', product)} */}
            {/* {product.enable_free_product_option === '1' ||
            product.enable_free_product_option === 1
              ? alert('yes')
              : alert('no')} */}
            {product?.item?.enable_free_product_option === '1' ||
            product?.item?.enable_free_product_option === 1 ? (
              myContext.setting2name ? (
                <Text>{`يشترى ${product?.item?.no_of_products_buy} احصل على ${product?.item?.no_of_free_products} مجانا`}</Text>
              ) : (
                <Text>{`Buy ${product?.item?.no_of_products_buy} Get ${product?.item?.no_of_free_products} Free`}</Text>
              )
            ) : (
              <Text>{product?.item?.discount} % OFF</Text>
            )}
          </View>
          <View style={styles.imageview}>
            {stock ? null : (
              <View style={styles.blurBackGround}>
                <TouchableOpacity style={styles.outofStock}>
                  <Text style={styles.outofStockText}>
                    {myContext.setting2name ? OUT_OF_STOCK : 'Out of stock'}
                  </Text>
                </TouchableOpacity>
              </View>
            )}

            <Image
              style={styles.image}
              source={{
                uri: `${config.url}/public/images/product/${product.item?.base_image}`,
              }}
            />
          </View>
          <View style={styles.priceContainerMain}>
            <View style={styles.priceContainer}>
              <View>
                <Text style={styles.headingAmount}>${price}</Text>
                <Text style={styles.originalPrice}>
                  ${product?.item?.price}
                </Text>
              </View>
              <View>
                <TouchableOpacity onPress={() => handleClick()}>
                  <Icon
                    name="heart"
                    size={30}
                    color={
                      product?.item?.heart ? product?.item?.heart : grayColor
                    }
                  />
                </TouchableOpacity>
              </View>
              {/* {console.log('asdsads', product?.item)} */}
            </View>
            <Text style={styles.itemText}>
              {myContext.setting2name
                ? product?.item?.name_arabic
                : product?.item?.name}
            </Text>

            {stock ? null : (
              <View style={styles.notAvailableContainer}>
                <Icon name="ban" color={redColor} size={22} />
                <Text style={styles.notAvailableText}>
                  {myContext.setting2name
                    ? 'غير متوفر في المخزن'
                    : 'Not Available In Stock'}
                </Text>
              </View>
            )}
          </View>

          <View style={styles.descriptionContainer}>
            <View style={styles.descHeader}>
              <Text style={styles.descHeading}>
                {myContext.setting2name ? 'وصف' : 'Description'}
              </Text>
            </View>
            <Text>
              {myContext.setting2name
                ? product?.item?.product_details_arabic
                : product?.item?.product_details.replace(/<\/?[^>]+(>|$)/g, '')}
            </Text>
          </View>
          <View style={styles.addContainer}>
            {/* {console.log('related prossds', relatedPro)} */}
            <CardsContaiener
              title={
                myContext.setting2name ? 'الإعلانات ذات الصلة' : 'Related Ads'
              }
              flashOffersList={relatedPro}
              navigate={navigate}
            />
          </View>
        </ScrollView>
        <View style={styles.footer}>
          <TouchableOpacity onPress={() => goBack()}>
            <Icon name="arrow-left" color={blackColor} size={24} />
          </TouchableOpacity>
          {stock ? (
            <TouchableOpacity
              style={styles.addCartBtn}
              onPress={() => addtocart(product.item)}>
              <Text style={styles.addCartBtnText}>
                {myContext.setting2name ? ADD_TO_CART : 'Add to Cart'}
              </Text>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity style={styles.comingCartBtn}>
              <Text style={styles.addCartBtnText}>
                {myContext.setting2name ? COMMING_SOOM : 'Comming Soon'}
              </Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    position: 'relative',
  },

  bodyContainer: {
    flex: 1,
    justifyContent: 'space-between',
  },

  scrollView: {
    flex: 1,
  },

  offContainer: {
    backgroundColor: lightGray,
    paddingTop: '3@s',
    paddingBottom: '3@s',
    paddingLeft: '8@s',
    paddingRight: '8@s',
    position: 'absolute',
    top: '10@s',
    left: '10@s',
    borderRadius: 50,
    zIndex: 2,
  },

  imageview: {
    paddingBottom: '30@s',
    borderBottomColor: lightGray,
    borderBottomWidth: '1@s',
  },

  image: {
    height: '250@s',
    width: '100%',
    resizeMode: 'contain',
  },

  priceContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  headingAmount: {
    color: greenColor,
    fontSize: '20@s',
    fontWeight: 'bold',
  },

  priceContainerMain: {
    padding: '15@s',
    borderBottomColor: lightGray,
    borderBottomWidth: 1,
    justifyContent: 'space-between',
    shadowColor: lightGray,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.0,
    elevation: 1,
  },
  itemText: {
    color: blackColor,
    fontSize: '12@s',
    marginTop: '5@s',
  },

  descriptionContainer: {
    padding: '15@s',
    borderBottomColor: lightGray,
    borderBottomWidth: 1,
  },
  originalPrice: {
    fontSize: '10@s',
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
  },
  descHeader: {
    paddingBottom: '10@s',
    borderBottomWidth: 1,
    borderBottomColor: lightGray,
    marginBottom: '10@s',
  },
  descHeading: {
    fontSize: '13@s',
    color: blackColor,
  },
  addContainer: {
    padding: '10@s',
  },
  footer: {
    alignItems: 'center',
    padding: '10@s',
    backgroundColor: whiteColor,
    borderTopColor: lightGray,
    borderWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: '20@s',
  },

  blurBackGround: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    backgroundColor: 'rgba(255,255,255, 0.6)',
    zIndex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },

  addCartBtn: {
    backgroundColor: greenColor,
    width: '50%',
    alignItems: 'center',
    padding: '10@s',
    borderRadius: '3@s',
  },
  addCartBtnText: {
    color: whiteColor,
    fontSize: '12@s',
  },

  comingCartBtn: {
    backgroundColor: redColor,
    width: '50%',
    alignItems: 'center',
    padding: '10@s',
    borderRadius: '3@s',
  },

  outofStock: {
    backgroundColor: redColor,
    paddingLeft: '40@s',
    paddingRight: '40@s',
    paddingTop: '4@s',
    paddingBottom: '4@s',
    borderRadius: '30@s',
  },

  outofStockText: {
    color: whiteColor,
    fontSize: '12@s',
  },

  notAvailableContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: '10@s',
  },

  notAvailableText: {
    color: blackColor,
    fontSize: '16@s',
    marginLeft: '10@s',
    fontWeight: 'bold',
  },

  cartfooterItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default CartScreen;
