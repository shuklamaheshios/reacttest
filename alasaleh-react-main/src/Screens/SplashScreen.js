import React from 'react';
import { ImageBackground, View, Text, Image } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import image from '../Assets/images/logo_splash.png';
import splash_image from '../Assets/images/splash_bg.png';

const SplashScreen = () => {
  return (
    <View style={styles.container}>
      <ImageBackground
        resizeMethod="resize"
        resizeMode="cover"
        source={splash_image}
        style={styles.image}>
        <Image source={image} style={styles.text} />
      </ImageBackground>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
    alignItems: 'center',
  },

  text: {
    width: '58%',
    alignSelf: 'center',
    height: 170,
  },
});

export default SplashScreen;
