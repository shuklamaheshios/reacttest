import React, { useState, useContext } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { Dimensions, NativeModule } from 'react-native'
import AppContext from '../Utils/AppContext';
import { createSwitchNavigator, createAppContainer } from 'react-navigation';

import Card from '../Containers/CartContainer';

import {
  HomeContainer,
  CartContainer,
  RegisterContainer,
  LoginContainer,
  ProductByCategoryContainer,
  ProductCardListContainer,
  CategoryListContainer,
  AboutUsContainer,
  RefundContainer,
  PrivacyPolicyContainer,
  CareerContainer,
  ShippingPolicyContainer,
  HelpAndSupportContainer,
  NewsLetterContainer,
  MyCartContainer,
  SavedContainer,
  RefferelContainer,
  MyAccountContainer,
  OTPContainer,
  UpdatePasswordContainer,
  OrderListContainer,
  ReferelPointsContainer,
  WhishListContainer,
  ProfileContainer,
  PaymentMethodsContainer,
  BillingInformationContainer,
  BillingInvoiceContainer,
  MyOrderContainer,
  CreditCardContainer,
  MapViewContainer,
  SplashContainer,
  ManageCreditCardContainer,
  AddMobileContainer,
  ShippingMapContainer,
  TermsAndCondition,
  ForgotPasswordContainer,

} from '../Containers';
import { DrawerContentData } from '../Components';
import HeaderInput from '../Components/HeaderInput';
import AddSubscriptionScreen from '../Screens/AddSubscriptionScreen'
import PaypalPaymentScreen from '../Screens/PaypalPaymentScreen'


const { width } = Dimensions.get('window');

const HomeStack = createStackNavigator();

const App = () => {
  const [language, setLaguage] = useState(false);

  return (
    <HomeStack.Navigator initialRouteName="Splash">
      <HomeStack.Screen
        language={language}
        setLanguage={() => setLaguage()}
        options={{ headerShown: false }}
        name="Home"
        component={HomeContainer}
      />
      <HomeStack.Screen
        options={{ headerShown: false }}
        name="Cart"
        component={CartContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="Register"
        component={RegisterContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="Product"
        component={ProductByCategoryContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="ProductCard"
        component={ProductCardListContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="Category"
        component={CategoryListContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="About"
        component={AboutUsContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="Refund"
        component={RefundContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="Privacy"
        component={PrivacyPolicyContainer}
      />
      <HomeStack.Screen
        options={{ headerShown: false }}
        name="ShippingPolicy"
        component={ShippingPolicyContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="Career"
        component={CareerContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="Help"
        component={HelpAndSupportContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="News"
        component={NewsLetterContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="MyCart"
        component={MyCartContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="Saved"
        component={SavedContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="Refferel"
        component={RefferelContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="MyAccount"
        component={MyAccountContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="OTP"
        component={OTPContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="UpdatePassword"
        component={UpdatePasswordContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="OrderList"
        component={OrderListContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="ReferelPoint"
        component={ReferelPointsContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="WhishList"
        component={WhishListContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="Profile"
        component={ProfileContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="Payment"
        component={PaymentMethodsContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="BillingInformation"
        component={BillingInformationContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="Billing"
        component={BillingInvoiceContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="MyOrder"
        component={MyOrderContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="CreditCard"
        component={CreditCardContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="GoogleMap"
        component={MapViewContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="Splash"
        component={SplashContainer}
      />
      <HomeStack.Screen
        options={{ headerShown: false }}
        name="Login"
        component={LoginContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="SingleProduct"
        component={Card}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="ManageCard"
        component={ManageCreditCardContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="HeaderInput"
        component={HeaderInput}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="AddMobile"
        component={AddMobileContainer}
      />
      <HomeStack.Screen
        options={{ headerShown: false }}
        name="TermsAndCondition"
        component={TermsAndCondition}
      />
      <HomeStack.Screen
        options={{ headerShown: false }}
        name="ShippingMap"
        component={ShippingMapContainer}
      />

      <HomeStack.Screen
        options={{ headerShown: false }}
        name="ForgotPasword"
        component={ForgotPasswordContainer}
      />
        <HomeStack.Screen
        options={{ headerShown: false }}
        name="StripeCard"
        component={AddSubscriptionScreen}
        
      />
      <HomeStack.Screen
        options={{ headerShown: false }}
        name="PaypalPayment"
        component={PaypalPaymentScreen}
        
      />
      
    </HomeStack.Navigator>
  );
};

const Drawer = createDrawerNavigator();

const MyDrawer = (props) => {
  // const [positon, setPosition ] = useState('left')
  const myContext = useContext(AppContext);
  const navigate = async (routeName) => {
    const { navigation } = props;
    await navigation.navigate(routeName);
  };

  return (
    <Drawer.Navigator
      // drawerPosition={myContext.setting2name ? 'left' : 'right'}
      drawerPosition="left"
      initialRouteName="Home"
      drawerStyle={{ width: width / 1.3 }}
      drawerContent={(props) => (
        <DrawerContentData navigate={navigate} {...props} />
      )}>
      <Drawer.Screen name="Home" component={App} />
    </Drawer.Navigator>
  );
};

const MainScreen = createSwitchNavigator(
  {
    Home: {
      screen: App,
    },

    MyDrawer: {
      screen: MyDrawer,
    },
  },

  {
    initialRouteName: 'MyDrawer',
  },
);

export default createAppContainer(MainScreen);
