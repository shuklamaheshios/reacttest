const SHIPPING_AMOUNT = 'قيمة الشحن';
const TAX = 'ضريبة';
const CAREERS = 'وظائف';
const COMMING_SOOM = 'قريبا';
const ADD_TO_CART = 'أضف إلى السلة';
const OUT_OF_STOCK = 'إنتهى من المخزن';
const PROCEED = 'تقدم';
const TOTAL_AMOUNT = 'الإجمالي';

export {
  SHIPPING_AMOUNT,
  TAX,
  CAREERS,
  COMMING_SOOM,
  ADD_TO_CART,
  OUT_OF_STOCK,
  PROCEED,
  TOTAL_AMOUNT,
};
