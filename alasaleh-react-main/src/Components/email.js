import faker from 'faker';

const data = [];
const count = 10;

let range = (n) => Array.from(Array(n).keys());

for (let i in range(count)) {
  data.push({
    id: faker.random.uuid(),
    user: {
      name: faker.name.firstName(),
    },
    subject: faker.lorem.words(5),
  });
}

export default data;
