import React, {Component} from 'react';
import {Text, View, ActivityIndicator, Animated} from 'react-native';
import logo from '../Assets/images/logo_grey.png';
import {ScaledSheet} from 'react-native-size-matters';
import {greenColor, whiteColor} from '../Theme/colors';

// const CustomLoader = ({loader}) => {
class CustomLoader extends Component {
  constructor(props) {
    super(props);

    this.RotateValue = new Animated.Value(0);
  }

  componentDidMount() {
    this.StartImageRotateFunction();
  }

  StartImageRotateFunction = () => {
    this.RotateValue.setValue(0);
    Animated.timing(this.RotateValue, {
      toValue: 2,
      duration: 1500,
      useNativeDriver: true,
    }).start(() => this.StartImageRotateFunction());
  };

  render() {
    const RotateData = this.RotateValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg'],
    });
    return (
      <View style={styles.container}>
        <View
          style={{
            backgroundColor: greenColor,
            width: 70,
            height: 70,
            borderRadius: 100,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Animated.Image
            style={{
              width: 35,
              height: 35,
              transform: [{rotateY: RotateData}],
            }}
            source={logo}
          />
        </View>
      </View>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    padding: '10@s',
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
  lodaerBody: {
    height: '100@s',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: whiteColor,
  },

  empty: {
    width: '75%',
  },

  loaderContainer: {
    width: '25%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default CustomLoader;
