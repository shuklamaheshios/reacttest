import React, {useState, useContext, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Platform,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {blackColor, greenColor, whiteColor} from '../Theme/colors';
import Icon from 'react-native-vector-icons/FontAwesome';
import AppContext from '../Utils/AppContext';
import SearchInput, {createFilter} from 'react-native-search-filter';
import axios from 'axios';
import Storage from '../Utils/storage';
import config from '../Utils/config';
const KEYS_TO_FILTERS = ['name'];

function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const HeaderInput = ({navigate}) => {
  const [searchTerm, setSearchTerm] = useState('');
  const myContext = useContext(AppContext);
  const [result, setResult] = useState([]);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };
  //

  const [tokeen, settoken] = useState('');
  const [wishlist, setWishlist] = useState([]);
  const [loader, setloader] = useState(true);
  useEffect(() => {
    Storage.retrieveData('user').then((resp) => {
      getwish(resp.token);
      settoken(resp.token);
    });
  }, []);
  useEffect(() => {
    getColorForProduct();
    setloader(false);
  }, [wishlist]);
  const getColorForProduct = () => {
    wishlist?.map((item, index) => {
      item.heart = 'red';
    });
    forceUpdate();
  };
  const addinWish = (id) => {
    const params = {
      product_id: id,
    };
    axios
      .post(`${config.apiUrl}/whishlist`, params, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${tokeen}`,
        },
      })
      .then((res) => {})
      .catch((error) => {
        console.error(error);
      });
  };
  const removeWish = (id) => {
    const params = {
      product_id: id,
    };
    axios
      .post(`${config.apiUrl}/whishlist/delete`, params, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${tokeen}`,
        },
      })
      .then((res) => {})
      .catch((error) => {
        console.error(error);
      });
  };
  const getwish = (t) => {
    fetch(`${config.apiUrl}/wishlist/show`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${t}`,
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        setWishlist(responseJson.data);
        getColorForProduct();
      });
  };
  const handleColor = async (id) => {
    let ch = true;
    wishlist.map((item) => {
      if (parseInt(item.id) === id) {
        removeWish(id);
        ch = false;
      }
    });
    if (ch) {
      addinWish(id);
    }
  };

  //
  let token = null;
  let user = null;
  const addtocart = async (item) => {
    let pr = 0;
    let cartDet = null;
    await Storage.retrieveData('cartDataDetails').then((resp) => {
      cartDet = resp;
    });
    if (cartDet) {
      cartDet.push(item);
      Storage.storeData('cartDataDetails', cartDet);
    } else {
      Storage.storeData('cartDataDetails', [cartDet]);
    }
    await Storage.retrieveData('cartData').then((resp) => {
      token = resp;
    });
    await Storage.retrieveData('user').then((resp) => {
      user = resp;
    });
    if (token === null) {
      await fetch(`${config.apiUrl}/carts`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      })
        .then((response) => response.json())
        .then((responseJson) => {
          Storage.storeData('cartData', responseJson);
          token = responseJson;
        });
    }
    if (item.promotion === 1 || item.promotion === '1') {
      pr = parseFloat(item.promotion_price).toFixed(2);
    } else {
      const k = (parseFloat(item.price) * parseFloat(item.discount)) / 100;
      const calculated = item.price - k;
      let p = calculated.toFixed(2);
      if (p > calculated) {
        p = p - 0.01;
      }
      pr = p;
    }
    let toCount = 0;
    await fetch(
      `${config.apiUrl}/carts/show_cart_data/${token.cartToken}`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          cartKey: token.cartKey,
        }),
      },
    )
      .then((response) => response.json())
      .then((responseJson) => {
        responseJson['item_in_cart'].map((cartItem) => {
          if (cartItem.productID === item.id) {
            toCount = parseInt(cartItem.Quantity);
            console.log('toCount.id', cartItem?.Quantity);
          }
        });

        console.log('cart', responseJson['item_in_cart']);
        // responseJson['item_in_cart'].map(item => this.getProducts(item.Name))
      })
      .catch((e) => {
        console.log('error is', e);
        this.setState({
          loader: false,
        });
      });
    let qti = 1;
    if (toCount !== 0) {
      qti = toCount + 1;
    }
    await fetch(
      `${config.apiUrl}/carts/add_to_cart/${token.cartToken}`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          cartKey: token.cartKey,
          productID: item.id,
          quantity: qti.toString(),
          price: pr,
          image: item.image,
        }),
      },
    )
      .then((response) => response.json())
      .then((responseJson) => {});
  };

  const filteredEmails = result.filter(
    createFilter(searchTerm, KEYS_TO_FILTERS),
  );

  const searchUpdated = (term) => {
    setSearchTerm(term);
    getsearch(term);
  };
  const getsearch = (term) => {
    const data = {
      search_txt: term,
    };
    axios
      .post(`${config.apiUrl}/search_items`, data)
      .then((res) => {
        setResult(res.data.data);
      })
      .catch((error) => {
        console.error(error);
      });
  };
  const getAproduct = async (name) => {
    await fetch(`${config.apiUrl}/productDetails/${name}`)
      .then((response) => response.json())
      .then((responseJson) => {
        navigate('SingleProduct', {
          item: responseJson.data,
          addtocart: addtocart,
          handleColor: handleColor,
        });
      })
      .catch((error) => {
        console.error(error);
      });
  };
  return (
    <View style={{width: '100%'}}>
      <View
        style={[
          myContext.setting2name
            ? styles.searchContainerUrdu
            : styles.searchContainer,
        ]}>
        <View style={{width: '75%', position: 'relative', zIndex: 100}}>
          {/* <TextInput
            placeholderTextColor={blackColor}
            placeholder={myContext.setting2name ? 'بحث' : 'Search Item'}
            style={[
              myContext.setting2name
                ? styles.searchInputArabic
                : styles.searchInput,
            ]}
          /> */}
          <SearchInput
            placeholderTextColor={blackColor}
            clearIconViewStyles={
              myContext.setting2name
                ? {
                    position: 'absolute',
                    top: Platform.OS === 'ios' ? 5 : 10,
                    left: 10,
                  }
                : {
                    position: 'absolute',
                    top: Platform.OS === 'ios' ? 5 : 10,
                    right: 10,
                  }
            }
            clearIcon={<Icon name="times" sty color={blackColor} size={18} />}
            onChangeText={(term) => {
              searchUpdated(term);
            }}
            style={[
              myContext.setting2name
                ? styles.searchInputArabic
                : styles.searchInput,
            ]}
            placeholder={myContext.setting2name ? 'بحث' : 'Search Item'}
          />
          {searchTerm ? (
            <ScrollView
              style={{
                backgroundColor: whiteColor,
                position: 'relative',
                width: '133%',
                zIndex: 4,
                height: '100%',
              }}>
              {filteredEmails.map((email) => {
                return (
                  <TouchableOpacity
                    onPress={() => getAproduct(email.slug)}
                    key={email.id}
                    style={styles.emailItem}>
                    <View style={{flex: 100, backgroundColor: whiteColor}}>
                      <Text style={{color: blackColor, fontSize: 16}}>
                        {myContext.setting2name
                          ? email.name_arabic
                          : email.name}
                      </Text>
                    </View>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          ) : null}
        </View>
        <TouchableOpacity style={{width: '25%'}}>
          <View
            style={[
              myContext.setting2name
                ? styles.searchBtnContainerArabic
                : styles.searchBtnContainer,
            ]}>
            <Icon
              name="search"
              color={whiteColor}
              size={Platform.OS === 'ios' ? 16 : 22}
            />
            <Text style={styles.searchtext}>
              {myContext.setting2name ? 'بحث' : 'Search'}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  searchContainer: {
    flexDirection: 'row',
    marginTop: '10@s',
    position: 'relative',
    zIndex: 100,
  },

  searchContainerUrdu: {
    flexDirection: 'row-reverse',
    marginTop: '10@s',
  },

  searchInput: {
    width: '100%',
    backgroundColor: whiteColor,
    paddingTop: '5@s',
    paddingBottom: '5@s',

    // paddingTop: Platform.OS === 'ios' ? '3@s' : '5.5@s',
    // paddingBottom: Platform.OS === 'ios' ? '0.5@s' : '5.5@s',
    paddingLeft: '12@s',
    borderTopLeftRadius: '4@s',
    borderBottomLeftRadius: '4@s',
    fontSize: '14@s',
  },

  searchInputArabic: {
    width: '100%',
    backgroundColor: whiteColor,
    paddingTop: '5@s',
    paddingBottom: '5@s',
    paddingLeft: '12@s',
    borderTopRightRadius: '4@s',
    borderBottomRightRadius: '4@s',
    textAlign: 'right',
    paddingRight: '10@s',
    fontSize: '16@s',
  },

  searchBtnContainer: {
    backgroundColor: greenColor,
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'space-between',
    borderTopRightRadius: '4@s',
    borderBottomRightRadius: '4@s',
    paddingTop: '8@s',
    paddingBottom: '8@s',
    paddingLeft: '8@s',
    paddingRight: '12@s',
  },

  searchBtnContainerArabic: {
    backgroundColor: greenColor,
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'space-between',
    borderTopLeftRadius: '4@s',
    borderBottomLeftRadius: '4@s',
    paddingTop: '8@s',
    paddingBottom: '8@s',
    paddingLeft: '8@s',
    paddingRight: '12@s',
  },
  searchtext: {
    fontSize: '12@s',
    color: whiteColor,
    marginLeft: '5@s',
  },

  emailItem: {
    borderBottomWidth: 0.5,
    borderColor: whiteColor,
    padding: 10,
    flex: 1,
    backgroundColor: whiteColor,
  },
  emailSubject: {
    color: 'rgba(0,0,0,0.5)',
  },
});

export default HeaderInput;
