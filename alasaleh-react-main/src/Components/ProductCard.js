import React, {useState, useContext, useEffect} from 'react';
import {
  Text,
  View,
  Dimensions,
  Image,
  TouchableOpacity,
  ToastAndroid,
  Button,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/FontAwesome';
import Toast from 'react-native-simple-toast';
import maxum from '../Assets/images/maxum.png';
import {
  blackColor,
  greenColor,
  lightGray,
  redColor,
  whiteColor,
} from '../Theme/colors';
import Storage from '../Utils/storage';
import AppContext from '../Utils/AppContext';
import config from '../Utils/config';


function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const Card = ({navigate, item, changeheart, handleColor, image, addtocart}) => {
  const myContext = useContext(AppContext);
  const [discountString, setDiscountString] = useState(null);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };
  const [price, setPrice] = useState(0);
  useEffect(() => {
    if (item?.promotion === '1' || item?.promotion === 1) {
      // let chType = parseFloat(item?.promotion_price);
      // let fix = chType.toFixed(2);
      // setPrice(fix);
      setPrice(parseFloat(item.promotion_price).toFixed(2));
    } else {
      const k = (parseFloat(item?.price) * parseFloat(item?.discount)) / 100;
      const calculated = item?.price - k;
      let p = calculated.toFixed(2);
      if (p > calculated) {
        p = p - 0.01;
      }
      setPrice(p);
    }
    if (
      item?.enable_free_product_option === 1 ||
      item?.enable_free_product_option === '1'
    ) {
      myContext.setting2name
        ? setDiscountString(
            `يشترى ${item.no_of_products_buy} احصل على ${item.no_of_free_products} مجانا`,
          )
        : setDiscountString(
            `Buy ${item?.no_of_products_buy} Get ${item.no_of_free_products} Free`,
          );
    }
  }, []);

  const handleColorLogic = (id) => {
    return handleColor ? handleColor(id) : null;
  };

  return (
    <View>
      {item?.qty >= 5 ? (
        <View style={styles.container}>
          <TouchableOpacity
            onPress={() => navigate('Cart', {item, addtocart, handleColor})}>
            <View
              style={
                myContext.setting2name
                  ? styles.offContainerArabic
                  : styles.offContainer
              }>
              {discountString ? (
                <Text style={styles.offText}>{discountString}</Text>
              ) : (
                <View>
                  {myContext.setting2name ? (
                    <Text style={styles.offText}>{item?.discount} % إيقاف</Text>
                  ) : (
                    <Text style={styles.offText}>{item?.discount} % OFF</Text>
                  )}
                </View>
              )}
            </View>

            <View
              style={
                myContext.setting2name
                  ? styles.heartContainerArabic
                  : styles.heartContainer
              }>
              <TouchableOpacity onPress={() => handleColorLogic(item?.id)}>
                <Icon
                  name="heart"
                  size={16}
                  color={item?.heart ? item?.heart : lightGray}
                />
              </TouchableOpacity>
            </View>

            <View style={{justifyContent: 'space-between'}}>
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginTop: 50,
                }}>
                <Image
                  style={styles.image}
                  source={{
                    uri: `${config.url}/public/images/product/${item.base_image}`,
                  }}
                  resizeMode="center"
                />
              </View>

              <View>
                <Text numberOfLines={1} style={styles.description}>
                  {myContext.setting2name ? item?.name_arabic : item?.name}
                </Text>
                <View style={styles.priceContainer}>
                  <View>
                    <Text style={styles.discountPrice}>${price}</Text>
                    <Text style={styles.originalPrice}>${item?.price}</Text>
                  </View>
                  <TouchableOpacity onPress={() => addtocart(item)}>
                    <View style={styles.cartBtn}>
                      <Icon
                        name="shopping-cart"
                        color={whiteColor}
                        size={12}
                        style={{marginRight: 10}}
                      />
                      <Text style={styles.addToCart}>
                        {myContext.setting2name
                          ? 'أضف إلى السلة'
                          : 'Add to Cart'}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      ) : (
        <View style={styles.outOfStockContainer}>
          <View style={styles.blurView}>
            <TouchableOpacity
              onPress={() => navigate('Cart', {item})}
              style={styles.commingSoonbtn}>
              <Text style={styles.commingSoonText}>
                {myContext.setting2name ? 'إنتهى من المخزن' : 'Out of stock'}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => handleColorLogic(item?.id)}
              style={
                myContext.setting2name
                  ? styles.heartContainerArabic
                  : styles.heartContainer
              }>
              <Icon
                name="heart"
                size={16}
                color={item?.heart ? item?.heart : lightGray}
              />
            </TouchableOpacity>
          </View>
          <View style={{padding: 4}}>
            <TouchableOpacity onPress={() => navigate('Cart', item)}>
              <View
                style={
                  myContext.setting2name
                    ? styles.offContainerArabic
                    : styles.offContainer
                }>
                {/* {myContext.setting2name ? (
                  <Text style={styles.offText}>{item?.discount} % إيقاف</Text>
                ) : (
                  <Text style={styles.offText}>{item?.discount} % OFF</Text>
                )} */}
                {discountString ? (
                  <Text style={styles.offText}>{discountString}</Text>
                ) : (
                  <View>
                    {myContext.setting2name ? (
                      <Text style={styles.offText}>
                        {item?.discount} % إيقاف
                      </Text>
                    ) : (
                      <Text style={styles.offText}>{item?.discount} % OFF</Text>
                    )}
                  </View>
                )}
              </View>

              <View style={{justifyContent: 'space-between'}}>
                <View
                  style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginTop: 50,
                  }}>
                  <Image
                    style={styles.image}
                    source={{
                      uri: `${config.url}/public/images/product/${item.base_image}`,
                    }}
                    resizeMode="center"
                  />
                </View>

                <View>
                  <Text numberOfLines={1} style={styles.description}>
                    {myContext.setting2name ? item?.name_arabic : item?.name}
                  </Text>
                  <View style={styles.priceContainer}>
                    <View>
                      <Text numberOfLines={1} style={styles.discountPrice}>
                        ${price}
                      </Text>
                      <Text style={styles.originalPrice}>${item?.price}</Text>
                    </View>
                    <TouchableOpacity>
                      <View style={styles.outOfStockcartBtn}>
                        <Text style={styles.addToCart}>
                          {myContext.setting2name ? 'قريبا' : 'Coming Soon'}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      )}
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    // width: cardwidth / 3.2,
    // marginLeft: '3@s',
    padding: '4@s',
    backgroundColor: whiteColor,
    borderRadius: '7@s',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,

    elevation: 10,
    marginRight: '5@s',
    position: 'relative',
    marginBottom: '3@s',
  },

  outOfStockContainer: {
    backgroundColor: whiteColor,
    borderRadius: '7@s',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,

    elevation: 10,
    marginRight: '5@s',
    position: 'relative',
    marginBottom: '3@s',
  },

  blurView: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(255,255,255, 0.5)',
    zIndex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  offContainer: {
    backgroundColor: lightGray,
    position: 'absolute',
    top: '5@s',
    left: '5@s',
    paddingTop: '2@s',
    paddingBottom: '2@s',
    paddingLeft: '3@s',
    paddingRight: '3@s',
    borderRadius: 50,
  },

  offContainerArabic: {
    backgroundColor: lightGray,
    position: 'absolute',
    top: '10@s',
    right: '5@s',
    paddingTop: '2@s',
    paddingBottom: '2@s',
    paddingLeft: '3@s',
    paddingRight: '3@s',
    borderRadius: 50,
  },

  offText: {
    fontSize: '8@s',
  },
  heartContainer: {
    position: 'absolute',
    top: '20@s',
    right: '5@s',
    zIndex: 4,
  },

  heartContainerArabic: {
    position: 'absolute',
    top: '30@s',
    left: '10@s',
    zIndex: 4,
  },

  image: {
    height: '100@s',
    width: '100%',
  },
  description: {
    fontSize: '9@s',
    lineHeight: '14@s',
    fontWeight: '600',
  },
  priceContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: '5@s',
  },
  discountPrice: {
    color: blackColor,
    fontWeight: 'bold',
    fontSize: '10@s',
    width: '35@s',
  },

  originalPrice: {
    fontSize: '8@s',
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
  },
  cartBtn: {
    backgroundColor: greenColor,
    padding: '2@s',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    borderRadius: '2@s',
  },

  outOfStockcartBtn: {
    backgroundColor: redColor,
    padding: '2@s',
    paddingLeft: '4@s',
    paddingRight: '4@s',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    borderRadius: '2@s',
  },
  addToCart: {
    color: whiteColor,
    fontSize: '8@s',
  },

  commingSoonbtn: {
    backgroundColor: redColor,
    padding: '2@s',
    paddingLeft: '4@s',
    paddingRight: '4@s',
    borderRadius: '5@s',
    position: 'relative',
    zIndex: 100,
  },
  commingSoonText: {
    color: whiteColor,
    fontSize: '9@s',
  },
});
export default Card;
