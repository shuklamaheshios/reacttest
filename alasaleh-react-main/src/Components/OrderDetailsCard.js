import React, {useState, useContext} from 'react';
import {Text, View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {
  blackColor,
  blueColor,
  grayColor,
  greenColor,
  lightGray,
  whiteColor,
} from '../Theme/colors';
import AppContext from '../Utils/AppContext';

function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const MyOrderCard = ({list}) => {
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };
  return (
    <View style={styles.container}>
      <View style={styles.body}>
        <Text style={styles.orderHeadingColor}>
          {myContext.setting2name ? 'اسم المنتج' : 'Product Name'}
        </Text>
        <View style={styles.tokenNumber}>
          <Text style={styles.tokenText}>{list.name}</Text>
        </View>

        <View style={styles.tableContainer}>
          <View style={styles.shippingCoast}>
            <Text numberOfLines={2} style={styles.tabletext}>
              {myContext.setting2name ? 'العدد' : 'Quantity'}
            </Text>
          </View>
          <View style={styles.shippingCoast}>
            <Text numberOfLines={2} style={styles.tabletext}>
              {myContext.setting2name ? 'ضريبة' : 'Tax'}
            </Text>
          </View>
          <View style={styles.shippingCoast}>
            <Text numberOfLines={2} style={styles.tabletext}>
              {myContext.setting2name ? 'سعر الوحده' : 'Unit Price'}
            </Text>
          </View>
          <View style={styles.lastItemShpping}>
            <Text numberOfLines={2} style={styles.tabletext}>
              {myContext.setting2name ? 'مجموع' : 'Total'}
            </Text>
          </View>
        </View>
        <View style={styles.downTable}>
          <View style={styles.downShipping}>
            <Text numberOfLines={2} style={styles.downtabletext}>
              {list.qty}
            </Text>
          </View>
          <View style={styles.downShipping}>
            <Text numberOfLines={2} style={styles.downtabletext}>
              ${list.tax}
            </Text>
          </View>
          <View style={styles.downShipping}>
            <Text numberOfLines={2} style={styles.downtabletext}>
              ${list.net_unit_price}
            </Text>
          </View>
          <View style={styles.lastDownRecord}>
            <Text numberOfLines={2} style={styles.totalAmountText}>
              ${list.total}
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    backgroundColor: grayColor,
    marginBottom: '10@s',
  },

  orderHeadingColor: {
    color: blackColor,
    fontSize: '13@s',
    marginBottom: '5@s',
    fontWeight: 'bold',
  },

  body: {
    padding: '5@s',
    backgroundColor: whiteColor,
    borderRadius: '5@s',
  },
  tokenNumber: {
    backgroundColor: lightGray,
    marginTop: '5@s',
  },
  tokenText: {
    color: blackColor,
    letterSpacing: '0.6@s',
  },
  statusContainer: {
    flexDirection: 'row',
    marginTop: '10@s',
    alignItems: 'center',
  },
  deliveryText: {
    color: blackColor,
    fontSize: '13@s',
  },
  statustextContainer: {
    padding: '1@s',
    paddingLeft: '7@s',
    paddingRight: '7@s',
    backgroundColor: blueColor,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: '20@s',
  },
  statusText: {
    color: whiteColor,
    letterSpacing: '0.4@s',
  },

  tableContainer: {
    marginTop: '10@s',
    flexDirection: 'row',
  },

  shippingCoast: {
    width: '25%',
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    borderBottomColor: lightGray,
    borderBottomWidth: 1,
    borderRightColor: lightGray,
    borderRightWidth: 1,
    paddingBottom: '5@s',
  },

  lastItemShpping: {
    width: '25%',
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    borderBottomColor: lightGray,
    borderBottomWidth: 1,
  },
  tabletext: {
    textAlign: 'center',
    fontSize: '11@s',
    color: blackColor,
    fontWeight: 'bold',
  },

  downtabletext: {
    color: '#333',
  },
  downTable: {
    flexDirection: 'row',
  },

  downShipping: {
    width: '25%',
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    borderRightColor: lightGray,
    borderRightWidth: 1,
    paddingTop: '5@s',
    paddingBottom: '5@s',
  },
  lastDownRecord: {
    width: '25%',
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    paddingTop: '5@s',
    paddingBottom: '5@s',
  },
  totalAmountText: {
    color: greenColor,
    fontSize: '11@s',
    fontWeight: '600',
  },
  dateContainer: {
    padding: '5@s',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  purchaseText: {
    color: blackColor,
    fontWeight: 'bold',
  },
});

export default MyOrderCard;
