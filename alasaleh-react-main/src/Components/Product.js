import React, {useState} from 'react';
import {View, TouchableOpacity, Image, Text, Dimensions} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {blackColor, greenColor, whiteColor} from '../Theme/colors';
import fruits from '../Assets/images/fruits.jpg';
import logo_grey from '../Assets/images/rounded_placeholder.png';
import config from '../Utils/config';


let check = true;
const Product = ({image, title, button, handleColor, id, navigate}) => {
  const handleColorLogic = (id) => {
    return handleColor ? handleColor(id) : navigate('ProductCard', {title, id});
  };
  if (check) {
    handleColorLogic(id);
    check = false;
  }

  // const [myButton, setMyButton] = useState(false)
  return (
    <View style={styles.itemContainer}>
      <TouchableOpacity
        onPress={() => handleColorLogic(id)}
        style={[button ? styles.greenButton : styles.itemList]}>
        {image ? (
          <Image
            source={{
              uri: `${config.url}/public/${image}`,
            }}
            style={styles.image}
          />
        ) : (
          <Image source={logo_grey} style={styles.emptyImage} />
        )}

        <View style={{width: 70}}>
          <Text
            numberOfLines={2}
            style={[button ? styles.whiteText : styles.itemListText]}>
            {title}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = ScaledSheet.create({
  itemContainer: {
    flex: 0.5,
    marginBottom: '5@s',
    marginTop: '10@s',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    zIndex: 0,
  },

  itemList: {
    alignItems: 'center',
    width: '80@s',
    paddingTop: '5@s',
    // paddingBottom: '5@s',
  },

  greenButton: {
    alignItems: 'center',
    width: '80@s',
    paddingTop: '5@s',
    paddingBottom: '7@s',
    backgroundColor: greenColor,
    color: whiteColor,
  },

  image: {
    width: '60@s',
    height: '60@s',
    borderRadius: 50,
    borderColor: greenColor,
    borderWidth: 2,
    resizeMode: 'cover',
  },

  emptyImage: {
    width: '60@s',
    height: '60@s',
    borderRadius: 100,
    borderColor: greenColor,
    borderWidth: 2,
    resizeMode: 'cover',
  },

  itemListText: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    textAlign: 'center',
    marginTop: '5@s',
    height: '35@s',
    color: blackColor,
  },

  whiteText: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    textAlign: 'center',
    marginTop: '5@s',
    height: '35@s',
    color: whiteColor,
  },
});
export default Product;
