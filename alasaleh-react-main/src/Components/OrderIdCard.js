import React, {useState, useContext, useEffect} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {OrderDetailsModal, WhyOrderCancelModal} from '../Screens';
import {
  blackColor,
  blueColor,
  greenColor,
  lightGray,
  redColor,
  whiteColor,
} from '../Theme/colors';
import AppContext from '../Utils/AppContext';
import axios from 'axios';
import Storage from '../Utils/storage';
import config from '../Utils/config';
function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const MyOrderCard = ({list, setModalVisible, cancelModal}) => {
  const [orderDetailsModal, setOrderDetailsModal] = useState(false);
  const [whyCancelModal, setWhyCancelModal] = useState(false);
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const [orderDetails, setOrderDetails] = useState([]);
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };
  useEffect(() => {
    getDetails();
  }, []);
  const getDetails = () => {
    Storage.retrieveData('user').then((resp) => {
      axios
        .get(`${config.apiUrl}/order-details/${list.id}`, {
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${resp.token}`,
          },
        })
        .then((res) => {
          setOrderDetails(res.data.data);
        })
        .catch((error) => {
          console.error(error);
        });
    });
  };

  const cancelOrder = () => {
    Storage.retrieveData('user').then((resp) => {
      // const params = {
      //   order_id: list.id,
      // };

      const params = {
        order_id: list.id,
        cancel_reason: 'xyz',
        cancel_note: 'xyz',
      };
      axios
        .post(
          `${config.apiUrl}/send_order_cancel_request`,
          params,
          {
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              Authorization: `Bearer ${resp.token}`,
            },
          },
        )
        // .post(`${config.apiUrl}/send_order_cancel_request`, params)
        .then((res) => {
          // setOrderDetails(res.data.data);
        })
        .catch((error) => {
          console.error(error);
        });
    });
  };
  return (
    <View style={styles.container}>
      <OrderDetailsModal
        modalVisible={orderDetailsModal}
        setModalVisible={setOrderDetailsModal}
        id={list.id}
        list={orderDetails}
      />

      <View style={styles.body}>
        <View style={styles.dateContainer}>
          <Text style={styles.purchaseText}>
            {myContext.setting2name ? 'تاريخ الشراء' : 'Date Purchase'}
          </Text>
          <Text>{list.updated_at}</Text>
        </View>
        <Text style={styles.orderHeadingColor}>
          {myContext.setting2name ? 'معرف تتبع الطلب' : 'Order Tracking ID'}
        </Text>
        <View style={styles.tokenNumber}>
          <Text style={styles.tokenText}>{list.reference_no}</Text>
        </View>
        <View style={styles.statusContainer}>
          <Text style={styles.deliveryText}>
            {myContext.setting2name ? 'الولاية التسليم' : 'Delivery Status'}
          </Text>
          {list.status === '1' || list.status === 1 ? (
            <View style={styles.pendingContainer}>
              <Text style={styles.statusText}>
                {myContext.setting2name ? 'قيد الانتظار' : 'Pending'}
              </Text>
            </View>
          ) : list.status === '2' || list.status === 2 ? (
            <View style={styles.statustextContainer}>
              <Text style={styles.statusText}>
                {myContext.setting2name ? 'في تقدم' : 'In Progress'}
              </Text>
            </View>
          ) : list.status === '3' || list.status === 3 ? (
            <View style={styles.statustextContainer}>
              <Text style={styles.statusText}>
                {myContext.setting2name ? 'منجز' : 'Completed'}
              </Text>
            </View>
          ) : (
            <></>
          )}
        </View>

        <View style={styles.isContainer}>
          <Text style={styles.deliveryText}>
            {myContext.setting2name ? 'تم إلغاء الطلب' : 'Is order Canceled'}
          </Text>
          <View style={styles.notCancledContainer}>
            {list.is_order_canceled === '0' || list.is_order_canceled === 0 ? (
              <Text style={styles.statusText}>
                {myContext.setting2name
                  ? 'غير ملغى'
                  : 'Not Canceled'}
              </Text>
            ) : (
              <Text style={styles.statusText}>
                {myContext.setting2name ? 'غير ملغى' : 'Canceled'}
              </Text>
            )}
          </View>
        </View>

        {/* <View style={styles.itemContainer}>
          <Text style={styles.itemText}>
            {myContext.setting2name ? 'العناصر' : 'Items'}
          </Text>
        </View> */}

        {/* <View style={styles.itemContainer}>
          <Text style={styles.itemNumber}>{list.item}</Text>
        </View> */}
        <View style={styles.tableContainer}>
          <View style={styles.shippingCoast}>
            <Text numberOfLines={2} style={styles.tabletext}>
              {myContext.setting2name ? 'تكلفة الشحن' : 'Shipping Coast'}
            </Text>
          </View>
          <View style={styles.shippingCoast}>
            <Text numberOfLines={2} style={styles.tabletext}>
              {myContext.setting2name ? 'ضريبة' : 'Tax'}
            </Text>
          </View>
          <View style={styles.shippingCoast}>
            <Text numberOfLines={2} style={styles.tabletext}>
              {myContext.setting2name ? 'خصم القسيمة' : 'Coupon Discount'}
            </Text>
          </View>
          <View style={styles.lastItemShpping}>
            <Text numberOfLines={2} style={styles.tabletext}>
              {myContext.setting2name ? 'المبلغ الإجمالي' : 'Total Amount'}
            </Text>
          </View>
        </View>
        <View style={styles.downTable}>
          <View style={styles.downShipping}>
            <Text numberOfLines={2} style={styles.downtabletext}>
              ${list.shipping_cost}
            </Text>
          </View>
          <View style={styles.downShipping}>
            <Text numberOfLines={2} style={styles.downtabletext}>
              ${list.total_tax}
            </Text>
          </View>
          <View style={styles.downShipping}>
            <Text numberOfLines={2} style={styles.downtabletext}>
              ${list.coupon_discount}
            </Text>
          </View>
          <View style={styles.lastDownRecord}>
            <Text numberOfLines={2} style={styles.totalAmountText}>
              ${list.total_price}
            </Text>
          </View>
        </View>
        <View style={styles.footer}>
          <TouchableOpacity
            onPress={() => {
              // setModalVisible(false), cancelOrder();
              setModalVisible(false)
            }}
            style={styles.cancelButton}>
            <Text style={styles.footerBtnText}>Back</Text>
          </TouchableOpacity>
          {/* <TouchableOpacity
            onPress={() => {
              setOrderDetailsModal(true);
            }}
            style={styles.viewButton}>
            <Text style={styles.footerBtnText}>View</Text>
          </TouchableOpacity> */}
        </View>
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    backgroundColor: whiteColor,
    marginBottom: '10@s',
  },

  itemContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: '10@s',
    borderBottomColor: blackColor,
    borderBottomWidth: 1,
  },

  itemNumber: {
    color: blackColor,
    fontSize: '14@s',
  },

  itemText: {
    color: blackColor,
    fontSize: '14@s',
    fontWeight: 'bold',
  },

  orderHeadingColor: {
    color: blackColor,
    fontSize: '13@s',
    marginBottom: '5@s',
  },

  body: {
    padding: '5@s',
    backgroundColor: whiteColor,
    borderRadius: '5@s',
  },
  tokenNumber: {
    backgroundColor: lightGray,
    marginTop: '5@s',
  },
  tokenText: {
    color: blackColor,
    letterSpacing: '0.6@s',
  },
  statusContainer: {
    flexDirection: 'row',
    marginTop: '10@s',
    alignItems: 'center',
    borderBottomColor: blackColor,
    borderBottomWidth: 1,
    paddingBottom: '7@s',
    borderTopColor: blackColor,
    borderTopWidth: 1,
    paddingTop: '7@s',
  },

  isContainer: {
    flexDirection: 'row',
    marginTop: '10@s',
    alignItems: 'center',
    borderBottomColor: blackColor,
    borderBottomWidth: 1,
    paddingBottom: '7@s',
  },
  deliveryText: {
    color: blackColor,
    fontSize: '13@s',
  },
  statustextContainer: {
    padding: '1@s',
    paddingLeft: '7@s',
    paddingRight: '7@s',
    backgroundColor: blueColor,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: '20@s',
  },

  pendingContainer: {
    padding: '1@s',
    paddingLeft: '7@s',
    paddingRight: '7@s',
    backgroundColor: '#CCCC00',
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: '20@s',
  },

  notCancledContainer: {
    padding: '1@s',
    paddingLeft: '7@s',
    paddingRight: '7@s',
    backgroundColor: greenColor,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: '20@s',
  },
  statusText: {
    color: whiteColor,
    letterSpacing: '0.4@s',
  },

  tableContainer: {
    marginTop: '10@s',
    flexDirection: 'row',
  },

  shippingCoast: {
    width: '25%',
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    borderBottomColor: lightGray,
    borderBottomWidth: 1,
    borderRightColor: lightGray,
    borderRightWidth: 1,
    paddingBottom: '5@s',
  },

  lastItemShpping: {
    width: '25%',
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    borderBottomColor: lightGray,
    borderBottomWidth: 1,
  },
  tabletext: {
    textAlign: 'center',
    fontSize: '11@s',
    color: blackColor,
    fontWeight: '700',
  },

  downtabletext: {
    color: '#333',
  },
  downTable: {
    flexDirection: 'row',
  },

  downShipping: {
    width: '25%',
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    borderRightColor: lightGray,
    borderRightWidth: 1,
    paddingTop: '5@s',
    paddingBottom: '5@s',
  },
  lastDownRecord: {
    width: '25%',
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    paddingTop: '5@s',
    paddingBottom: '5@s',
  },
  totalAmountText: {
    color: greenColor,
    fontSize: '11@s',
    fontWeight: '600',
  },
  dateContainer: {
    padding: '5@s',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  purchaseText: {
    color: blackColor,
    fontWeight: 'bold',
    paddingRight: '7@s',
  },

  footer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '15@s',

  },

  cancelButton: {
    backgroundColor: greenColor,
    padding: '7@s',
    width: '49%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '7@s',
  },
  viewButton: {
    backgroundColor: redColor,
    width: '49%',
    padding: '7@s',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '7@s',
  },

  footerBtnText: {
    color: whiteColor,
    fontSize: '14@s',
  },
});

export default MyOrderCard;
