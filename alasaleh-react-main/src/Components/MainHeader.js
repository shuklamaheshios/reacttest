import React, {useState, useContext, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  NativeModules,
  Image,
  Platform,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {blackColor, greenColor, headerColor, whiteColor} from '../Theme/colors';
import logo_text from '../Assets/images/text_logo.png';
import heartImage from '../Assets/images/heart_light_grey.png';
import nav_icon from '../Assets/images/nav_icon.png';
import profile_white_bg from '../Assets/images/profile_white_bg.png';
import cartImage from '../Assets/images/cart_white.png';
import SearchInput, {createFilter} from 'react-native-search-filter';
import HeaderInput from './HeaderInput';
import AppContext from '../Utils/AppContext';
import Storage from '../Utils/storage';
import config from '../Utils/config';
import axios from 'axios';
import { useIsFocused } from "@react-navigation/native";

function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}
const MainHeader = ({navigate, home, count, wishCount}) => {
  const [searchTerm, setSearchTerm] = useState('');
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const [cartCount, setCartCount] = useState(0);
  const [wishlistCount, setWishlistCount] = useState(0);
  const [img, setimg] = useState(null);
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };

  const isFocused = useIsFocused();

  useEffect(() => {
    getCartCount();
    getwish();
    Storage.retrieveData('user').then((resp) => {
      getProfile(resp);
    });
  },[isFocused]);


  const getProfile = async (user) => {
    let data = {
      id: user.user_id,
    };
    await axios
      .get(`${config.apiUrl}/user/${user.user_id}`, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + user.token,
          // Authorization: `bearer ${token}`,
        },
      })
      .then((res) => {
        setimg(res.data.data.image);
        console.log('imageeeeeee', res.data.data.image);
      })
      .catch((error) => {
        console.error(error);
      });
  };
  const getCartCount = async () => {
    let token = null;
    await Storage.retrieveData('cartData').then((resp) => {
      token = resp;
    });
    if (token) {
      await fetch(`${config.apiUrl}/carts/show_cart_data`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          cartKey: token.cartKey,
        }),
      })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log('Items in Cart', responseJson['item_in_cart'].length);
          let cartQC = 0;
          responseJson['item_in_cart'].map((cartData) => {
            cartQC = parseInt(cartData.Quantity) + cartQC;
          });
          console.log('uppp', cartQC);

          setCartCount(cartQC);
        })
        .catch((e) => {
          // this.setState({
          //   loader: false,
          // });
        });
    }
  };
  const getwish = async () => {
    let token = null;
    await Storage.retrieveData('user').then((resp) => {
      if (resp == 'Unauthorised') {
        token = null;
      } else {
        token = resp;
      }
    });
    fetch(`${config.apiUrl}/wishlist/show`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token.token}`,
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        setWishlistCount(responseJson.data.length);
      });
  };

  const navigationHeader = async () => {
    let isLogin = null;
    await Storage.retrieveData('user').then((resp) => {
      isLogin = resp;
    });

    if (isLogin) {
      navigate('MyAccount');
    } else {
      navigate('Login');
    }
  };

  return (
    <View style={styles.container}>
      <View
        style={[
          myContext.setting2name
            ? styles.headerContainerUrdu
            : styles.headerContainer,
        ]}>
        <View>
          <TouchableOpacity onPress={() => navigate('Home')}>
            <Image source={logo_text} style={styles.headingLogo} />
          </TouchableOpacity>
          <Text
            style={{
              color: whiteColor,
              textAlign: 'center',
              fontSize: 9,
              marginTop: 2,
            }}>
            One place is enough
          </Text>
        </View>
        <View
          style={[
            myContext.setting2name ? styles.rightItemsUrdu : styles.rightItems,
          ]}>
          <TouchableOpacity onPress={handleClick}>
            <Text style={styles.rightitemsList}>
              {myContext.setting2name ? 'العربية' : 'العربية'}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={handleClick}>
            <Text style={styles.rightitemsList}>
              {myContext.setting2name ? 'Eng' : 'Eng'}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigate('WhishList')}>
            <View style={styles.rightitemsList}>
              <Image source={heartImage} style={styles.iconImage} />
              {/* <View style={styles.notificationContainer}>
                <Text numberOfLines={1} style={styles.notificationText}>
                  {wishCount ? wishCount : wishlistCount}
                </Text>
              </View> */}
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigate('MyCart')}>
            <View style={styles.rightitemsList}>
              {/* <Icon name="shopping-cart" color={whiteColor} size={25}></Icon> */}
              <Image source={cartImage} style={styles.iconImage} />
              {/* <View style={styles.notificationContainer}>
                <Text numberOfLines={1} style={styles.notificationText}>
                  {count ? count : cartCount}
                </Text>
              </View> */}
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigationHeader()}>
            <View style={styles.userBtn}>
              {img ? (
                <Image
                  source={{
                    uri: `${config.url}/public/images/user/${img}`,
                  }}
                  style={styles.userIconImageProfile}
                />
              ) : (
                <Image source={profile_white_bg} style={styles.userIconImage} />
              )}
              {/* <Icon name="user" color={blackColor} size={20}></Icon> */}
            </View>
          </TouchableOpacity>
          {home ? (
            <TouchableOpacity onPress={() => navigate('drawer', '')}>
              <View style={styles.drawerbtn}>
                {/* <Icon name="bars" color={blackColor} size={22}></Icon> */}

                <Image source={nav_icon} style={styles.userIconImage} />
              </View>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity onPress={() => navigate('Home')}>
              <View style={styles.drawerbtn}>
                <Icon name="home" color={whiteColor} size={26}></Icon>
                {/* <Image source={nav_icon} style={styles.userIconImage} /> */}
              </View>
            </TouchableOpacity>
          )}
        </View>
      </View>
      <HeaderInput navigate={navigate} />
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    padding: '10@s',
    paddingLeft: '5@s',
    // paddingRight: Platform.OS = "ios" ? "10@s" : "5@s",
    // paddingRight: "20@s",

    backgroundColor: headerColor,
    position: 'relative',
    zIndex: 3,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: '30@s',
    // backgroundColor: 'red'
  },

  headerContainerUrdu: {
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: '20@s',
  },

  headingText: {
    color: whiteColor,
    fontSize: '20@s',
  },
  rightItems: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: '200@s',
  },
  rightItemsUrdu: {
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 0,
    paddingRight: '20@s',
  },
  rightitemsList: {
    color: whiteColor,
    paddingLeft: '8@s',
    paddingRight: '8@s',
    fontSize: '16@s',
    letterSpacing: '2@s',
    position: 'relative',
  },

  userBtn: {
    // backgroundColor: whiteColor,
    padding: '5@s',
    height: '22@s',
    width: '22@s',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    marginLeft: '10@s',
  },

  drawerbtn: {
    // backgroundColor: whiteColor,
    paddingRight: '4@s',
    paddingLeft: '4@s',
    paddingTop: '2@s',
    paddingBottom: '2@s',
    marginLeft: '1@s',
  },

  notificationContainer: {
    position: 'absolute',
    backgroundColor: greenColor,
    top: '-3@s',
    right: '-2@s',
    // width: '17@s',
    // height: '17@s',
    borderRadius: '100@s',
    // width: '100%',
    minWidth: '20@s',
    paddingTop: '2@s',
    paddingBottom: '2@s',
    paddingLeft: '4@s',
    paddingRight: '4@s',
    // height: '20@s',
    // width: '20@s',
    justifyContent: 'center',
    alignItems: 'center',
  },

  notificationText: {
    color: whiteColor,
    fontSize: '10@s',
    alignSelf: 'center',
  },
  headingLogo: {
    height: '15@s',
    width: '87@s',
  },

  iconImage: {
    height: '18@s',
    width: '20@s',
  },

  userIconImage: {
    height: '20@s',
    width: '20@s',
  },
  userIconImageProfile: {
    height: '20@s',
    width: '20@s',
    borderRadius: 50,
  },
});

export default MainHeader;
