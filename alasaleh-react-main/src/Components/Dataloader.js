import React from 'react';
import {View, ActivityIndicator} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {greenColor, whiteColor} from '../Theme/colors';

const DataLoader = ({animating}) => {
  return (
    <View style={styles.container}>
      <View style={styles.body}>
        <View style={styles.loderContainer}>
          <ActivityIndicator
            animating={animating}
            color={greenColor}
            size="large"
          />
        </View>
        <View style={styles.rightContainer}></View>
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
    padding: '10@s',
  },
  loderContainer: {
    width: '30%',
  },

  body: {
    backgroundColor: whiteColor,
    paddingTop: '20@s',
    paddingBottom: '20@s',
    padding: '10@s',
    flexDirection: 'row',
    alignItems: 'center',
  },

  rightContainer: {
    width: '70%',
  },
});

export default DataLoader;
