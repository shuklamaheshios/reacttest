import React, {useState, useContext} from 'react';
import {Image, Text, View, TouchableOpacity, ToastAndroid} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {CartDeleteModal} from '../Screens';

import {blackColor, greenColor, whiteColor} from '../Theme/colors';
import AppContext from '../Utils/AppContext';
import config from '../Utils/config';


function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const MyCartCard = ({
  title,
  price,
  item,
  updateCart,
  add,
  sub,
  img,
  cartToken,
  navigate,
}) => {
  const [modalVisible, setModalVisible] = useState(false);

  const forceUpdate = useForceUpdate();
  const myContext = useContext(AppContext);
  const addqty = () => {
    // let qnty =  parseInt(item.Quantity)
    add(item, false);
  };

  let pri = 0;
  let qti = 0;
  if (item.dq) {
    if (item.enable_free_product_option === 1) {
      if (item.no_of_products_buy < item.dq) {
        qti = item.dq - item.no_of_free_products;
        pri = item?.price * qti;
        pri = pri.toFixed(2)
      } else {
        pri = item?.price * item.dq;
        pri = pri.toFixed(2)

      }
    } else {
      pri = item?.price * item.dq;
      pri = pri.toFixed(2)

    }
  } else {
    if (item.enable_free_product_option === 1) {
      if (item.no_of_products_buy < item.Quantity) {
        qti = item.Quantity - item.no_of_free_products;
        pri = item?.price * qti;
        pri = pri.toFixed(2)

      } else {
        pri = item?.price * item.Quantity;
        pri = pri.toFixed(2)

      }
    } else {
      pri = item?.price * item.Quantity;
      pri = pri.toFixed(2)

    }
  }

  // if (item.enable_free_product_option === 1) {
  //   if (item.no_of_products_buy < item.Quantity) {
  //     const qti = item.Quantity - item.no_of_free_products;
  //     pri = item?.price * qti;
  //   } else {
  //     pri = item?.price * item?.Quantity;
  //   }

  //   // if (item.no_of_products_buy === item.Quantity) {
  //   //   item.Quantity = parseInt(item.Quantity) + item.no_of_free_products;
  //   // } else {
  //   //   item.Quantity = parseInt(item.Quantity) + 1;
  //   // }
  // }
  const subqty = () => {
    // let qnty =  parseInt(item.Quantity)
    add(item, true);
  };

  // let pr = 0;
  // if (item.promotion === '1' || item.promotion === 1) {
  //   pr = parseFloat(item.promotion_price).toFixed(2);
  // } else {
  //   const k = (parseFloat(item.price) * parseFloat(item.discount)) / 100;
  //   const calculated = item.price - k;
  //   let p = calculated.toFixed(2);
  //   if (p > calculated) {
  //     p = p - 0.01;
  //   }
  //   pr = p;
  // }

  const sendValue = () => {
    setModalVisible(true);
  };

  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => setModalVisible(true)}>
      <CartDeleteModal
        modalVisible={modalVisible}
        setModalVisible={setModalVisible}
        img={img}
        item={item}
        cartToken={cartToken}
        navigate={navigate}
      />

      <TouchableOpacity
        onPress={() => setModalVisible(true)}
        style={myContext.setting2name ? styles.bodyUrdu : styles.body}>
        <Image
          style={styles.image}
          source={{
            uri: `${config.url}/public/images/product/${img}`,
          }}
        />

        <View style={styles.card}>
          <TouchableOpacity
            onPress={() => setModalVisible(true)}
            style={styles.cardtextBody}>
            <View>
              <Text
                numberOfLines={2}
                style={
                  myContext.setting2name
                    ? styles.cardHeading1
                    : styles.cardHeading
                }>
                {myContext.setting2name ? item?.name_arabic : item?.Name}
              </Text>
              {item?.no_of_products_buy > 0 && item?.no_of_free_products && (
                <Text
                  style={{
                    color: greenColor,
                    marginTop: 8,
                    textAlign: myContext.setting2name ? 'right' : 'left',
                  }}>
                  {myContext.setting2name
                    ? `يشترى ${item?.no_of_products_buy} احصل على ${item?.no_of_free_products} مجانا`
                    : `Buy ${item?.no_of_products_buy} Get ${item?.no_of_free_products} Free`}
                </Text>
              )}
              <Text
                style={
                  myContext.setting2name ? styles.offtextUrdu : styles.offtext
                }>
                {myContext.setting2name ? `السعر $${pri}` : `Price $${pri}`}
              </Text>
            </View>

            <View
              style={
                myContext.setting2name
                  ? {
                      justifyContent: 'flex-start',
                      width: '100%',
                      flexDirection: 'row',
                      alignItems: 'center',
                      marginLeft: 20,
                    }
                  : {
                      justifyContent: 'flex-end',
                      width: '100%',
                      flexDirection: 'row',
                      alignItems: 'center',
                    }
              }>
              <TouchableOpacity style={styles.sumText} onPress={() => subqty()}>
                <Icon name="minus" color={whiteColor} size={12} />
              </TouchableOpacity>
              <Text style={styles.quantity}>{item?.Quantity}</Text>
              <TouchableOpacity style={styles.sumText} onPress={() => addqty()}>
                <Icon name="plus" color={whiteColor} size={12} />
              </TouchableOpacity>
            </View>
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    </TouchableOpacity>
  );
};

const styles = ScaledSheet.create({
  container: {
    paddingRight: '10@s',
    paddingLeft: '10@s',
    marginBottom: '10@s',
  },

  image: {
    height: '90@s',
    width: '80@s',
    resizeMode: 'center',
  },

  body: {
    flexDirection: 'row',
    paddingTop: '10@s',
    paddingBottom: '15@s',
    borderBottomColor: blackColor,
    borderBottomWidth: '0.5@s',
  },

  bodyUrdu: {
    flexDirection: 'row-reverse',
    paddingTop: '10@s',
    paddingBottom: '15@s',
    borderBottomColor: blackColor,
    borderBottomWidth: '0.5@s',
  },

  card: {
    flexDirection: 'row',
  },
  offContainerArabic: {
    backgroundColor: greenColor,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: greenColor,
  },
  offContainer: {
    backgroundColor: greenColor,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: greenColor,
  },
  cardtextBody: {
    display: 'flex',
    justifyContent: 'space-between',
    marginLeft: '20@s',
    width: '80%',
  },

  heartImage: {
    height: '20@s',
    width: '20@s',
  },
  cardHeading: {
    fontSize: '14@s',
    width: '100%',
    color: blackColor,
  },
  cardHeading1: {
    fontSize: '14@s',
    width: '100%',
    color: blackColor,
    // marginLeft: 27,
    textAlign: 'right',
  },
  heartImageContainer: {
    padding: '10@s',
    justifyContent: 'center',
  },
  offtext: {
    color: blackColor,
    fontWeight: 'bold',
    fontSize: '11@s',
    marginTop: '5@s',
  },

  offtextUrdu: {
    color: blackColor,
    fontWeight: 'bold',
    fontSize: '11@s',
    marginTop: '5@s',
    alignSelf: 'flex-end',
  },

  sumText: {
    width: '20@s',
    height: '20@s',
    borderRadius: 50,
    backgroundColor: blackColor,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sumWhitetext: {
    color: whiteColor,
    fontSize: '14@s',
  },

  quantity: {
    marginLeft: '15@s',
    marginRight: '15@s',
    color: blackColor,
    fontSize: '14@s',
  },
});

export default MyCartCard;
