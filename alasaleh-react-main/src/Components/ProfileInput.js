import React from 'react';
import {Text, TextInput, View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {blackColor, greenColor, redColor, whiteColor} from '../Theme/colors';
import Icon from 'react-native-vector-icons/FontAwesome5';

const ProfileInput = ({
  placeholder,
  label,
  type,
  onChangeText,
  name,
  icon,
  edit,
  defaultValue,
  value,
  keyboardType
}) => {
  return (
    <View>
      <Text style={styles.label}>{label}</Text>
      <View
        style={
          icon ? styles.profileInputContainerIcon : styles.profileInputContainer
        }>
        <TextInput
         value={value}
         keyboardType={keyboardType}
          defaultValue={defaultValue}
          placeholder={placeholder}
          style={icon ? styles.iconIput : styles.profileInput}
          type={type}
          editable={edit}
          onChangeText={(value) => onChangeText(name, value)}
        />

        <View
          style={
            icon
              ? {
                  backgroundColor: whiteColor,
                  padding: 10,
                  height: 40,
                  width: 40,
                  borderRadius: 100,
                  alignSelf: 'center',
                  marginBottom: 10,
                  marginRight: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                  flexDirection: 'row',
                  shadowColor: '#000',
                  shadowOffset: {
                    width: 0,
                    height: 3,
                  },
                  shadowOpacity: 0.27,
                  shadowRadius: 4.65,

                  elevation: 6,
                }
              : null
          }>
          <Icon
            style={{alignSelf: 'center'}}
            name={icon}
            size={20}
            color={greenColor}
          />
        </View>
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  profileInputContainer: {
    padding: '3@s',
    marginBottom: '5@s',
    flexDirection: 'row',
  },

  profileInputContainerIcon: {
    padding: '3@s',
    marginBottom: '5@s',
    flexDirection: 'row',
    borderColor: blackColor,
    borderWidth: 1,
    backgroundColor: whiteColor,
  },

  profileInput: {
    borderColor: blackColor,
    borderWidth: 1,
    padding: '2@s',
    paddingLeft: '10@s',
    width: '100%',
    backgroundColor: whiteColor,
  },
  label: {
    color: '#333333',
    fontSize: '10@s',
    marginLeft: '5@s',
    marginBottom: '5@s',
  },

  iconIput: {
    padding: '2@s',
    paddingLeft: '10@s',
    width: '85%',
    height: '70@s',
    textAlignVertical: 'top',
    backgroundColor: whiteColor,
  },
});

export default ProfileInput;
