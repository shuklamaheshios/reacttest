import React, {useState, useContext, useEffect} from 'react';
import {Image, Text, View, TouchableOpacity} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/FontAwesome5';
import maxum from '../Assets/images/maxum.png';

import red_heart from '../Assets/images/heart_red.png';
import AppContext from '../Utils/AppContext';
import {
  blackColor,
  blueColor,
  greenColor,
  lightGray,
  redColor,
  whiteColor,
} from '../Theme/colors';
import config from '../Utils/config';


function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const WhishListCard = ({
  title,
  price,
  removeWish,
  id,
  img,
  qty,
  addtocart,
  product,
}) => {
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };
  const [discountString, setDiscountString] = useState(null);
  const [prices, setPrice] = useState(0);
  useEffect(() => {
    if (product?.promotion === '1' || product?.promotion === 1) {
      // let chType = parseFloat(product?.promotion_price);
      // let fix = chType.toFixed(2);
      // setPrice(fix);
      setPrice(parseFloat(product.promotion_price).toFixed(2));
    } else {
      const k =
        (parseFloat(product?.price) * parseFloat(product?.discount)) / 100;
      const calculated = product?.price - k;
      let p = calculated.toFixed(2);
      if (p > calculated) {
        p = p - 0.01;
      }
      setPrice(p);
    }
    if (
      product?.enable_free_product_option === 1 ||
      product?.enable_free_product_option === '1'
    ) {
      myContext.setting2name
        ? setDiscountString(
            `يشترى ${product.no_of_products_buy} احصل على ${product.no_of_free_products} مجانا`,
          )
        : setDiscountString(
            `Buy ${product?.no_of_products_buy} Get ${product.no_of_free_products} Free`,
          );
    }
  }, []);
  let status = false;
  if (qty >= 5) {
    status = true;
  }
  const productImage = product?.image.split(',');

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.body}>
        <View style={styles.imageContainer}>
          <Image
            style={styles.image}
            source={{
              uri: `${config.url}/public/images/product/${productImage[0]}`,
            }}
          />
          {status ? null : (
            <View
              style={{
                backgroundColor: 'rgba(255,255,255,0.5)',
                width: '100%',
                height: '100%',
                position: 'absolute',
                justifyContent: 'center',
                alignItems: 'center',
                padding: 5,
              }}>
              <TouchableOpacity>
                <View style={styles.cartBtnComing}>
                  <Text style={styles.imageAddCart}>
                    {myContext.setting2name ? 'قريبا' : 'Comming Soon'}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          )}
        </View>
        <View style={styles.card}>
          <View style={styles.cardtextBody}>
            <View>
              <Text numberOfLines={2} style={styles.cardHeading}>
                {title}
              </Text>
            </View>
          </View>

          <View style={styles.wishlistFooter}>
            <View>
              <Text style={styles.offtext}>{product?.discount} % OFF</Text>
              <Text
                numberOfLines={1}
                style={styles.dollarText}>{`$ ${prices}`}</Text>
              <Text style={styles.originalPrice}>${product?.price}</Text>
            </View>

            <View style={styles.heartImageContainer}>
              <TouchableOpacity onPress={() => removeWish(id)}>
                <Image source={red_heart} style={styles.heartImage} />
              </TouchableOpacity>

              {status ? (
                <TouchableOpacity onPress={() => addtocart(product)}>
                  <View style={styles.cartBtn}>
                    <Icon
                      name="shopping-cart"
                      color={whiteColor}
                      size={17}
                      style={{marginRight: 10}}
                    />
                    <Text style={styles.addToCart}>
                      {myContext.setting2name ? 'أضف إلى السلة' : 'Add to Cart'}
                    </Text>
                  </View>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity>
                  <View style={styles.cartBtnComing}>
                    <Text style={styles.addToCart}>
                      {myContext.setting2name ? 'قريبا' : 'Comming Soon'}
                    </Text>
                  </View>
                </TouchableOpacity>
              )}
            </View>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    paddingLeft: '10@s',
    marginBottom: '10@s',
  },

  imageContainer: {
    backgroundColor: whiteColor,
    height: '90@s',
    width: '25%',
    resizeMode: 'center',
  },

  image: {
    backgroundColor: whiteColor,
    height: '90@s',
    width: '100%',
    resizeMode: 'center',
  },

  body: {
    flexDirection: 'row',
    paddingTop: '10@s',
    paddingBottom: '15@s',
    borderBottomColor: blackColor,
    borderBottomWidth: '0.5@s',
  },

  card: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    width: '75%',
    paddingLeft: '15@s',
  },

  cardtextBody: {
    display: 'flex',
    justifyContent: 'space-between',
  },

  heartImage: {
    height: '20@s',
    width: '20@s',
    marginBottom: '5@s',
  },
  cardHeading: {
    fontSize: '14@s',
    width: '150@s',
    color: blackColor,
  },

  heartImageContainer: {
    padding: '10@s',
    justifyContent: 'center',
    alignItems: 'flex-end',
    marginRight: '5@s',
  },
  originalPrice: {
    fontSize: '8@s',
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
  },
  offtext: {
    color: greenColor,
    fontWeight: 'bold',
    fontSize: '11@s',
  },

  dollarText: {
    color: blackColor,
    fontWeight: 'bold',
    fontSize: '11@s',
    width: '100@s',
  },

  crosstext: {
    color: '#333333',
    fontSize: '10@s',
  },

  wishlistFooter: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },

  cartBtn: {
    backgroundColor: greenColor,
    padding: '5@s',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: '2@s',
  },

  addToCart: {
    color: whiteColor,
    fontSize: '11@s',
  },

  cartBtnComing: {
    backgroundColor: redColor,
    paddingTop: '2@s',
    paddingBottom: '2@s',
    paddingLeft: '5@s',
    paddingRight: '5@s',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: '5@s',
  },

  imageAddCart: {
    color: whiteColor,
    fontSize: '9@s',
  },
});

export default WhishListCard;
