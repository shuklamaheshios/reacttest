import React, {useState, useContext} from 'react';
import {View, TextInput, Text} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {blackColor} from '../Theme/colors';
import AppContext from '../Utils/AppContext';

function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}

const Input = ({label, placeHolder, type, onChangeText, name}) => {
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const handleClick = () => {
    myContext.toggleSetting2();
    forceUpdate();
  };
  return (
    <View style={styles.container}>
      <Text style={styles.inputLabel}>{label}</Text>
      <TextInput
        type={type}
        style={styles.input}
        placeholder={placeHolder}
        onChangeText={(value)=>onChangeText(name,value)}
      />
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    paddingTop: '10@s',
    paddingLeft: '13@s',
    paddingRight: '13@s',
  },

  input: {
    borderColor: blackColor,
    borderWidth: 1,
    paddingLeft: '10@s',
    paddingRight: '10@s',
    paddingTop: '5@s',
    paddingBottom: '5@s',
    color: blackColor,
  },
  inputLabel: {
    marginBottom: '5@s',
    color: blackColor,
    fontSize: '14@s',
  },

  customeInput: {
    borderColor: blackColor,
    borderWidth: 1,
    paddingLeft: '10@s',
    paddingRight: '10@s',
    paddingTop: '5@s',
    paddingBottom: '5@s',
    color: blackColor,
    height: '100@s',
  },
});

export default Input;
