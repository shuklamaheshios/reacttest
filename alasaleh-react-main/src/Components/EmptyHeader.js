import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/FontAwesome';
import { blackColor, redColor } from '../Theme/colors';

const EmptyHeader = ({ title, goBack }) => {
  return (
    <View style={styles.container}>
      <View style={{
        width: '90%', alignSelf: 'center', flexDirection: 'row',
        alignItems: 'center', justifyContent: 'space-between'
      }}>
        <View>
          <TouchableOpacity onPress={goBack} style={styles.backArrow}>
            <Icon name="arrow-left" size={22} />
          </TouchableOpacity>
        </View>
        <View style={{ alignSelf: 'center' }}>
          <Text style={styles.headerText}>{title}</Text>
        </View>
        <View />
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    paddingTop: '5@s',
    paddingBottom: '5@s',
    backgroundColor: 'transparent',
    paddingLeft: '10@s',
    paddingRight: '10@s',
  },

  backArrow: {
    paddingTop: '5@s',
    paddingBottom: '5@s',
    // paddingLeft: '5@s',
    paddingRight: '20@s',
    alignItems: 'center',
  },

  headerText: {
    color: blackColor,
    fontSize: '18@s',
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default EmptyHeader;
