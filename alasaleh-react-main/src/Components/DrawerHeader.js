import React, { useContext, useState } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/FontAwesome';
import { greenColor, whiteColor } from '../Theme/colors';
import AppContext from '../Utils/AppContext';

const DrawerHeader = ({ goBack, title }) => {
  const myContext = useContext(AppContext);
  return (
    <View style={styles.container}>
      <View
        style={{
          flexDirection: `${myContext.setting2name ? 'row-reverse' : 'row'}`,
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <View>
          <TouchableOpacity onPress={goBack} style={styles.backArrow}>
            <Icon
              name={myContext.setting2name ? 'arrow-right' : 'arrow-left'}
              size={22}
              color={whiteColor}
            />
          </TouchableOpacity>
        </View>
        <View>
          <Text style={styles.headerText}>{title}</Text>
        </View>
        <View></View>
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    paddingTop: '10@s',
    paddingBottom: '10@s',
    backgroundColor: greenColor,
    paddingLeft: '10@s',
    paddingRight: '10@s',
  },

  backArrow: {
    paddingTop: '5@s',
    paddingBottom: '5@s',
    alignItems: 'center',
  },

  headerText: {
    color: whiteColor,
    fontSize: '16@s',
    textAlign: 'center',
  },
});

export default DrawerHeader;
