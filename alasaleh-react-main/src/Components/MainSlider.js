import React, {useContext} from 'react';
import {Text, View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {SliderBox} from 'react-native-image-slider-box';
import {greenColor, lightGray} from '../Theme/colors';
import AppContext from '../Utils/AppContext';
import config from '../Utils/config';

const MainSlider = ({homePageProducts}) => {
  const images = [];
  const myContext = useContext(AppContext);
  if (myContext.setting2name) {
    homePageProducts?.slider_ar?.map((item) => {
      {
      }
      //  var img = require(" "+item.img_path)
      images.push(`${config.url}/public/${item.img_path}`);
    });
  } else {
    homePageProducts?.slider_en?.map((item) => {
      {
      }
      //  var img = require(" "+item.img_path)
      images.push(`${config.url}/public/${item.img_path}`);
    });
  }
  return (
    <View style={styles.container}>
      <View style={styles.boxContainer}>
        <SliderBox
          sliderBoxHeight="100%"
          // resizeMethod={'auto'}
          autoplay={true}
          resizeMode="center"
          imageLoadingColor={greenColor}
          circleLoop={true}
          images={images}
          dotColor={greenColor}
          inactiveDotColor={lightGray}
          style={{
            height: 150,
            borderColor: 'black',
            borderWidth: 1,
            marginRight: 5,
          }}
        />
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    padding: '3@s',
  },
  boxContainer: {
    // backgroundColor: '#f8f8ff',
    height: '150@s',
    width: '100%',
    // borderWidth: 1,
    // borderColor: 'gray',
    // justifyContent: 'center',
    // alignItems: 'center',
    // padding: '10@s',
  },
});

export default MainSlider;
