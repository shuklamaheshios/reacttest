import React, {useState, useContext, useEffect} from 'react';
import {
  Text,
  View,
  Dimensions,
  Image,
  TouchableOpacity,
  ToastAndroid,
  Button,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import Toast from 'react-native-simple-toast';
import heartImage from '../Assets/images/heart_light_grey.png';
import Icon from 'react-native-vector-icons/FontAwesome';
import maxum from '../Assets/images/maxum.png';
import {
  blackColor,
  greenColor,
  lightGray,
  redColor,
  whiteColor,
} from '../Theme/colors';
import AppContext from '../Utils/AppContext';
import Storage from '../Utils/storage';
import config from '../Utils/config';
import axios from 'axios';
import {image} from 'faker';

function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}
const cardwidth = Dimensions.get('window').width;

const OutOfStockCard = ({
  navigate,
  stock,
  products,
  addtocart,
  list,
  addinWishL,
  removeWishL,
  update,
}) => {
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();

  const [token, settoken] = useState('');
  const [wishlist, setWishlist] = useState([]);
  const [loader, setloader] = useState(true);
  const [price, setPrice] = useState(0);
  const [discountString, setDiscountString] = useState(null);
  const navigationHeader = async (id) => {
    let isLogin = null;
    await Storage.retrieveData('user').then((resp) => {
      isLogin = resp;
    });

    if (isLogin) {
      handleColor(id);
      // Toast.show('Product added to wishlist');
    } else {
      navigate('Login');
    }
  };
  // console.log('promotion', products);
  useEffect(() => {
    if (products.promotion === '1' || products.promotion === 1) {
      setPrice(parseFloat(products.promotion_price).toFixed(2));
    } else {
      const k =
        (parseFloat(products.price) * parseFloat(products.discount)) / 100;
      const calculated = products.price - k;
      let p = calculated.toFixed(2);
      if (p > calculated) {
        p = p - 0.01;
      }
      setPrice(p);
    }

    // if (
    //   products.enable_free_product_option === '1' ||
    //   products.enable_free_product_option === 1
    // ) {
    //   if (myContext.setting2name) {
    //     setDiscountString(
    //       `يشترى ${products?.no_of_products_buy} احصل على ${products?.no_of_free_products} مجانا`,
    //     );
    //   } else {
    //     setDiscountString(
    //       `Buy ${products.no_of_products_buy} Get ${products.no_of_free_products} Free`,
    //     );
    //   }
    // }

    Storage.retrieveData('user').then((resp) => {
      getwish(resp.token);
      settoken(resp.token);
    });
  }, []);
  useEffect(() => {
    getColorForProduct();
    setloader(false);
  }, [wishlist]);
  const getColorForProduct = () => {
    wishlist?.map((item, index) => {
      list.map((product) => {
        if (product.heart !== 'red') {
          if (product.id === item.id) {
            product.heart = 'red';
          } else {
            product.heart = lightGray;
          }
        }
      });
    });
    forceUpdate();
  };
  const addinWish = (id) => {
    const params = {
      product_id: id,
    };
    axios
      .post(`${config.apiUrl}/whishlist`, params, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        Toast.show(res.message, Toast.LONG);
      })
      .catch((error) => {
        console.error(error);
      });
  };
  const removeWish = (id) => {
    const params = {
      product_id: id,
    };
    axios
      .post(`${config.apiUrl}/whishlist/delete`, params, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        Toast.show(res.message, Toast.SHORT);
      })
      .catch((error) => {
        console.error(error);
      });
  };
  const getwish = (t) => {
    fetch(`${config.apiUrl}/wishlist/show`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${t}`,
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        setWishlist(responseJson.data);
        getColorForProduct();
      });
  };
  const handleColor = async (id) => {
    list.map((data, index) => {
      if (data.id === id) {
        if (data.heart === 'red') {
          data.heart = lightGray;
          removeWishL(data.id);
        } else {
          data.heart = 'red';
          addinWishL(data.id);
        }
      }
    });

    forceUpdate();
  };
  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() =>
          navigate('Cart', {
            item: products,
            handleColor: handleColor,
            addtocart: addtocart,
            update: update,
          })
        }
        style={styles.blurBack}>
        <TouchableOpacity
          onPress={() =>
            navigate('Cart', {
              item: products,
              handleColor: handleColor,
              addtocart: addtocart,
              update: update,
            })
          }
          style={styles.centerBtn}>
          <View style={styles.cartBtn}>
            <Text style={styles.addToCart}>
              {myContext.setting2name ? 'إنتهى من المخزن' : 'Out of Stock'}
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={
            myContext.setting2name
              ? styles.heartContainerArabic
              : styles.heartContainer
          }
          onPress={() => {
            navigationHeader(products.id);
          }}>
          <Icon
            name="heart"
            size={26}
            color={products.heart ? products.heart : lightGray}
          />
        </TouchableOpacity>
      </TouchableOpacity>
      <View style={{padding: 5}}>
        <TouchableOpacity
          onPress={() =>
            navigate('Cart', {
              item: products,
              handleColor: handleColor,
              addtocart: addtocart,
              update: update,
            })
          }>
          <View
            style={
              myContext.setting2name
                ? styles.offContainerArabic
                : styles.offContainer
            }>
            {/* {discountString ? (
            <Text style={styles.offText}>{discountString}</Text>
          ) : (
            <Text style={styles.offText}>{products.discount} % OFF</Text>
          )} */}
            {products.enable_free_product_option === '1' ||
            products.enable_free_product_option === 1 ? (
              myContext.setting2name ? (
                <Text
                  style={
                    styles.getFreeText
                  }>{`يشترى ${products.no_of_products_buy} احصل على ${products.no_of_free_products} مجانا`}</Text>
              ) : (
                <Text
                  style={
                    styles.getFreeText
                  }>{`Buy ${products.no_of_products_buy} Get ${products.no_of_free_products} Free`}</Text>
              )
            ) : myContext.setting2name ? (
              <Text style={styles.getFreeText}>
                {products.discount} % إيقاف
              </Text>
            ) : (
              <Text style={styles.getFreeText}>{products.discount} % OFF</Text>
            )}
          </View>
          <View
            style={
              myContext.setting2name
                ? styles.heartContainerArabic
                : styles.heartContainer
            }></View>
          <View style={{justifyContent: 'space-between'}}>
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: 50,
              }}>
              <Image
                style={styles.image}
                source={{
                  uri: `${config.url}/public/images/product/${products.image}`,
                }}
                resizeMode="center"
              />
            </View>

            <View>
              <Text numberOfLines={2} style={styles.description}>
                {myContext.setting2name ? products.name_arabic : products.name}
              </Text>
              <View style={styles.priceContainer}>
                <View>
                  <Text style={styles.discountPrice}>${price}</Text>
                  <Text style={styles.originalPrice}>${products.price}</Text>
                </View>
                <TouchableOpacity onPress={() => addtocart(products)}>
                  <View style={styles.cartBtn}>
                    <Text style={styles.addToCart}>
                      {myContext.setting2name
                        ? 'أضف إلى السلة'
                        : 'Comming soon'}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    width: cardwidth / 2.6,
    // padding: '5@s',
    backgroundColor: whiteColor,
    borderRadius: '7@s',
    shadowColor: blackColor,
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 5,
    marginRight: '7@s',
    marginLeft: '2@s',
    position: 'relative',
    marginBottom: '10@s',
    marginTop: '2@s',
  },

  offContainer: {
    backgroundColor: lightGray,
    position: 'absolute',
    top: '0@s',
    left: '2@s',
    paddingTop: '2@s',
    paddingBottom: '2@s',
    paddingLeft: '5@s',
    paddingRight: '5@s',
    borderRadius: 50,
  },
  offContainerArabic: {
    backgroundColor: lightGray,
    position: 'absolute',
    top: '0@s',
    right: '2@s',
    paddingTop: '2@s',
    paddingBottom: '2@s',
    paddingLeft: '5@s',
    paddingRight: '5@s',
    borderRadius: 50,
  },

  offText: {
    fontSize: '10@s',
  },
  heartContainer: {
    position: 'absolute',
    top: '15@s',
    right: '0@s',
  },

  heartContainerArabic: {
    position: 'absolute',
    top: '20@s',
    left: '5@s',
    zIndex: 2,
  },

  image: {
    height: '100@s',
    width: '100%',
  },
  description: {
    fontSize: '12@s',
    fontWeight: '600',
  },
  priceContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: '5@s',
    alignItems: 'center',
  },
  discountPrice: {
    color: blackColor,
    fontWeight: 'bold',
    fontSize: '12@s',
  },

  originalPrice: {
    fontSize: '10@s',
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
  },
  cartBtn: {
    backgroundColor: redColor,
    padding: '3@s',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: '100@s',
  },
  addToCart: {
    color: whiteColor,
    fontSize: '10@s',
  },

  iconImage: {
    height: '16@s',
    width: '16@s',
  },

  getFreeText: {
    fontSize: '8.5@s',
  },

  blurBack: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    backgroundColor: 'rgba(255,255,255,0.5)',
    zIndex: 2,
    borderRadius: '10@s',
    justifyContent: 'center',
    alignItems: 'center',
  },
  centerBtn: {
    position: 'absolute',
    top: '50%',
  },

  iconImage: {
    height: '18@s',
    width: '18@s',
  },

  heartContainer: {
    position: 'absolute',
    top: '22@s',
    right: '15@s',
    zIndex: 10,
  },

  heartContainerArabic: {
    position: 'absolute',
    top: '22@s',
    left: '15@s',
    zIndex: 10,
  },
});
export default OutOfStockCard;
