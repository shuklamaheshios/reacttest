import React, {useState, useContext} from 'react';
import {View, Text, TouchableOpacity, FlatList} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import Toast from 'react-native-simple-toast';

import {Card} from '../Components';
import {blackColor} from '../Theme/colors';
import OutOfStockCard from './OutOfStockCard';

import AppContext from '../Utils/AppContext';
import Storage from '../Utils/storage';
import config from '../Utils/config';

function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue((value) => ++value); // update the state to force render
}
const CardsContaiener = ({
  title,
  navigate,
  flashOffersList,
  update,
  addLocalCart,
  addinWishL,
  removeWishL,
  count,
}) => {
  const myContext = useContext(AppContext);
  const forceUpdate = useForceUpdate();
  const [price, setPrice] = useState(0);
  let token = null;
  let user = null;

  const addtocart = async (item) => {
    let pr = 0;
    // console.log('item', item);
    await Storage.retrieveData('cartData').then((resp) => {
      token = resp;
    });
    await Storage.retrieveData('user').then((resp) => {
      console.log('user', resp);
      user = resp;
    });

    if (token === null) {
      await fetch(`${config.apiUrl}/carts`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      })
        .then((response) => response.json())
        .then((responseJson) => {
          Storage.storeData('cartData', responseJson);
          token = responseJson;
        });
    }
    if (item.promotion === '1' || item.promotion === 1) {
      pr = parseFloat(item.promotion_price).toFixed(2);
    } else {
      const k = (parseFloat(item.price) * parseFloat(item.discount)) / 100;
      const calculated = item.price - k;
      let p = calculated.toFixed(2);
      if (p > calculated) {
        p = p - 0.01;
      }
      pr = p;
    }
    // let cartDet = null;
    // await Storage.retrieveData('cartDataDetails').then((resp) => {
    //   console.log('new cart:', resp);
    //   cartDet = resp;
    // });
    // if (cartDet) {
    //   item = {
    //     ...item,
    //     price: pr,
    //   };

    //   cartDet.push(item);
    //   console.log('adding cart:', cartDet);
    //   Storage.storeData('cartDataDetails', cartDet);
    // } else {
    //   item = {
    //     ...item,
    //     price: pr,
    //   };
    //   Storage.storeData('cartDataDetails', [item]);
    // }

    let toCount = 0;
    
    await fetch(
      `${config.apiUrl}/carts/show_cart_data/${token.cartToken}`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          cartKey: token.cartKey,
        }),
      },
    )
      .then((response) => response.json())
      .then((responseJson) => {
        responseJson.item_in_cart.map((cartItem) => {
          if (cartItem.productID === item.id) {
            toCount = parseInt(cartItem.Quantity);
            console.log('toCount.id', cartItem?.Quantity);
          }
        });

        console.log('cart', responseJson);
        // responseJson['item_in_cart'].map(item => this.getProducts(item.Name))
      })
      .catch((e) => {
        console.log('error is', e);
        // this.setState({
        //   loader: false,
        // });
      });
    let qti = 1;
    if (toCount !== 0) {
      qti = toCount + 1;
      item.Quantity = qti;
    }
    if (item.enable_free_product_option === 1) {
      // item.Quantity = parseInt(item.Quantity) + 1;
      // if (!item.Qun) {
      //   console.log('item.Qun', item.Qun);
      //   item.Qun = 0;
      // }
      item.Qun = item.Qun + 1;
      item.dq = item.dq + 1;

      if (
        item.Quantity === item.no_of_products_buy ||
        item.Qun === item.no_of_products_buy
      ) {
        item.Qun = 0;

        item.Quantity = parseInt(item.Quantity) + item.no_of_free_products;
        qti = item.Quantity;
      }
      // else if (item.Quantity % item.no_of_products_buy !== 0) {
      //   item.Quantity = parseInt(item.Quantity) + 1;
      // }
    }
    await fetch(
      `${config.apiUrl}/carts/add_to_cart/${token.cartToken}`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          cartKey: token.cartKey,
          productID: item.id,
          quantity: qti.toString(),
          price: pr,
          image: item.image,
        }),
      },
    )
      .then((response) => response.json())
      .then((res) => {
        Toast.show(res.message);
        update();
        // forceUpdate();
      });
  };
  const renderItem = ({item}) =>
    item.qty >= 5 ? (
      <Card
        stock={true}
        navigate={navigate}
        title={item.title}
        products={item}
        list={flashOffersList}
        addtocart={addtocart}
        addinWishL={addinWishL}
        removeWishL={removeWishL}
        update={count}
      />
    ) : (
      <OutOfStockCard
        // stock={true}
        // navigate={navigate}
        // title={item.title}
        // navigate={navigate}
        // item={item}
        stock={true}
        navigate={navigate}
        title={item.title}
        products={item}
        list={flashOffersList}
        addtocart={addtocart}
        addinWishL={addinWishL}
        removeWishL={removeWishL}
        update={count}
      />
    );

  return (
    <View style={styles.container}>
      <View
        style={
          myContext.setting2name ? styles.cardHeaderUrdu : styles.cardHeader
        }>
        <Text style={styles.cardHeaderText}>{title}</Text>
        <TouchableOpacity
          onPress={() => navigate('ProductCard', flashOffersList)}>
          <Text style={styles.viewtext}>
            {myContext.setting2name ? 'مشاهدة الكل' : 'View All'}
          </Text>
        </TouchableOpacity>
      </View>
      <FlatList
        data={flashOffersList}
        inverted={myContext.setting2name ? true : false}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
        horizontal
        showsHorizontalScrollIndicator={false}
      />
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    padding: '5@s',
    marginTop: '10@s',
    marginBottom: '10@s',
  },
  cardHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    marginBottom: '10@s',
  },

  cardHeaderUrdu: {
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    marginBottom: '10@s',
  },

  cardHeaderText: {
    fontSize: '16@s',
    color: blackColor,
    fontWeight: 'bold',
  },

  viewtext: {
    color: blackColor,
    fontSize: '14@s',
    fontWeight: 'normal',
  },

  image: {
    height: '50@s',
    width: '100%',
  },
});

export default CardsContaiener;
