import React, { useState, useContext, useEffect } from 'react';
import { SafeAreaView, Text, View, TouchableOpacity, Image } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import RNRestart from 'react-native-restart';
import profile_green_bg from '../Assets/images/profile_green_bg.png';
import { blackColor, greenColor, lightGray } from '../Theme/colors';
import Storage from '../Utils/storage';
import config from '../Utils/config';
import AppContext from '../Utils/AppContext';
import axios from 'axios';
const DrawerContentData = ({ navigation }) => {
  const myContext = useContext(AppContext);
  const [user, setuser] = useState();
  const [log, setlog] = useState();
  const startReload = () => RNRestart.Restart();
  const [img, setimg] = useState(null);
  useEffect(() => {
    Storage.retrieveData('profile').then((resp) => {
      setuser(resp);
    });
    Storage.retrieveData('user').then((resp) => {
      setlog(resp);
      getProfile(resp);
    });
  }, []);

  const getProfile = async (user) => {
    let data = {
      id: user.user_id,
    };
    await axios
      .get(`${config.apiUrl}/user/${user.user_id}`, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + user.token,
          // Authorization: `bearer ${token}`,
        },
      })
      .then((res) => {
        console.log("The Image Date: ", res.data.data)
        setimg(res.data.data.image);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const logout = async () => {
    await Storage.removeData('user');
    await Storage.removeData('profile');
    startReload();
  };

  const navigationHeader = async () => {
    let isLogin = null;
    await Storage.retrieveData('user').then((resp) => {
      isLogin = resp;
    });

    if (isLogin) {
      navigation.navigate('MyAccount');
    } else {
      navigation.navigate('Login');
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <View
        style={[
          myContext.setting2name
            ? styles.userNameContainerUrdu
            : styles.userNameContainer,
        ]}>
        <View style={styles.userContainer}>
          <Text style={styles.userContainerHeading}>
            {myContext.setting2name ? 'مرحبا' : 'Hi'}
          </Text>
          <Text numberOfLines={1} style={styles.userNameText}>
            {user ? user : 'Username'}
          </Text>
        </View>
        {/* <Image
          style={styles.image}
          source={log.image ? log.image : profile_green_bg}
        /> */}
        {img ? (
          <Image
            source={{
              uri: `${config.url}/public/images/user/${img}`,
            }}
            style={styles.image}
          />
        ) : (
            <Image source={profile_green_bg} style={styles.image} />
          )}
      </View>
      <View style={{ padding: 10 }}>
        <TouchableOpacity
          onPress={() => navigation.navigate('Refferel')}
          style={styles.itemView}>
          <Text style={styles.itemText}>
            {myContext.setting2name ? 'دعوة صديق' : 'Refer A Friend'}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigationHeader()}
          style={styles.itemView}>
          <Text style={styles.itemText}>
            {myContext.setting2name ? 'حسابي' : 'My Account'}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate('About')}
          style={styles.itemView}>
          <Text style={styles.itemText}>
            {myContext.setting2name ? 'معلومات عنا' : 'About Us'}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate('Refund')}
          style={styles.itemView}>
          <Text style={styles.itemText}>
            {myContext.setting2name
              ? 'سياسة الاسترداد والإرجاع'
              : 'Refund And Return Policy'}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate('TermsAndCondition')}>
          <View style={styles.itemView}>
            <Text style={styles.itemText}>
              {myContext.setting2name
                ? 'الأحكام والشروط'
                : 'Terms and Conditions'}
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate('Privacy')}
          style={styles.itemView}>
          <Text style={styles.itemText}>
            {myContext.setting2name ? 'سياسة خاصة' : 'Privacy Policy'}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate('Career')}
          style={styles.itemView}>
          <Text style={styles.itemText}>
            {myContext.setting2name ? 'وظائف' : 'Careers'}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate('ShippingPolicy')}
          style={styles.itemView}>
          <Text style={styles.itemText}>
            {myContext.setting2name ? 'الشحن' : 'Shipping'}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate('Help')}
          style={styles.itemView}>
          <Text style={styles.itemText}>
            {myContext.setting2name ? 'مساعدة و دعم' : 'Help & Support'}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate('News')}
          style={styles.itemView}>
          <Text style={styles.itemText}>
            {myContext.setting2name ? 'النشرة الإخبارية' : 'Newletter'}
          </Text>
        </TouchableOpacity>
        {log ? (
          <TouchableOpacity onPress={() => logout()} style={styles.listItem}>
            <Text style={styles.itemText}>
              {myContext.setting2name ? 'تسجيل خروج' : 'Log Out'}
            </Text>
          </TouchableOpacity>
        ) : (
            <></>
          )}
      </View>
    </SafeAreaView>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },

  itemView: {
    marginTop: '10@s',
    marginBottom: '10@s',
  },

  itemText: {
    color: blackColor,
    fontSize: '14@s',
    fontWeight: 'bold',
  },

  userNameContainer: {
    backgroundColor: lightGray,
    padding: '10@s',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  userNameContainerUrdu: {
    backgroundColor: lightGray,
    padding: '10@s',
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
  },

  image: {
    width: '60@s',
    height: '60@s',
    borderRadius: 100,
  },

  userContainer: {
    justifyContent: 'space-between',
  },

  userContainerHeading: {
    color: greenColor,
    fontSize: '18@s',
    fontWeight: 'bold',
  },

  userNameText: {
    color: '#333333',
    fontSize: '13@s',
    width: '160@s',
  },
});

export default DrawerContentData;
