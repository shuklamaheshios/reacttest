import React from 'react';
import { Component } from 'react';
import { SafeAreaView, ActivityIndicator, View } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import { Refferel } from '../Screens';
import axios from 'axios';
import { CustomLoader } from '../Components';
import config from '../Utils/config';
class RefferelContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: false,
    };
  }
  navigate = async (routeName) => {
    const { navigation } = this.props;
    if (routeName === 'drawer') {
      await navigation.openDrawer();
    } else {
      await navigation.navigate(routeName);
    }
  };

  goBack = () => {
    const { navigation } = this.props;
    navigation.goBack();
  };
  handlechange = (name, value) => {
    this.setState({
      [name]: value,
    });
  };
  sendreferal = async (token) => {
    this.setState({
      loader: true,
    });
    const params = {
      receiver_email: this.state.email,
      subject: this.state.subject,
      reffer_link: 'alasaleh.com',
      note: this.state.note,
    };
    await axios
      .post(
        `${config.apiUrl}/send_refferal_email`,
        params,
      )
      .then((res) => {
        this.setState({
          loader: false,
        });
      })
      .catch((error) => {
        console.log('errororor');
        console.error(error);
      });
  };
  render() {
    return (
      <SafeAreaView style={styles.container}>
        {this.state.loader ? (
          // <View
          //   style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          //   <ActivityIndicator
          //     size="large"
          //     color="#00ff00"
          //     animating={this.state.loader}
          //   />
          // </View>
          <CustomLoader />
        ) : (
            <Refferel
              handlechange={this.handlechange}
              navigate={this.navigate}
              goBack={this.goBack}
              sendreferal={this.sendreferal}
            />
          )}
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
});

export default RefferelContainer;
