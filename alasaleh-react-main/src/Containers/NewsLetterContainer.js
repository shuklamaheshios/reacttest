import React, {Component} from 'react';
import {SafeAreaView} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {NewsLetterScreen} from '../Screens';
import {whiteColor} from '../Theme/colors';
import Toast from 'react-native-simple-toast';
import axios from 'axios';
import config from '../Utils/config';
class NewsLetterContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
    };
  }
  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };
  handleChange = (name, value) => {
    this.setState({
      [name]: value,
    });
  };
  submit = () => {
    const params = {
      email: this.state.email,
    };
    console.log(params);
    axios
      .post(`${config.apiUrl}/subscribe`, params)
      .then((res) => {
        console.log('help submit', res);
        Toast.show(res.data.message, Toast.LONG);
      })
      .catch((error) => {
        Toast.show('Unable To Subscribed', Toast.LONG);
        console.error(error);
      });
  };
  navigate = async (routeName, billingInfo) => {
    const {navigation} = this.props;
    if (routeName === 'drawer') {
      navigation.openDrawer();
    } else {
      await navigation.navigate(routeName, billingInfo);
    }
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <NewsLetterScreen
          goBack={this.goBack}
          handleChange={this.handleChange}
          submit={this.submit}
          navigate={this.navigate}
        />
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: whiteColor,
  },
});

export default NewsLetterContainer;
