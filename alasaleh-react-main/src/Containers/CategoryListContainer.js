import React, { Component } from 'react';
import { SafeAreaView, ActivityIndicator, View } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import { CustomLoader } from '../Components';
import { CategoryListScreen } from '../Screens';
import { whiteColor } from '../Theme/colors';
import config from '../Utils/config';
class CategoryListContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      productList: [],
      loader: true,
    };
  }
  navigate = async (routeName, stock) => {
    const { navigation } = this.props;
    if (routeName === 'drawer') {
      await navigation.openDrawer();
    } else {
      await navigation.navigate(routeName, stock);
    }
  };
  componentDidMount() {
    const { params } = this.props.route;
    this.getProductsBySubCategory(params);
  }
  getProductsBySubCategory = async (id) => {
    await fetch(`${config.apiUrl}/get_sub_categories/${id}`)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          productList: responseJson.data,
          loader: false,
        });
      })
      .catch((error) => {
        console.error(error);
      });
  };
  render() {
    return (
      <SafeAreaView style={styles.container}>
        {this.state.loader ? (
          <CustomLoader />
        ) : (
            <CategoryListScreen
              productList={this.state.productList ? this.state.productList : [{}]}
              navigate={this.navigate}
            />
          )}
      </SafeAreaView>
    );
  }
}
const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: whiteColor,
  },
});
export default CategoryListContainer;
