import React, {Component} from 'react';
import {SafeAreaView, ActivityIndicator, View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {CustomLoader} from '../Components';
import {LoginScreen} from '../Screens';
import {whiteColor} from '../Theme/colors';
//import SyncStorage from 'sync-storage';
import Storage from '../Utils/storage';
import config from '../Utils/config';
import RNRestart from 'react-native-restart';
import {GoogleSignin, statusCodes} from '@react-native-community/google-signin';
import Toast from 'react-native-simple-toast';
import {
  LoginManager,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';

class LoginContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      loader: false,
    };
  }
  startReload = () => RNRestart.Restart();
  componentDidMount() {
    GoogleSignin.configure({
      webClientId:
        '470831512232-6g8beorair4kih4bfp2c2dk6iei0du24.apps.googleusercontent.com',
      offlineAccess: true,
      hostedDomain: '',
      forceConsentPrompt: true,
    });
  }
  navigate = async (routeName) => {
    const {navigation} = this.props;
    if (routeName === 'drawer') {
      await navigation.openDrawer();
    } else {
      await navigation.navigate(routeName);
    }
  };

  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };
  handleChange = (name, value) => {
    this.setState({
      [name]: value,
    });
  };
  login = async () => {
    // this.setState({
    //   loader: true,
    // });
    console.log(`${config.apiUrl}/login`, {
      email: this.state.email,
      password: this.state.password,
    })
    await fetch(`${config.apiUrl}/login`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: this.state.email,
        password: this.state.password,
      }),
    })
      .then((response) => {
        console.log('login api response', response)
        return response.json()
      })
      .then((responseJson) => {
        console.log('login response : ', responseJson)
        // SyncStorage.set('user', responseJson.data);
        Toast.show(responseJson.message, Toast.LONG);
        if (responseJson.status) {
          Storage.storeData('user', responseJson.data);
          Storage.storeData('profile', this.state.email);
          this.setState({
            loader: false,
          });
          this.startReload();
        } else {
          Toast.show('Invalid Credentials');
          this.setState({
            loader: false,
          });
        }
      }).catch((err) => {
        console.log('login error', err)
      });
  };
  _signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      // this.setState({ userInfo: userInfo, loggedIn: true });
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // alert(statusCodes.SIGN_IN_CANCELLED)
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // alert(statusCodes.IN_PROGRESS)
        // operation (f.e. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // alert(statusCodes.PLAY_SERVICES_NOT_AVAILABLE)
        // play services not available or outdated
      } else {
        alert('Error Please Try Again', error);
      }
    }
  };
  handleFacebookLogin() {
    LoginManager.logInWithPermissions(['public_profile', 'email']).then(
      (result, error) => {
        if (error) {
          alert(JSON.stringify(error));
        } else if (result.isCancelled) {
          console.log('Login is cancelled.');
        } else {
          AccessToken.getCurrentAccessToken().then((data) => {
            const processRequest = new GraphRequest(
              '/me?fields=name,email,picture.type(large)',
              null,
              this.getResponseInfo,
            );

            // Start the graph request.
            new GraphRequestManager().addRequest(processRequest).start();
          });
        }
      },
    );
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        {this.state.loader ? (
          // <View
          //   style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          //   <ActivityIndicator
          //     size="large"
          //     color="#00ff00"
          //     animating={this.state.loader}
          //   />
          // </View>
          <CustomLoader />
        ) : (
          <LoginScreen
            navigate={this.navigate}
            goBack={this.goBack}
            login={this.login}
            handleChange={this.handleChange}
            googleLogin={this._signIn}
            facebookLogin={this.handleFacebookLogin}
          />
        )}
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: whiteColor,
  },
});

export default LoginContainer;
