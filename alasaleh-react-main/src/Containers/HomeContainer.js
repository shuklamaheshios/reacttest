import React, {Component} from 'react';
import {Button, SafeAreaView, ActivityIndicator, View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {HomeScreen} from '../Screens';
import {whiteColor} from '../Theme/colors';
import Storage from '../Utils/storage';
import config from '../Utils/config';
import axios from 'axios';
import Geolocation from '@react-native-community/geolocation';
import DataLoader from '../Components/Dataloader';
import {CustomLoader} from '../Components';
import Toast from 'react-native-simple-toast';
class HomeContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      parentCategory: [{}],
      loader: false,
      lat: 0,
      long: 0,
      flashOffersList: [{}],
      token: '',
      homePageProducts: {},
      is_updated: false,
      update: 0,
      wish: 0,
    };
    this.props.navigation.addListener('didFocus', (payload) => {
      this.setState({is_updated: true});
    });
  }

  componentDidMount = async () => {
    // await Storage.retrieveData('cartData');
    // await Storage.removeData('cartDataDetails');
    // Geolocation.getCurrentPosition(
    //   (position) => {
    //     const initialPosition = JSON.stringify(position);
    //     this.setState({initialPosition});
    //     console.log('initialPosition:', initialPosition);
    //   },
    //   (error) => console.log('Error', JSON.stringify(error)),
    //   {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
    // );
    // this.state.watchID = Geolocation.watchPosition((position) => {
    //   const lastPosition = JSON.stringify(position);
    //   this.setState({
    //     lat: position.coords.latitude,
    //     long: position.coords.longitude,
    //   });
    // });

    this.getProductsByCategory();
    this.getFlashOffers();
    this.getHomePageProducts();
    this.getwish();
  }

  getwish = async () => {
    let token = null;
    await Storage.retrieveData('user').then((resp) => {
      if (resp == 'Unauthorised') {
        token = null;
      } else {
        token = resp;
      }
    });
    fetch(`${config.apiUrl}/wishlist/show`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token.token}`,
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        // setWishlistCount(responseJson.data.length);
        this.setState({
          wish: responseJson.data.length,
        });
      });
  };

  navigate = async (routeName, id) => {
    const {navigation} = this.props;
    if (routeName === 'drawer') {
      navigation.openDrawer();
    } else {
      await navigation.navigate(routeName, id);
    }
  };
  getProductsByCategory = async () => {
    this.setState({
      loader: true,
    });
    await fetch(`${config.apiUrl}/get_all_parent_categories`)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          parentCategory: responseJson.data,
          loader: false,
        });
      })
      .catch((error) => {
        console.error(error);
      });
  };

  getHomePageProducts = async () => {
    this.setState({
      loader: true,
    });
    await fetch(`${config.apiUrl}/home_page_products`)
      .then((response) => response.json())
      .then((responseJson) => {
        // console.log('home page product:', responseJson.data);
        this.setState({
          homePageProducts: responseJson.data,
          loader: false,
        });
      })
      .catch((error) => {
        console.error(error);
      });
  };
  getFlashOffers = async () => {
    this.setState({
      loader: true,
    });
    await fetch(
      `${config.apiUrl}/get_flash_and_promotional_offers`,
    )
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          flashOffersList: responseJson.data.flash_offers,
          loader: false,
        });
      })
      .catch((error) => {
        console.error(error);
      });
  };
  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };
  update = async () => {
    let token = null;
    await Storage.retrieveData('cartData').then((resp) => {
      token = resp;
    });
    if (token) {
      await fetch(`${config.apiUrl}/carts/show_cart_data`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          cartKey: token.cartKey,
        }),
      })
        .then((response) => response.json())
        .then((responseJson) => {
          // setCartCount(responseJson['item_in_cart'].length);

          let cartQC = 0;
          responseJson['item_in_cart'].map((cartData) => {
            cartQC = parseInt(cartData.Quantity) + cartQC;
          });
          this.setState({
            update: cartQC,
          });
        })
        .catch((e) => {
          this.setState({
            loader: false,
          });
        });
    }
    // await Storage.storeData('cartCount', this.state.update + 1);
  };
  updateWp = async () => {
    this.setState({
      wish: this.state.wish + 1,
    });
    await Storage.storeData('wishCount', this.state.wish + 1);
  };
  updateWr = async () => {
    if (this.state.wish !== 0) {
      this.setState({
        wish: this.state.wish - 1,
      });
      await Storage.storeData('wishCount', this.state.wish - 1);
    }
  };
  
  addLocalCart = async (item) => {
    let token = null;
    let user = null;
    let pr = 0;
    await Storage.retrieveData('cartData').then((resp) => {
      token = resp;
    });
    await Storage.retrieveData('user').then((resp) => {
      user = resp;
    });

    if (token === null) {
      await fetch(`${config.apiUrl}/carts`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }).then((response) => response.json());
      // .then((responseJson) => {
      //   Storage.storeData('cartData', responseJson);
      //   token = responseJson;
      //   console.log(token);
      // });
    }
    if (item.promotion === '1' || item.promotion === 1) {
      pr = parseFloat(item.promotion_price).toFixed(2);
    } else {
      const k = (parseFloat(item.price) * parseFloat(item.discount)) / 100;
      const calculated = item.price - k;
      let p = calculated.toFixed(2);
      if (p > calculated) {
        p = p - 0.01;
      }
      pr = p;
    }

    let toCount = 0;
    await fetch(
      `${config.apiUrl}/carts/show_cart_data/${token.cartToken}`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          cartKey: token.cartKey,
        }),
      },
    )
      .then((response) => response.json())
      .then((responseJson) => {
        responseJson['item_in_cart'].map((cartItem) => {
          if (cartItem.productID === item.id) {
            toCount = parseInt(cartItem.Quantity);
            console.log('toCount.id', cartItem?.Quantity);
          }
        });

        console.log('cart', responseJson['item_in_cart']);
        // responseJson['item_in_cart'].map(item => this.getProducts(item.Name))
      })
      .catch((e) => {
        console.log('error is', e);
        this.setState({
          loader: false,
        });
      });
    let qti = 1;
    if (toCount !== 0) {
      qti = toCount + 1;
    }
    await fetch(
      `${config.apiUrl}/carts/add_to_cart/${token.cartToken}`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          cartKey: token.cartKey,
          productID: item.id,
          quantity: qti.toString(),
          price: item.price,
          image: item.image,
        }),
      },
    )
      .then((response) => response.json())
      .then((res) => {
        Toast.show(res.message);
        // this.update();
        forceUpdate();
      });
  };
  addinWishL = async (id) => {
    let token = '';
    await Storage.retrieveData('user').then((resp) => {
      token = resp.token;
    });
    const params = {
      product_id: id,
    };
    axios
      .post(`${config.apiUrl}/whishlist`, params, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        Toast.show(res.message, Toast.LONG);
        this.updateWp();
      })
      .catch((error) => {
        console.error(error);
      });
  };
  removeWishL = async (id) => {
    let token = '';
    await Storage.retrieveData('user').then((resp) => {
      token = resp.token;
    });
    const params = {
      product_id: id,
    };
    axios
      .post(`${config.apiUrl}/whishlist/delete`, params, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        Toast.show(res.message, Toast.SHORT);
        this.updateWr();
      })
      .catch((error) => {
        console.error(error);
      });
  };
  // formatedata = (products)  =>{
  //   let data=[]
  //   products.map(product => {
  //     console.log(product)
  //   })
  // }
  render() {

    return (
      <SafeAreaView style={styles.safeAreaContainer}>
        {this.state.loader ? (
          <CustomLoader />
        ) : (
          <HomeScreen
            navigate={this.navigate}
            goBack={this.goBack}
            parentCategory={
              this.state.parentCategory ? this.state.parentCategory : [{}]
            }
            flashOffersList={
              this.state.flashOffersList ? this.state.flashOffersList : [{}]
            }
            homePageProducts={this.state.homePageProducts}
            update={this.update}
            count={this.state.update}
            addLocalCart={this.addLocalCart}
            removeWishL={this.removeWishL}
            addinWishL={this.addinWishL}
            wishCount={this.state.wish}
          />
        )}
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  safeAreaContainer: {
    flex: 1,
    backgroundColor: whiteColor,
  },
});

export default HomeContainer;
