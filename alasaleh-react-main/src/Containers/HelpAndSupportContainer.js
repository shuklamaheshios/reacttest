import React, {Component} from 'react';
import {SafeAreaView} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {HelpAndSupport} from '../Screens';
import {whiteColor} from '../Theme/colors';
import axios from 'axios';
import Toast from 'react-native-simple-toast';
import config from '../Utils/config';
class HelpAndSupportContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
    };
  }
  componentDidMount() {}
  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };
  handleChange = (name, value) => {
    this.setState({
      [name]: value,
    });
  };

  navigate = async (routeName, billingInfo) => {
    const {navigation} = this.props;
    if (routeName === 'drawer') {
      navigation.openDrawer();
    } else {
      await navigation.navigate(routeName, billingInfo);
    }
  };

  submit = () => {
    const params = {
      email: this.state.email,
      name: this.state.name,
      phone: this.state.phone,
      message: this.state.message,
    };
    axios
      .post(`${config.apiUrl}/save_help_support`, params)
      .then((res) => {
        Toast.show(res.data.message, Toast.LONG);
      })
      .catch((error) => {
        console.error(error);
        Toast.show('Error', Toast.LONG);
      });
  };
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <HelpAndSupport
          goBack={this.goBack}
          handleChange={this.handleChange}
          submit={this.submit}
          navigate={this.navigate}
        />
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: whiteColor,
  },
});

export default HelpAndSupportContainer;
