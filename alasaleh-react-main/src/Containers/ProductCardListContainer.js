import React, {Component} from 'react';
import {Text, SafeAreaView, ActivityIndicator, View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {CustomLoader} from '../Components';
import {ProductCardListScreen} from '../Screens';
import {whiteColor} from '../Theme/colors';
import Storage from '../Utils/storage';
import config from '../Utils/config';

class ProductCardListContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      productList: [],
      loader: false,
      update: 0,
    };
  }
  componentDidMount() {
    const {params} = this.props.route;
    if (params?.id) {
      this.getProducts(params?.id, params?.title);
    } else {
      this.setState({
        productList: params,
      });
    }
  }
  getProducts = async (id, title) => {
    this.setState({
      loader: true,
    });
    const removespace = title.replace(' ', '%20');
    await fetch(
      `${config.apiUrl}/search_by_category/category_id/${id}`,
    )
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          productList: responseJson.data.lims_product_list,
          loader: false,
        });
      })
      .catch((error) => {
        console.error(error);
      });
  };
  navigate = async (routeName, params) => {
    const {navigation} = this.props;
    if (routeName === 'drawer') {
      await navigation.openDrawer();
    } else {
      await navigation.navigate(routeName, params);
    }
  };

  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };
  update = async () => {
    let token = null;
    await Storage.retrieveData('cartData').then((resp) => {
      token = resp;
    });
    if (token) {
      await fetch(`${config.apiUrl}/carts/show_cart_data`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          cartKey: token.cartKey,
        }),
      })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log('Items in Cart', responseJson['item_in_cart'].length);
          // setCartCount(responseJson['item_in_cart'].length);
          let cartQC = 0;
          responseJson['item_in_cart'].map((cartData) => {
            cartQC = parseInt(cartData.Quantity) + cartQC;
          });
          this.setState({
            update: cartQC,
          });
        })
        .catch((e) => {
          console.log('error is', e);
          this.setState({
            loader: false,
          });
        });
    }
    // await Storage.storeData('cartCount', this.state.update + 1);
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        {this.state.loader ? (
          // <View
          //   style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          //   <ActivityIndicator
          //     size="large"
          //     color="#00ff00"
          //     animating={this.state.loader}
          //   />
          // </View>
          <CustomLoader />
        ) : (
          <ProductCardListScreen
            navigate={this.navigate}
            products={this.state.productList ? this.state.productList : [{}]}
            update={this.update}
            countCart={this.state.update}
          />
        )}
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: whiteColor,
    padding: 0,
    margin: 0,
  },
});

export default ProductCardListContainer;
