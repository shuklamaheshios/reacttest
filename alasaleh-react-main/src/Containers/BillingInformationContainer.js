import React, { Component } from 'react';
import { SafeAreaView, ActivityIndicator, View } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import { BillingInformationScreen } from '../Screens';
import Geocoder from 'react-native-geocoding';
import axios from 'axios';
import Storage from '../Utils/storage';
import config from '../Utils/config';
import { CustomLoader } from '../Components';
class BillingInformationContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      coupenCode: '',
      token: '',
      loader: false,
      location: '',
      user: {},
      coupen: false,
    };
  }

  async componentDidMount() {
    this.getData();
    await Storage.retrieveData('user').then((resp) => {
      this.setState({
        token: resp.token,
      });
      this.getProfile(resp);
    });
  }
  handlechange = (name, value) => {
    this.setState({
      [name]: value,
    });
  };

  handlelatlng = (name, value) => {
    console.log(name, value);
    this.setState({
      [name]: value,
    });
    this.setState({
      newlat: true,
    });
    this.forceUpdate();
  };

  getProfile = async (user) => {
    this.setState({
      loader: true,
    });
    let data = {
      id: user.user_id,
    };
    axios
      .get(`${config.apiUrl}/user/${user.user_id}`, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${user.token}`,
        },
      })
      .then((res) => {
        this.setState({ user: res.data.data });
        this.setState({
          loader: false,
        });
      })
      .catch((error) => {
        console.log('errororor , Billing');
        console.error(error);
      });
  };

  validateCoupenCode = () => {
    this.setState({
      loader: false,
    });
    const params = {
      coupon_code: this.state.coupenCode,
    };
    // console.log('this.state.coupenCode', this.state.coupenCode);
    // console.log('coupen discount validate call', this.state.token);
    axios
      .post(`${config.apiUrl}/validate_coupon_code`, params, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${this.state.token}`,
        },
      })
      .then((res) => {
        // console.log('coupenCode is: ', res.data);
        console.log('coupen discount issss:', res.data);
        if (res.data.status === true) {
          Storage.storeData('coupon', res.data);
          Storage.storeData('skip', 'validCoupon');
        }
        this.setState({
          coupen: true,
          loader: false,
        });
      })
      .catch((error) => {
        console.log('errororor billing 2');
        console.error(error);
      });
  };

  navigate = async (routeName, billingInfo) => {
    const { navigation } = this.props;
    if (routeName === 'drawer') {
      navigation.openDrawer();
    } else {
      await navigation.navigate(routeName, billingInfo);
    }
  };

  goBack = () => {
    const { navigation } = this.props;
    navigation.goBack();
  };

  getData = async (lat, lng) => {
    Geocoder.init('AIzaSyCvypE2o4uA0pvGQxwY4KqfXryigcrDqTQ');

    let location = '';
    let country = '';
    let city = '';
    let zip = '';
    await Geocoder.from(lat, lng).then((json) => {
      json.results[1].address_components.forEach((value, index) => {
        console.log(value);
        if (value.types[0] === 'country') {
          country = value.long_name;
        } else if (value.types[0] === 'administrative_area_level_1') {
          city = value.long_name;
        } else if (value.types[0] === 'postal_code') {
          zip = value.long_name;
        }

        location = location + value.long_name;
      });
    });
    this.setState({
      user: {
        ...this.state.user,
        country: country,
        city: city,
        zip_code: zip,
        latitude: lat.toString(),
        longitude: lng.toString(),
        address: location,
      },
      location: location,
    });
  };
  render() {
    return (
      <SafeAreaView style={styles.container}>
        {this.state.loader ? (
          // <View
          //   style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          //   <ActivityIndicator
          //     size="large"
          //     color="#00ff00"
          //     animating={this.state.loader}
          //   />
          // </View>
          <CustomLoader />
        ) : (
            <BillingInformationScreen
              navigate={this.navigate}
              goBack={this.goBack}
              handlechange={this.handlechange}
              validateCoupenCode={this.validateCoupenCode}
              user={this.state.user ? this.state.user : {}}
              latlng={this.state}
              handlelatlng={this.handlelatlng}
              locationAddress={this.state.location}
              newlat={this.state.newlat}
              getLocation={this.getData}
              coupen={this.state.coupen}
            />
          )}
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
});

export default BillingInformationContainer;
