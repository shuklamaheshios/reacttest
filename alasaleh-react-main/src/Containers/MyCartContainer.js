import React from 'react';
import {Component} from 'react';
import {SafeAreaView, ActivityIndicator, View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {CustomLoader} from '../Components';
import MyCart from '../Screens/MyCart';
import Storage from '../Utils/storage';
import config from '../Utils/config';
class MyCartContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      carttoken: '',
      price: 0,
      qnty: 1,
      loader: false,
      listS: [],
      updateCount: 0,
    };
  }

  async componentDidMount() {
    let token = null;

    await Storage.retrieveData('cartData').then((resp) => {
      token = resp;
      this.setState({
        carttoken: resp,
      });
    });
    await Storage.retrieveData('cartDataDetails').then((resp) => {
      this.setState({
        listS: resp,
      });
    });
    await fetch(
      `${config.apiUrl}/carts/show_cart_data/${token.cartToken}`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          cartKey: token.cartKey,
        }),
      },
    )
      .then((response) => response.json())
      .then((responseJson) => {
        let cartQC = 0;
        responseJson['item_in_cart'].map((cartData) => {
          cartQC = parseInt(cartData.Quantity) + cartQC;
        });
        this.setState({
          updateCount: cartQC,
        });
        this.setState({
          list: responseJson['item_in_cart'],
          loader: false,
        });
        // responseJson['item_in_cart'].map(item => this.getProducts(item.Name))
      })
      .catch((e) => {
        console.log('error is', e);
        this.setState({
          loader: false,
        });
      });
  }
  // getProducts = async (id) => {
  //   await fetch(
  //     `${config.apiUrl}/productDetails/Tazah-Fava-Beans-With-Chick-Peas`,
  //     {
  //       method: 'GET',
  //       headers: {
  //         Accept: 'application/json',
  //         'Content-Type': 'application/json',
  //       }
  //     },
  //   )
  //     .then((response) => response.json())
  //     .then((responseJson) => {
  //       console.log("otem", responseJson)
  //     })
  //     .catch((e) => {
  //       console.log('error is', e);
  //       this.setState({
  //         loader: false,
  //       });
  //     });

  // }
  updateCart = async (item, chk) => {
    // let pr = 0;
    // if (item.promotion === 1 || item.promotion === '1') {
    //   pr = parseFloat(item.promotion_price).toFixed(2);
    // } else {
    //   const k = (parseFloat(item.price) * parseFloat(item.discount)) / 100;
    //   const calculated = item.price - k;
    //   let p = calculated.toFixed(2);
    //   if (p > calculated) {
    //     p = p - 0.01;
    //   }
    //   pr = p;
    // }
    // console.log('mmmmmmmm', item);
    let toCount = 0;
    let cartQC = 0;
    await fetch(
      `${config.apiUrl}/carts/show_cart_data/${this.state.carttoken.cartToken}`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          cartKey: this.state.carttoken.cartKey,
        }),
      },
    )
      .then((response) => response.json())
      .then((responseJson) => {
        responseJson['item_in_cart'].map((cartItem) => {
          cartQC = parseInt(cartItem.Quantity) + cartQC;
          if (cartItem.productID === item.id) {
            toCount = parseInt(cartItem.Quantity);
            console.log('toCount.id', cartItem?.Quantity);
          }
        });
        console.log('cartQC', cartQC);
        this.setState({
          updateCount: cartQC,
        });

        // console.log('cart', responseJson['item_in_cart']);
        // responseJson['item_in_cart'].map(item => this.getProducts(item.Name))
      })
      .catch((e) => {
        console.log('error is', e);
        this.setState({
          loader: false,
        });
      });
    let qti = 1;
    if (toCount !== 0) {
      qti = toCount + 1;
    }
    await fetch(
      `${config.apiUrl}/carts/add_to_cart/${this.state.carttoken.cartToken}`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          cartKey: this.state.carttoken.cartKey,
          productID: item.productID,
          quantity: item.Quantity,
          price: item.price,
          image: item.image,
        }),
      },
    )
      .then((response) => response.json())
      .then((responseJson) => {
        // console.log('respppp', this.state.updateCount);
        if (chk) {
          this.setState({
            updateCount: cartQC + 1,
          });
        } else {
          this.setState({
            updateCount: cartQC - 1,
          });
        }
        this.forceUpdate();
        this.setState({
          loader: false,
        });
      });
  };
  navigate = async (routeName) => {
    const {navigation} = this.props;
    if (routeName === 'drawer') {
      await navigation.openDrawer();
    } else {
      await navigation.navigate(routeName);
    }
  };

  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };
  render() {
    const {list} = this.state;
    let totalprice = 0;
    let p = 0;
    if (list) {
      list.map((item) => {
        if (item.dq) {
          p = parseFloat(item?.price) * item?.dq;
          // console.log('p', p);
        } else {
          p = parseFloat(item?.price) * item?.Quantity;
          // console.log('p else', p);
        }

        totalprice = parseFloat(p) + totalprice;
      });
    }
    Storage.storeData('price', totalprice);
    return (
      <SafeAreaView style={styles.container}>
        {this.state.loader ? (
          // <View
          //   style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          //   <ActivityIndicator
          //     size="large"
          //     color="#00ff00"
          //     animating={this.state.loader}
          //   />
          // </View>
          <CustomLoader />
        ) : (
          <MyCart
            goBack={this.goBack}
            list={this.state.list ? this.state.list : []}
            totalprice={totalprice}
            // list={this.state.listS}
            navigate={this.navigate}
            updateCart={this.updateCart}
            cartToken={this.state.carttoken}
            count={this.state.updateCount}
          />
        )}
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
});

export default MyCartContainer;
