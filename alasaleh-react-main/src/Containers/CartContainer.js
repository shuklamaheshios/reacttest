import React, {Component} from 'react';
import {SafeAreaView} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {CartScreen} from '../Screens';
import {whiteColor} from '../Theme/colors';
import Storage from '../Utils/storage';
import Toast from 'react-native-simple-toast';
import config from '../Utils/config';
class CartContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countCart: 0,
      wish: true,
    };
  }
  componentDidMount() {
    const {route} = this.props;
    this.setState(route.params);
  }
  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  navigate = async (routeName, id) => {
    const {navigation} = this.props;
    if (routeName === 'drawer') {
      navigation.openDrawer();
    } else {
      await navigation.navigate(routeName, id);
    }
  };
  update = async () => {
    let token = null;
    await Storage.retrieveData('cartData').then((resp) => {
      token = resp;
    });
    if (token) {
      await fetch(`${config.apiUrl}/carts/show_cart_data`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          cartKey: token.cartKey,
        }),
      })
        .then((response) => response.json())
        .then((responseJson) => {
          // setCartCount(responseJson['item_in_cart'].length);
          let cartQC = 0;
          responseJson['item_in_cart'].map((cartData) => {
            cartQC = parseInt(cartData.Quantity) + cartQC;
          });
          this.setState({
            countCart: cartQC,
          });
        })
        .catch((e) => {
          this.setState({
            loader: false,
          });
        });
    }
    // await Storage.storeData('cartCount', this.state.update + 1);
  };
  addtocart = async (item) => {
    let pr = 0;
    let token = null;
    let user = null;
    let cartDet = null;
    await Storage.retrieveData('cartDataDetails').then((resp) => {
      cartDet = resp;
    });
    if (cartDet) {
      cartDet.push(item);
      Storage.storeData('cartDataDetails', cartDet);
    } else {
      Storage.storeData('cartDataDetails', [cartDet]);
    }
    await Storage.retrieveData('cartData').then((resp) => {
      token = resp;
    });
    await Storage.retrieveData('user').then((resp) => {
      user = resp;
    });

    if (token === null) {
      await fetch(`${config.apiUrl}/carts`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      })
        .then((response) => response.json())
        .then((responseJson) => {
          Storage.storeData('cartData', responseJson);
          token = responseJson;
        });
    }
    if (item.promotion === '1' || item.promotion === 1) {
      pr = parseFloat(item.promotion_price).toFixed(2);
    } else {
      const k = (parseFloat(item.price) * parseFloat(item.discount)) / 100;
      const calculated = item.price - k;
      let p = calculated.toFixed(2);
      if (p > calculated) {
        p = p - 0.01;
      }
      pr = p;
    }
    let toCount = 0;
    await fetch(
      `${config.apiUrl}/carts/show_cart_data/${token.cartToken}`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          cartKey: token.cartKey,
        }),
      },
    )
      .then((response) => response.json())
      .then((responseJson) => {
        responseJson['item_in_cart'].map((cartItem) => {
          if (cartItem.productID === item.id) {
            toCount = parseInt(cartItem.Quantity);
            console.log('toCount.id', cartItem?.Quantity);
          }
        });

        console.log('cart', responseJson['item_in_cart']);
        // responseJson['item_in_cart'].map(item => this.getProducts(item.Name))
      })
      .catch((e) => {
        console.log('error is', e);
        this.setState({
          loader: false,
        });
      });
    let qti = 1;
    if (toCount !== 0) {
      qti = toCount + 1;
    }
    await fetch(
      `${config.apiUrl}/carts/add_to_cart/${token.cartToken}`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          cartKey: token.cartKey,
          productID: item.id,
          quantity: qti.toString(),
          price: pr,
          image: item.image,
        }),
      },
    )
      .then((response) => response.json())
      .then((res) => {
        Toast.show(res.message);
        this.update();
        forceUpdate();
      });
  };
  changeWish = () => {
    this.setState({
      wish: !this.state.wish,
    });
    this.forceUpdate();
  };
  render() {
    const {params} = this.props.route;
    return (
      <SafeAreaView style={styles.container}>
        <CartScreen
          stock={params}
          navigate={this.navigate}
          goBack={this.goBack}
          product={this.state}
          addtocart={this.addtocart}
          countCart={this.state.countCart}
          changeWish={this.changeWish}
        />
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: whiteColor,
  },
});

export default CartContainer;
