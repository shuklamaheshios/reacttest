import React, {Component} from 'react';
import {SafeAreaView} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {ShippingPolicyScreen} from '../Screens';
import {whiteColor} from '../Theme/colors';
import config from '../Utils/config';

class ShippingPolicyContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
    };
  }
  componentDidMount() {
    this.getdetail();
  }

  navigate = async (routeName) => {
    const {navigation} = this.props;
    if (routeName === 'drawer') {
      await navigation.openDrawer();
    } else {
      await navigation.navigate(routeName);
    }
  };
  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };
  getdetail = async () => {
    await fetch(`${config.apiUrl}/static_pages/shipping`)
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('shipping', responseJson);
        this.setState({
          data: responseJson.data,
        });
      })
      .catch((error) => {
        console.error(error);
      });
  };
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <ShippingPolicyScreen
          navigate={this.navigate}
          goBack={this.goBack}
          data={this.state.data}
        />
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: whiteColor,
  },
});

export default ShippingPolicyContainer;
