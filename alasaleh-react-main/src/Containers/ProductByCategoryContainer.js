import React, {Component} from 'react';
import {SafeAreaView} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {ProductByCategoryScreen} from '../Screens';
import {whiteColor} from '../Theme/colors';

class ProductByCategoryContainer extends Component {
  navigate = async (routeName) => {
    const {navigation} = this.props;
    if (routeName === 'drawer') {
      await navigation.openDrawer();
    } else {
      await navigation.navigate(routeName);
    }
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <ProductByCategoryScreen navigate={this.navigate} />
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: whiteColor,
  },
});

export default ProductByCategoryContainer;
