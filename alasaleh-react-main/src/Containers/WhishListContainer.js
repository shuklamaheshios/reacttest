import React from 'react';
import {Component} from 'react';
import {SafeAreaView, ActivityIndicator, View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {WhishListScreen} from '../Screens';
import {lightGray} from '../Theme/colors';
import Storage from '../Utils/storage';
import config from '../Utils/config';
import axios from 'axios';
import {CustomLoader} from '../Components';

class WhishListContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: '',
      list: [],
      loader: false,
    };
  }
  componentDidMount() {
    Storage.retrieveData('user').then((resp) => {
      this.getwish(resp.token);
      this.setState({
        token: resp.token,
      });
    });
  }
  navigate = async (routeName) => {
    const {navigation} = this.props;
    if (routeName === 'drawer') {
      await navigation.openDrawer();
    } else {
      await navigation.navigate(routeName);
    }
  };

  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };
  getwish = (t) => {
    this.setState({
      loader: true,
    });
    fetch(`${config.apiUrl}/wishlist/show`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${t}`,
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          list: responseJson.data,
          loader: false,
        });
      });
  };
  removeWish = (id) => {
    this.setState({
      loader: true,
    });
    const params = {
      product_id: id,
    };
    axios
      .post(`${config.apiUrl}/whishlist/delete`, params, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${this.state.token}`,
        },
      })
      .then((res) => {
        this.setState({
          list: res.data.data,
          loader: false,
        });
      })
      .catch((error) => {
        console.log('errororor');
        console.error(error);
      });
  };
  render() {
    return (
      <SafeAreaView style={styles.container}>
        {this.state.loader ? (
          // <View
          //   style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          //   <ActivityIndicator
          //     size="large"
          //     color="#00ff00"
          //     animating={this.state.loader}
          //   />
          // </View>
          <CustomLoader />
        ) : (
          <WhishListScreen
            removeWish={this.removeWish}
            navigate={this.navigate}
            goBack={this.goBack}
            list={this.state.list ? this.state.list : [{}]}
          />
        )}
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f5f5',
  },
});

export default WhishListContainer;
