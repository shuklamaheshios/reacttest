import React, { Component } from 'react';
import { SafeAreaView, ActivityIndicator, View } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import { CustomLoader } from '../Components';
import { RegisterScreen } from '../Screens';
import { whiteColor } from '../Theme/colors';
import Toast from 'react-native-simple-toast';
import config from '../Utils/config';
// import { GoogleSignin, statusCodes } from '@react-native-community/google-signin';
class RegisterContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      f_name: '',
      l_name: '',
      email: '',
      password: '',
      loader: false,
      confirm_password: ''
    };
  }

  componentDidMount() {
    // GoogleSignin.configure({
    //   scopes: ['email'], // what API you want to access on behalf of the user, default is email and profile
    //   webClientId:
    //     '418977770929-g9ou7r9eva1u78a3anassxxxxxxx.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
    //   offlineAccess: true,
    // });
  }

  // googles = async () => {
  //   try {
  //     await GoogleSignin.hasPlayServices();
  //     const userInfo = await GoogleSignin.signIn();
  //     this.setState({ userInfo });
  //     console.log(userInfo)
  //   } catch (error) {
  //     if (error.code === statusCodes.SIGN_IN_CANCELLED) {
  //       // user cancelled the login flow
  //     } else if (error.code === statusCodes.IN_PROGRESS) {
  //       // operation (e.g. sign in) is in progress already
  //     } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
  //       // play services not available or outdated
  //     } else {
  //       // some other error happened
  //       console.log(error)
  //     }
  //   }
  // };

  navigate = async (routeName, params) => {
    const { navigation } = this.props;
    if (routeName === 'drawer') {
      await navigation.openDrawer();
    } else {
      await navigation.navigate(routeName, params);
    }
  };

  goBack = () => {
    const { navigation } = this.props;
    navigation.goBack();
  };
  handleChange = (name, value) => {
    this.setState({
      [name]: value,
    });
    console.log(name, value);
  };
  genrateOtp = async () => {

    if (this.state.password === this.state.confirm_password && this.state.password != '' && this.state.f_name != '' && this.state.l_name != '' &&  this.state.email != '') {
      this.setState({
        loader: true,
      });
      await fetch(`${config.apiUrl}/send_otp_code`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email: this.state.email,
        }),
      })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log('otp response', responseJson)
          this.setState({
            loader: false,
          });
          // SyncStorage.set('user', responseJson.data);
          const params = {
            name: this.state.f_name,
            last_name: this.state.l_name,
            email: this.state.email,
            password: this.state.password,
            c_password: this.state.password,
            otp: responseJson.message.otp_code
          };
          if (responseJson.status) this.navigate('OTP', params);
          else console.log('invalid credentials', responseJson);
        });
    }
    else {
      Toast.show('please fill all information and make sure password match');
    }
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        {this.state.loader ? (
          // <View
          //   style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          //   <ActivityIndicator
          //     size="large"
          //     color="#00ff00"
          //     animating={this.state.loader}
          //   />
          // </View>
          <CustomLoader />
        ) : (
            <RegisterScreen
              signIni={this.googles}
              goBack={this.goBack}
              genrateOtp={this.genrateOtp}
              handleChange={this.handleChange}
              navigate={this.navigate}
            />
          )}
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: whiteColor,
  },
});

export default RegisterContainer;
