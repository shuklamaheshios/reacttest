import React, {Component} from 'react';
import {SafeAreaView} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {RefundScreen} from '../Screens';
import {whiteColor} from '../Theme/colors';
import config from '../Utils/config';

class RefundContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
    };
  }
  componentDidMount() {
    this.getdetail();
  }
  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };
  getdetail = async () => {
    await fetch(
      `${config.apiUrl}/static_pages/REFUND-AND-RETURN-POLICY`,
    )
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('REFUND-AND-RETURN-POLICY', responseJson);
        this.setState({
          data: responseJson.data,
        });
      })
      .catch((error) => {
        console.error(error);
      });
  };

  navigate = async (routeName) => {
    const {navigation} = this.props;
    if (routeName === 'drawer') {
      await navigation.openDrawer();
    } else {
      await navigation.navigate(routeName);
    }
  };
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <RefundScreen
          navigate={this.navigate}
          goBack={this.goBack}
          data={this.state.data}
        />
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: whiteColor,
  },
});

export default RefundContainer;
