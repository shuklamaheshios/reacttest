import React from 'react';
import {Component} from 'react';
import {SafeAreaView, View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {AddMobileScreen} from '../Screens';
import config from '../Utils/config';
class AddMobileContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      f_name: '',
      l_name: '',
      mobile: '',
      password: '',
      loader: false,
    };
  }
  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  navigate = async (routeName, billingInfo) => {
    const {navigation} = this.props;
    if (routeName === 'drawer') {
      navigation.openDrawer();
    } else {
      await navigation.navigate(routeName, billingInfo);
    }
  };

  handleChange = (name, value) => {
    this.setState({
      [name]: value,
    });
  };

  genrateOtp = async () => {
    this.setState({
      loader: true,
    });
    await fetch(`${config.apiUrl}/send_otp_code`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: this.state.mobile,
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('resp', responseJson.message.otp);
        this.setState({
          loader: false,
        });
        // SyncStorage.set('user', responseJson.data);
        const params = {
          name: this.state.f_name,
          last_name: this.state.l_name,
          email: this.state.mobile,
          password: this.state.password,
          c_password: this.state.password,
          otp: responseJson.message.otp,
        };
        if (responseJson.status) this.navigate('OTP', params);
        else console.log('invalid credentials');
      });
  };
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <AddMobileScreen
          handleChange={this.handleChange}
          genrateOtp={this.genrateOtp}
          goBack={this.goBack}
          navigate={this.navigate}
        />
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
});

export default AddMobileContainer;
