import React, { Component } from 'react';
import { SafeAreaView, ActivityIndicator, View } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
// import Geocoder from 'react-native-geocoder';
import { ProfileScreen } from '../Screens';
import axios from 'axios';
import Geolocation from '@react-native-community/geolocation';

import Storage from '../Utils/storage';
import config from '../Utils/config';
import { CustomLoader } from '../Components';
import ImagePicker from 'react-native-image-picker';
import Geocoder from 'react-native-geocoding';
import { withNavigation } from 'react-navigation';

class ProfileContainer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      user: {},
      token: '',
      loader: false,
      image: null,
      lat: 0,
      lng: 0,
      newlat: false,
      location: '',
      gotit: true,

    };
  }

  async componentDidMount() {
    this.getData;
    await Storage.retrieveData('user').then((resp) => {
      this.setState({
        token: resp.token,
      });
      this.getProfile(resp);
    });
  }

  // componentDidUpdate() {
  //   if (this.state.gotit) {

  //   }
  // }

  getData = async (lat, lng) => {
    Geocoder.init('AIzaSyCvypE2o4uA0pvGQxwY4KqfXryigcrDqTQ');

    let location = '';
    let country = '';
    let city = '';
    let zip = '';
    await Geocoder.from(lat, lng).then((json) => {
      json.results[1].address_components.forEach((value, index) => {
        console.log(value);
        if (value.types[0] === 'country') {
          country = value.long_name;
        } else if (value.types[0] === 'administrative_area_level_1') {
          city = value.long_name;
        } else if (value.types[0] === 'postal_code') {
          zip = value.long_name;
        }

        location = location + value.long_name;
      });
    });
    this.setState({
      user: {
        ...this.state.user,
        country: country,
        city: city,
        zip_code: zip,
        latitude: lat.toString(),
        longitude: lng.toString(),
      },
      location: location,
    });
  };

  navigate = async (routeName, param) => {
    const { navigation } = this.props;
    if (routeName === 'drawer') {
      await navigation.openDrawer();
    } else {
      await navigation.navigate(routeName, param);
    }
  };

  goBack = () => {
    const { navigation } = this.props;
    navigation.goBack();
  };
  handleChange = (name, value) => {
    console.log(name, value);
    this.setState({
      [name]: value,
    });
  };
  handlelatlng = (name, value) => {
    this.setState({
      [name]: value,
    });
    this.setState({
      newlat: true,
    });
    this.forceUpdate();
  };

  getProfile = async (user) => {
    this.setState({
      loader: true,
    });
    let data = {
      id: user.user_id,
    };
    axios
      .get(`${config.apiUrl}/user/${user.user_id}`, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${user.token}`,
        },
      })
      .then((res) => {
        this.setState({ user: res.data.data, loader: false });
      })
      .catch((error) => {
        console.log('errororor');
        console.error(error);
      });
  };
  uploadImage = () => {
    const options = {
      title: 'Select Avatar',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        console.log(response);

        this.setState({
          image: response,
        });
      }
    });
  };
  saveData = () => {

    let data = {};
    if (this.state.image) {
      data = {
        name: this.state.name ? this.state.name : this.state.user.name,
        last_name: this.state.last_name
          ? this.state.last_name
          : this.state.user.last_name,
        phone: this.state.phone ? this.state.phone : this.state.user.phone,
        country: this.state.country
          ? this.state.country
          : this.state.user.country,
        zip_code: this.state.zip_code
          ? this.state.zip_code
          : this.state.user.zip_code,
        city: this.state.city ? this.state.city : this.state.user.city,
        address: this.state.location
          ? this.state.location
          : this.state.user.address,
        longitude: this.state.lat ? this.state.lat : this.state.lat,
        latitude: this.state.lng ? this.state.lng : this.state.lng,
        apartment: this.state.apartment
          ? this.state.apartment
          : this.state.user.apartment,
        image: 'data:image/png;base64,' + this.state.image.data,
      };
    } else {

      data = {
        name: this.state.name ? this.state.name : this.state.user.name,
        last_name: this.state.last_name
          ? this.state.last_name
          : this.state.user.last_name,
        phone: this.state.phone ? this.state.phone : this.state.user.phone,
        country: this.state.country
          ? this.state.country
          : this.state.user.country,
        zip_code: this.state.zip_code
          ? this.state.zip_code
          : this.state.user.zip_code,
        city: this.state.city ? this.state.city : this.state.user.city,
        address: this.state.location
          ? this.state.location
          : this.state.user.address,
        longitude: this.state.user.longitude,
        latitude: this.state.user.latitude,
        apartment: this.state.apartment
          ? this.state.apartment
          : this.state.user.apartment,
      };

      if ( data.phone === null || data.country === null || data.zip_code === null || data.city === null || data.longitude === null || data.latitude === null || data.apartment === null) {
        alert('please fill all fields')
        return;
      }

      alert('success')


    }
    console.log('data', data);
    this.setState({
      loader: true,
    });
    axios
      .post(`${config.apiUrl}/saveProfile`, data, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${this.state.token}`,
        },
      })
      .then((res) => {
        console.log(res);
        this.setState({
          loader: false,
        });
        this.props.navigation.goBack()
      })
      .catch((error) => {
        console.log('errororor');
        console.error(error);
      });
  };
  render() {
    return (
      <SafeAreaView style={styles.container}>
        {/* {this.getData} */}
        {this.state.loader ? (
          // <View
          //   style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          //   <ActivityIndicator
          //     size="large"
          //     color="#00ff00"
          //     animating={this.state.loader}
          //   />
          // </View>
          <CustomLoader />
        ) : (
            <ProfileScreen
              saveData={this.saveData}
              navigate={this.navigate}
              goBack={this.goBack}
              user={this.state.user}
              handleChange={this.handleChange}
              so={this.state.image}
              uploadImage={this.uploadImage}
              latlng={this.state}
              handlelatlng={this.handlelatlng}
              newlat={this.state.newlat}
              locationAddress={this.state.location}
              getLocation={this.getData}
            />
          )}
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
});


export default ProfileContainer;
