import React, {Component} from 'react';
import {SafeAreaView, View, ActivityIndicator} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {exp} from 'react-native/Libraries/Animated/src/Easing';
import {MyOrderScreen} from '../Screens';
import {redColor} from '../Theme/colors';
import axios from 'axios';
import Storage from '../Utils/storage';
import config from '../Utils/config';
import {CustomLoader} from '../Components';

class MyOrderContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orderlist: [],
      loader: false,
    };
  }

  async componentDidMount() {
    await Storage.retrieveData('user').then((resp) => {
      console.log(resp);
      this.getOrders(resp.token);
    });
  }
  navigate = async (routeName) => {
    const {navigation} = this.props;
    if (routeName === 'drawer') {
      await navigation.openDrawer();
    } else {
      await navigation.navigate(routeName);
    }
  };

  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  getOrders = async (token) => {
    this.setState({
      loader: true,
    });
    await axios
      .get(`${config.apiUrl}/orderlist`, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        // console.log('api responseeee', res.data.data);
        this.setState({
          orderlist: res.data.data,
          loader: false,
        });
      })
      .catch((error) => {
        console.log('errororor');
        console.error(error);
      });
  };
  render() {
    return (
      <SafeAreaView style={styles.container}>
        {this.state.loader ? (
          // <View
          //   style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          //   <ActivityIndicator
          //     size="large"
          //     color="#00ff00"
          //     animating={this.state.loader}
          //   />
          // </View>
          <CustomLoader loader={this.state.loader} />
        ) : (
          <MyOrderScreen
            goBack={this.goBack}
            navigate={this.navigate}
            orederList={this.state.orderlist}
          />
        )}
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
});

export default MyOrderContainer;
