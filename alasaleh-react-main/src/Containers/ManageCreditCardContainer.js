import React from 'react';
import {Component} from 'react';
import {SafeAreaView} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {ManageCreditCard} from '../Screens';
import {whiteColor} from '../Theme/colors';

class ManageCreditCardContainer extends Component {
  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  navigate = async (routeName, billingInfo) => {
    const {navigation} = this.props;
    if (routeName === 'drawer') {
      navigation.openDrawer();
    } else {
      await navigation.navigate(routeName, billingInfo);
    }
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <ManageCreditCard navigate={this.navigate} goBack={this.goBack} />
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: whiteColor,
  },
});

export default ManageCreditCardContainer;
