import React, {Component} from 'react';
import {SafeAreaView, ActivityIndicator, View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {MyAcountScreen} from '../Screens';
import {whiteColor} from '../Theme/colors';
import Storage from '../Utils/storage';
import config from '../Utils/config';
import axios from 'axios';
import {CustomLoader} from '../Components';
import Geocoder from 'react-native-geocoding';
import RNRestart from 'react-native-restart';
class MyAccountContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      name: '',
      email: '',
      loader: false,
    };
  }
  startReload = () => RNRestart.Restart();
  async componentDidMount() {
    await Storage.retrieveData('user').then((resp) => {
      // this.setState({
      //   userId:resp.user_id
      // })
      this.getProfile(resp);
      //this.updatePassword(resp.token)
      
    });
  }

  navigate = async (routeName) => {
    const {navigation} = this.props;
    if (routeName === 'drawer') {
      await navigation.openDrawer();
    } else {
      await navigation.navigate(routeName);
    }
  };

  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  logout = async () => {
    await Storage.removeData('user');
    await Storage.removeData('profile');
    await Storage.removeData('cartData');
    this.startReload();
  };

  getProfile = async (user) => {
    this.setState({
      loader: true,
    });
    let data = {
      id: user.user_id,
    };
    await axios
      .get(`${config.apiUrl}/user/${user.user_id}`, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + user.token,
          // Authorization: `bearer ${token}`,
        },
      })
      .then((res) => {
        this.setState({
          name: res.data.data.name,
          email: res.data.data.email,
          loader: false,
        });
      })
      .catch((error) => {
        console.error(error);
      });
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        {this.state.loader ? (
          // <View
          //   style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          //   <ActivityIndicator
          //     size="large"
          //     color="#00ff00"
          //     animating={this.state.loader}
          //   />
          // </View>
          <CustomLoader />
        ) : (
          <MyAcountScreen
            navigate={this.navigate}
            goBack={this.goBack}
            logout={this.logout}
            userData={this.state ? this.state : {}}
          />
        )}
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: whiteColor,
  },
});

export default MyAccountContainer;
