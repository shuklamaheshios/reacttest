import React, {Component} from 'react';
import {SafeAreaView, View, ActivityIndicator} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {BillingInvoiceScreen} from '../Screens';
import axios from 'axios';
import Storage from '../Utils/storage';
import config from '../Utils/config';
import {CustomLoader} from '../Components';
class BillingInvoiceContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: {},
      inovice: {},
      info: {},
      apikey: '',
      loader: false,
      skiping: '',
    };
  }
  async componentDidMount() {
    await Storage.retrieveData('skip').then((res) => {
      this.setState({
        skiping: res,
      });
    });
    console.log('par', this.props.route.params);
    // console.log('i m calling',this.props.route.params);
    await Storage.storeData('userinfo', this.props.route.params);
    //const {params} = this.props.route;

    // this.setState({
    //   apikey: params.tok.id,
    // });
    await Storage.retrieveData('billing').then((resp) => {
      this.setState({
        info: resp,
      });
    });

    await Storage.retrieveData('cartData').then((resp) => {
      this.setState({
        tokenuser: resp,
      });
      this.getInovice(resp);
    });

    await Storage.retrieveData('user').then((resp) => {
      this.setState({
        userdata: resp,
      });
    });

    // await Storage.retrieveData('user').then((resp) => {
    //   this.setState({
    //     user:resp
    //   })
    // })
  }
  payWithStripe = () => {
    this.setState({
      loader: true,
    });
    const params = {
      amount: this.state.inovice.total_amount,
      stripe_token: this.state.apikey,
    };
    axios
      .post(`${config.apiUrl}/pay_with_stripe`, params, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${this.state.userdata.token}`,
        },
      })
      .then((res) => {
        this.SaveOrder(res);
        this.setState({
          loader: false,
        });
      })
      .catch((error) => {
        console.error(error);
      });
  };
  getInovice = async (token) => {

    if (this.props.route.params.latitude == null || this.props.route.params.longitude == null){
      alert ('Please fill all of the required information')
      this.navigate('BillingInformation')
    }

    this.setState({
      loader: true,
    });
    let coupen = null;

    // if(this.props.route.params.skip)
    if (this.state.skiping == 'validCoupon') {
      await Storage.retrieveData('coupen').then((res) => {
        coupen = res.data[0].code;
      });

      let params;
      if (coupen) {
        params = {
          lat: this.props.route.params.latitude,
          long: this.props.route.params.longitude,
          cartKey: token.cartKey,
          user_id: this.props.route.params.id,
          coupon_code: coupen,
          // subtotal,
          state: this.props.route.params.state,
        };
      } else {
        params = {
          lat: this.props.route.params.latitude,
          long: this.props.route.params.longitude,
          cartKey: token.cartKey,
          user_id: this.props.route.params.user_id,
          // coupon_code:coupen,
          // subtotal,
          state: this.props.route.params.state,
        };
      }
      await axios
        .post(
          `${config.apiUrl}/carts/${token.cartToken}/shippingCalculation`,
          params,
        )
        .then((res) => {
          Storage.storeData('shipment', res.data.data);
          Storage.storeData('skip', '');
          Storage.storeData('invoice', res.data.data);
          this.setState({
            inovice: res.data.data,
            loader: false,
          });
        })
        .catch((error) => {
          console.error(error);
        });
    } else {
      let params;
      params = {
        lat: this.props.route.params.latitude,
        long: this.props.route.params.longitude,
        cartKey: token.cartKey,
        user_id: this.props.route.params.user_id,
        // coupon_code:coupen,
        // subtotal,
        state: this.props.route.params.state,
      };

      await axios
        .post(
          `${config.apiUrl}/carts/${token.cartToken}/shippingCalculation`,
          params,
        )
        .then((res) => {
          Storage.storeData('shipment', res.data.data);
          Storage.storeData('skip', '');
          Storage.storeData('invoice', res.data.data);
          this.setState({
            inovice: res.data.data,
            loader: false,
          });
        })
        .catch((error) => {
          console.error(error);
        });
    }
  };

  SaveOrder = async (cnum) => {
    this.setState({
      loader: true,
    });
    const {tokenuser, info} = this.state;
    const params = {
      cart_token: tokenuser.cartToken,
      cartKey: tokenuser.cartKey,
      email: info.billingInfo.email,
      address: info.billingInfo.address,
      city: info.billingInfo.city,
      state: info.billingInfo.state ? info.billingInfo.state : 'state',
      postal_code: info.billingInfo.zip_code,
      // "apartment":info.billingInfo.apartment,
      lat: parseFloat(info.billingInfo.latitude),
      long: parseFloat(info.billingInfo.longitude),
      transaction_id: this.state.apikey,
      // "coupon_code":info.billingInfo.coupenCode,
      save_account_info: '0',
      // "credit_card_no":cnum.cardNumber,
      // "card_type":"credit",
      // "card_exp":"06/2021"
      payment_method: 'stripe',
    };






    await axios
      .post(`${config.apiUrl}/save_order_with_paypal`, params, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${this.state.userdata.token}`,
        },
      })
      .then((res) => {
        this.setState({
          saved: res.data.data,
          loader: false,
        });
        alert('Your Order Has Been Placed');
        Storage.removeData('cartData');
        Storage.removeData('coupen');
        Storage.removeData('cartDataDetail');
        this.navigate('Home');
      })
      .catch((error) => {
        console.error(error);
      });
  };
  navigate = async (routeName) => {
    const {navigation} = this.props;
    if (routeName === 'drawer') {
      navigation.openDrawer();
    } else {
      await navigation.navigate(routeName);
    }
  };

  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };
  render() {
    return (
      <SafeAreaView style={styles.contaienr}>
        {this.state.loader ? (
          // <View
          //   style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          //   <ActivityIndicator
          //     size="large"
          //     color="#00ff00"
          //     animating={this.state.loader}
          //   />
          // </View>
          <CustomLoader />
        ) : (
          <BillingInvoiceScreen
            navigate={this.navigate}
            goBack={this.goBack}
            inovice={this.state.inovice ? this.state.inovice : {}}
            SaveOrder={this.payWithStripe}
          />
        )}
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  contaienr: {
    flex: 1,
  },
});

export default BillingInvoiceContainer;
