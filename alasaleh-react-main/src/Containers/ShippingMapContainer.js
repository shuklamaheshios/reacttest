import React, {Component} from 'react';
import {SafeAreaView} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {MapViewScreen, ShippingMapScreen} from '../Screens';

class ShippingMapContainer extends Component {
  constructor(props) {
    super(props);
  }
  state = {
    coordinates: [
      {
        name: '1',
        latitude: 31.4697,
        longitude: 74.2728,
      },
      {
        name: '2',
        latitude: 31.5204,
        longitude: 74.3587,
      },
      {
        name: '3',
        latitude: 31.5204,
        longitude: 74.3587,
      },
    ],

    mapRegion: {
      lat: 31.5204,
      lng: 74.3587,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    },
    latitude: 0,
    longitude: 0,

    marginBootom: 1,
  };

  componentDidMount() {
    console.log('params are', this.props.route.params);
  }

  handleRegionChange = (mapRegion) => {
    this.setState({mapRegion});
  };

  navigate = async (routeName, param) => {
    const {navigation} = this.props;
    if (routeName === 'drawer') {
      await navigation.openDrawer();
    } else {
      await navigation.navigate(routeName, param);
    }
  };

  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  onRegionChange = (lat, lng) => {
    this.setState({
      latitude: lat,
      longitude: lng,
    });
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <ShippingMapScreen
          navigate={this.navigate}
          goBack={this.goBack}
          coordinates={this.state.coordinates}
          marginBootom={this.state.marginBootom}
          handleRegionChange={this.handleRegionChange}
          handleChange={this.props.route.params.handlelatlng}
          getLocation={this.props.route.params.getLocation}
        />
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
});

export default ShippingMapContainer;
