import React, { Component } from 'react';
import { SafeAreaView, View, ActivityIndicator } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import ReferelPointsScreen from '../Screens/ReferelPointsScreen';
import axios from 'axios';
import Storage from '../Utils/storage';
import { CustomLoader } from '../Components';
import config from '../Utils/config';
class ReferelPointsContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: false,
      amount: '',
      points: '',
      refferd: '',
    };
  }
  async componentDidMount() {
    await Storage.retrieveData('user').then((resp) => {
      this.setState({
        token: resp.token,
        loader: false,
      });
      this.getReferPoint(resp);
      this.getProfile(resp);
    });
  }
  navigate = async (routeName) => {
    const { navigation } = this.props;
    if (routeName === 'drawer') {
      await navigation.openDrawer();
    } else {
      await navigation.navigate(routeName);
    }
  };

  goBack = () => {
    const { navigation } = this.props;
    navigation.goBack();
  };

  getProfile = async (user) => {
    this.setState({
      loader: true,
    });
    let data = {
      id: user.user_id,
    };
    axios
      .get(`${config.apiUrl}/user/${user.user_id}`, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${user.token}`,
        },
      })
      .then((res) => {
        console.log(res.data);
        this.setState({
          amount: res.data.data.points_amount,
          points: res.data.data.total_points,
          refferd: res.data.data.referrad_amount,
          loader: false,
        });
      })
      .catch((error) => {
        console.log('errororor');
        console.error(error);
      });
  };

  getReferPoint = (user) => {
    // this.setState({
    //   loader: true,
    // });
    axios
      .get(
        `${config.apiUrl}/points-and-referral-amount`,
        {
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${user.token}`,
          },
        },
      )
      .then((res) => {
        console.log('refer', res.data.data.points);
        this.setState({ infoData: res.data.data.points });
      })
      .catch((error) => {
        console.log('errororor');
        console.error(error);
      });
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        {this.state.loader ? (
          // <View
          //   style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          //   <ActivityIndicator
          //     size="large"
          //     color="#00ff00"
          //     animating={this.state.loader}
          //   />
          // </View>
          <CustomLoader />
        ) : (
            <ReferelPointsScreen
              navigate={this.navigate}
              goBack={this.goBack}
              referpoints={this.state ? this.state : [{}]}
            />
          )}
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
});

export default ReferelPointsContainer;
