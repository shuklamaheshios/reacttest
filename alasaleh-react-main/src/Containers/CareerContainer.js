import React, {Component} from 'react';
import {SafeAreaView} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {CareerScreen} from '../Screens';
import {whiteColor} from '../Theme/colors';
import config from '../Utils/config';
class CareerContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
    };
  }
  componentDidMount() {
    this.getdetail();
  }
  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };
  navigate = async (routeName, billingInfo) => {
    const {navigation} = this.props;
    if (routeName === 'drawer') {
      navigation.openDrawer();
    } else {
      await navigation.navigate(routeName, billingInfo);
    }
  };
  getdetail = async () => {
    await fetch(`${config.apiUrl}/static_pages/careers`)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          data: responseJson.data,
        });
      })
      .catch((error) => {
        console.error(error);
      });
  };
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <CareerScreen
          navigate={this.navigate}
          goBack={this.goBack}
          data={this.state.data}
        />
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: whiteColor,
  },
});

export default CareerContainer;
