import React, {Component} from 'react';
import {SafeAreaView, View, ActivityIndicator} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import Toast from 'react-native-simple-toast';
import {CreditCardScreen} from '../Screens';
import Stripe from 'react-native-stripe-api';
import axios from 'axios';
import Storage from '../Utils/storage';
import config from '../Utils/config';
import {CustomLoader} from '../Components';
class CreditCardContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cardNumber: '',
      cvc: '',
      expM: '',
      expY: '',
      postal: '',
      apiKey: '',
      tok: '',
      userto: '',
      loader: false,
    };
  }

  async componentDidMount() {
    const {route} = this.props;
    // this.setState({
    //   apiKey: route.params.cardInfo.key,
    // });

    Storage.retrieveData('Stripe').then((resp) => {
      this.setState({
        apiKey: resp.key,
      });
    });

    Storage.retrieveData('cartData').then((res) => {
      this.setState({
        cart: res,
      });
    });

    Storage.retrieveData('user').then((resp) => {
      this.setState({
        userto: resp.token,
      });
    });

    Storage.retrieveData('userinfo').then((resp) => {
      this.setState({
        userinfo: resp,
      });
    });
  }

  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  navigate = async (routeName, details) => {
    const {navigation} = this.props;
    if (routeName === 'drawer') {
      await navigation.openDrawer();
    } else {
      await navigation.navigate(routeName, details);
    }
  };

  genToken = async () => {
    const apiKey = this.state.apiKey;
    const client = new Stripe(apiKey);
    const token = await client.createToken({
      number: this.state.cardNumber,
      exp_month: this.state.expM,
      exp_year: this.state.expY,
      cvc: this.state.cvc,
      // address_zip: this.state.postal,
    });
    this.setState({
      tok: token,
    });
    if (token) {
      token.error ? console.log('invalid') : this.payWithStripe(token);
    }
  };

  payWithStripe = async (token) => {
    this.setState({
      loader: true,
    });
    let price = 0;
    await Storage.retrieveData('price').then((res) => {
      price = res;
    });

    const params = {
      amount: price,
      stripe_token: token.id,
    };
    axios
      .post(`${config.apiUrl}/pay_with_stripe`, params, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${this.state.userto}`,
        },
      })
      .then((res) => {
        this.setState({
          loader: false,
        });
        this.SaveOrder();
        Toast.show(res.message);
      })
      .catch((error) => {
        console.error(error);
        this.setState({
          loader: false,
        });
        Toast.show(error.message);
      });
  };
  handleChange = (name, value) => {
    this.setState({
      [name]: value,
    });
  };

  SaveOrder = async () => {
    this.setState({
      loader: true,
    });
    // console.log(this.state.cart,this.state.userinfo,this.state.apiKey,)
    const params = {
      cart_token: this.state.cart.cartToken,
      cartKey: this.state.cart.cartKey,
      email: this.state.userinfo.email,
      address: this.state.userinfo.address,
      city: this.state.userinfo.city,
      state: this.state.userinfo.state ? this.state.userinfo.state : 'state',
      postal_code: this.state.userinfo.zip_code,
      // "apartment":info.billingInfo.apartment,
      lat: parseFloat(this.state.userinfo.latitude),
      long: parseFloat(this.state.userinfo.longitude),
      transaction_id: this.state.apiKey,
      // "coupon_code":info.billingInfo.coupenCode,
      save_account_info: '0',
      // "credit_card_no":cnum.cardNumber,
      // "card_type":"credit",
      // "card_exp":"06/2021"
      payment_method: 'points',
    };

    await axios
      .post(`${config.apiUrl}/save_order_with_paypal`, params, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${this.state.userto}`,
        },
      })
      .then((res) => {
        this.setState({
          saved: res.data.data,
          loader: false,
        });
        Storage.removeData('cartData');
        this.navigate('Home');
      })
      .catch((error) => {
        this.setState({
          loader: false,
        });
        console.error(error);
      });
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        {/* {this.state.loader ? (
          // <View
          //   style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          //   <ActivityIndicator
          //     size="large"
          //     color="#00ff00"
          //     animating={this.state.loader}
          //   />
          // </View>
        
          <CustomLoader />
        ) : ( */}
        <CreditCardScreen
          navigate={this.navigate}
          handleChange={this.handleChange}
          genToken={this.genToken}
          details={this.state}
          goBack={this.goBack}
          SaveOrder={this.SaveOrder}
        />
        {/* )} */}
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
});

export default CreditCardContainer;
