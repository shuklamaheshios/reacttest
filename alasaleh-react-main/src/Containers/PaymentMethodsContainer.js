import React from 'react';
import {Component} from 'react';
import {SafeAreaView, Button, ActivityIndicator, View} from 'react-native';
import {CustomLoader} from '../Components';
import {ScaledSheet} from 'react-native-size-matters';
import {PaymentMethodScreen} from '../Screens';
import {whiteColor} from '../Theme/colors';
import axios from 'axios';
import Storage from '../Utils/storage';
import config from '../Utils/config';

//  import stripe from 'react-native-stripe-payments';
class PaymentMethodsContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: '',
      card: {},
      billingInfo: {},
      points: false,
      points_amount: 0,
      userinfo: {},
      // cart,
      // userto,
    };
  }
  async componentDidMount() {
    const {params} = this.props.route;
    console.log('paramsss are', params);
    this.setState({
      billingInfo: params,
    });
    await Storage.retrieveData('user').then((resp) => {
      this.setState({
        token: resp.token,
      });
      this.getProfile(resp);
      this.getPaypal();
    });
    await Storage.retrieveData('userinfo').then((resp) => {
      console.log('ressss', resp);
      this.setState({
        userinfo: resp,
      });
    });
    await Storage.retrieveData('cartData').then((res) => {
      console.log('cart', res);
      this.setState({
        cart: res,
      });
    });

    await Storage.retrieveData('user').then((resp) => {
      this.setState({
        userto: resp.token,
      });
    });
  }
  navigate = async (routeName, billingInfo) => {
    const {navigation} = this.props;
    if (routeName === 'drawer') {
      await navigation.openDrawer();
    } else {
      await navigation.navigate(routeName, billingInfo);
    }
  };

  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  changeRadio = (type) => {
    if (type === 'paypal') {
      this.getPaypal();
    } else {
      this.getCreditCard();
    }
  };

  getProfile = async (user) => {
    let data = {
      id: user.user_id,
    };
    axios
      .get(`${config.apiUrl}/user/${user.user_id}`, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${user.token}`,
        },
      })
      .then((res) => {
        console.log('user info is', res.data.data);
        if (
          parseFloat(res.data.data.total_points) >= 20.0 &&
          parseFloat(res.data.data.points_amount) >= 20
        ) {
          this.setState({
            points: true,
            points_amount: res.data.points_amount,
          });
        }
      })
      .catch((error) => {
        console.log('errororor');
        console.error(error);
      });
  };

  getCreditCard = async () => {
    fetch(`${config.apiUrl}/get_stripe_credentials`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${this.state.token}`,
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        Storage.storeData('Stripe', responseJson.message);
        
        this.setState({
          cardInfo: responseJson.message,
        });
      });
  };

  getPaypal = async () => {
    fetch(`${config.apiUrl}/get_paypal_credentials`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${this.state.token}`,
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('paypal card pay', responseJson);
        Storage.storeData('Paypal', responseJson.message);
        this.setState({
          cardInfo: responseJson.message,
        });
      });
  };

  // setStripe =() =>{
  //   console.log(this.state.cardInfo)
  //   stripe.setOptions({ publishingKey: this.state.cardInfo.key });
  //   const cardDetails = {
  //     number: '4242424242424242',
  //     expMonth: 10,
  //     expYear: 21,
  //     cvc: '888',
  //   }
  //   stripe.confirmPayment(this.state.cardInfo.secret, cardDetails)
  //     .then(result => {
  //       console.log(result)
  //     })
  //     .catch(err =>
  //       console.log(err))
  // }

  SaveOrder = async (ar) => {
    
    const { navigation } = this.props;
    if ( this.state.userinfo.email === null || this.state.userinfo.address === null || this.state.userinfo.city === null || this.state.userinfo.zip_code === null) {
      alert('please fill all your billing information inside profile');
      navigation.navigate('Profile')

      return;
    }

    this.setState({
      loader: true,
    });
    // console.log(this.state.cart,this.state.userinfo,this.state.apiKey,)
    const params = {
      cart_token: this.state.cart.cartToken,
      cartKey: this.state.cart.cartKey,
      email: this.state.userinfo.email,
      address: this.state.userinfo.address,
      city: this.state.userinfo.city,
      state: this.state.userinfo.state ? this.state.userinfo.state : 'state',
      postal_code: this.state.userinfo.zip_code,
      // "apartment":info.billingInfo.apartment,
      lat: parseFloat(this.state.userinfo.latitude),
      long: parseFloat(this.state.userinfo.longitude),
      transaction_id: Date.now(),
      // "coupon_code":info.billingInfo.coupenCode,
      save_account_info: '0',
      // "credit_card_no":cnum.cardNumber,
      // "card_type":"credit",
      // "card_exp":"06/2021"
      payment_method: 'point',
    };
    await axios
      .post(`${config.apiUrl}/save_order_with_paypal`, params, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${this.state.userto}`,
        },
      })
      .then((res) => {
        this.setState({
          saved: res.data.data,
          loader: false,
        });
        if (ar) {
          alert('وقد وضعت طلبك');
        } else {
          alert('Your Order Has Been Placed');
        }
        console.log('saved');
        Storage.removeData('cartData');
        Storage.removeData('cartDataDetails');
        this.navigate('Home', []);
      })
      .catch((error) => {
        this.setState({
          loader: false,
        });
        console.log('errororor');
        console.error(error);
      });
  };

  render() {
    return (
      <SafeAreaView style={styles.safeAreaContainer}>
        {this.state.loader ? (
          <CustomLoader />
        ) : (
        <PaymentMethodScreen
          changeRadio={this.changeRadio}
          navigate={this.navigate}
          goBack={this.goBack}
          getCreditCard={this.getCreditCard}
          getPaypal={this.getPaypal}
          details={this.state ? this.state : [{}]}
          checkPoints={this.state.points}
          SaveOrder={this.SaveOrder}
        />
      )}
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: whiteColor,
  },
  safeAreaContainer: {
    flex: 1,
    backgroundColor: whiteColor,
  }
});

export default PaymentMethodsContainer;
