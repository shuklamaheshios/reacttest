import React, {Component} from 'react';
import {Text, SafeAreaView} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {ForgotPasswordScreen} from '../Screens';
import {whiteColor} from '../Theme/colors';
import axios from 'axios';
import config from '../Utils/config';

class ForgotPasswordContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
    };
  }
  navigate = async (routeName) => {
    const {navigation} = this.props;
    if (routeName === 'drawer') {
      await navigation.openDrawer();
    } else {
      await navigation.navigate(routeName);
    }
  };

  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  handleChange = (name, value) => {
    this.setState({
      [name]: value,
    });
  };

  submit = () => {
    const params = {
      email: this.state.email,
    };
    axios
      .post(`${config.apiUrl}/forgot_password`, params)
      .then((res) => {
        
        alert(res.data.message);
        this.setState({
          loader: false,
        });
      })
      .catch((error) => {
        console.error(error);
      });
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <ForgotPasswordScreen
          goBack={this.goBack}
          navigate={this.navigate}
          handleChange={this.handleChange}
          submit={this.submit}
        />
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: whiteColor,
  },
});

export default ForgotPasswordContainer;
