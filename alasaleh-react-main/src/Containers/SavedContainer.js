import React from 'react';
import {Component} from 'react';
import {SafeAreaView} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {SavedScreen} from '../Screens';
import MyCart from '../Screens/MyCart';

class SavedContainer extends Component {
  navigate = async (routeName) => {
    const {navigation} = this.props;
    if (routeName === 'drawer') {
      await navigation.openDrawer();
    } else {
      await navigation.navigate(routeName);
    }
  };

  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <SavedScreen navigate={this.navigate} goBack={this.goBack} />
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
});

export default SavedContainer;
