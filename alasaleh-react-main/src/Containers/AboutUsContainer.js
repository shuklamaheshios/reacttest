import React, { Component } from 'react';
import { SafeAreaView } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import { AboutUsScreen } from '../Screens';
import { whiteColor } from '../Theme/colors';
import { CustomLoader } from '../Components';
import config from '../Utils/config';
class AboutUsContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: false,
      aboutData: ''
    };
  }
  componentDidMount = () => {
    console.log("LUN LORA:   ((((**##^^#&*#&#&#*")

    this.getAboutData()
  }
  getAboutData = async () => {
    this.setState({ loader: true })
    await fetch(`${config.apiUrl}/static_pages/about-us`)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          aboutData: responseJson.data,
          loader: false,
        });

      })
      .catch((error) => {
        console.error(error);
      });
  };
  goBack = () => {
    const { navigation } = this.props;
    navigation.goBack();
  };

  navigate = async (routeName, billingInfo) => {
    const { navigation } = this.props;
    if (routeName === 'drawer') {
      navigation.openDrawer();
    } else {
      await navigation.navigate(routeName, billingInfo);
    }
  };
  render() {
    return (
      <SafeAreaView style={styles.container}>
        {
          this.state.loader ?

            <CustomLoader /> :

            <AboutUsScreen navigate={this.navigate} goBack={this.goBack} aboutData={this.state.aboutData} />
        }
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: whiteColor,
  },
});

export default AboutUsContainer;
