const helpers = {
    helper1: function(){
        
            alert('test')
            let pr = 0;
            // console.log('item', item);
             Storage.retrieveData('cartData').then((resp) => {
              token = resp;
            });
             Storage.retrieveData('user').then((resp) => {
              console.log('user', resp);
              user = resp;
            });
        
            if (token === null) {
               fetch(`${config.apiUrl}/carts`, {
                method: 'POST',
                headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
                },
              })
                .then((response) => response.json())
                .then((responseJson) => {
                  Storage.storeData('cartData', responseJson);
                  token = responseJson;
                });
            }
            if (item.promotion === '1' || item.promotion === 1) {
              pr = parseFloat(item.promotion_price).toFixed(2);
            } else {
              const k = (parseFloat(item.price) * parseFloat(item.discount)) / 100;
              const calculated = item.price - k;
              let p = calculated.toFixed(2);
              if (p > calculated) {
                p = p - 0.01;
              }
              pr = p;
            }
            // let cartDet = null;
            // await Storage.retrieveData('cartDataDetails').then((resp) => {
            //   console.log('new cart:', resp);
            //   cartDet = resp;
            // });
            // if (cartDet) {
            //   item = {
            //     ...item,
            //     price: pr,
            //   };
        
            //   cartDet.push(item);
            //   console.log('adding cart:', cartDet);
            //   Storage.storeData('cartDataDetails', cartDet);
            // } else {
            //   item = {
            //     ...item,
            //     price: pr,
            //   };
            //   Storage.storeData('cartDataDetails', [item]);
            // }
        
            let toCount = 0;
            
             fetch(
              `${config.apiUrl}/carts/show_cart_data/${token.cartToken}`,
              {
                method: 'POST',
                headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                  cartKey: token.cartKey,
                }),
              },
            )
              .then((response) => response.json())
              .then((responseJson) => {
                responseJson.item_in_cart.map((cartItem) => {
                  if (cartItem.productID === item.id) {
                    toCount = parseInt(cartItem.Quantity);
                    console.log('toCount.id', cartItem?.Quantity);
                  }
                });
        
                console.log('cart', responseJson);
                // responseJson['item_in_cart'].map(item => this.getProducts(item.Name))
              })
              .catch((e) => {
                console.log('error is', e);
                // this.setState({
                //   loader: false,
                // });
              });
            let qti = 1;
            if (toCount !== 0) {
              qti = toCount + 1;
              item.Quantity = qti;
            }
            if (item.enable_free_product_option === 1) {
              // item.Quantity = parseInt(item.Quantity) + 1;
              // if (!item.Qun) {
              //   console.log('item.Qun', item.Qun);
              //   item.Qun = 0;
              // }
              item.Qun = item.Qun + 1;
              item.dq = item.dq + 1;
        
              if (
                item.Quantity === item.no_of_products_buy ||
                item.Qun === item.no_of_products_buy
              ) {
                item.Qun = 0;
        
                item.Quantity = parseInt(item.Quantity) + item.no_of_free_products;
                qti = item.Quantity;
              }
              // else if (item.Quantity % item.no_of_products_buy !== 0) {
              //   item.Quantity = parseInt(item.Quantity) + 1;
              // }
            }
             fetch(
              `${config.apiUrl}/carts/add_to_cart/${token.cartToken}`,
              {
                method: 'POST',
                headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                  cartKey: token.cartKey,
                  productID: item.id,
                  quantity: qti.toString(),
                  price: pr,
                  image: item.image,
                }),
              },
            )
              .then((response) => response.json())
              .then((res) => {
                Toast.show(res.message);
                update();
                // forceUpdate();
              });
    },
    helper2: function(param1){

    },
    helper3: function(param1, param2){

    }
}

export default helpers;