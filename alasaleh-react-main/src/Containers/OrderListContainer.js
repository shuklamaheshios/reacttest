import React, {Component} from 'react';
import {SafeAreaView} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {OrderListScreen} from '../Screens';
import {whiteColor} from '../Theme/colors';

class OrderListContainer extends Component {
  navigate = async (routeName) => {
    const {navigation} = this.props;
    if (routeName === 'drawer') {
      await navigation.openDrawer();
    } else {
      await navigation.navigate(routeName);
    }
  };

  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <OrderListScreen navigate={this.navigate} goBack={this.goBack} />
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: whiteColor,
  },
});

export default OrderListContainer;
