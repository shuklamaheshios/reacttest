import React, {Component} from 'react';
import {SafeAreaView, ActivityIndicator, View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import Toast from 'react-native-simple-toast';
import {UpdatePasswordScreen} from '../Screens';
import {whiteColor} from '../Theme/colors';
import axios from 'axios';
import Storage from '../Utils/storage';
import {CustomLoader} from '../Components';
import config from '../Utils/config';

class UpdatePasswordContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      old: '',
      new: '',
      confirm: '',
      token: '',
      loader: false,
    };
  }
  async componentDidMount() {
    await Storage.retrieveData('user').then((resp) => {
      this.setState({
        token: resp.token,
      });
    });
  }

  handleChange = (name, value) => {
    this.setState({
      [name]: value,
    });
  };

  updatePassword = async () => {
    this.setState({
      loader: true,
    });
    const params = {
      old_password: this.state.old,
      new_password: this.state.new,
      confirm_password: this.state.confirm,
    };
    if (
      this.state.old.length === 0 ||
      this.state.new.length === 0 ||
      this.state.confirm.length === 0
    ) {
      this.setState({
        loader: false,
      });
      Toast.show('Please Fill all the fields', Toast.LONG);
    } else {
      axios
        .post(`${config.apiUrl}/change_password`, params, {
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${this.state.token}`,
          },
        })
        .then((res) => {
          console.log('out cond', res);
          this.setState({
            loader: false,
          });
          Toast.show(res.data.message, Toast.LONG);
          if (res.data.status === 200) {
            this.navigate('MyAccount');
            // this.setState({
            //   loader: false,
            // });
          }
        })
        .catch((error) => {
          this.setState({
            loader: false,
          });
          Toast.show(error.message, Toast.LONG);
        });
    }
  };
  navigate = async (routeName) => {
    const {navigation} = this.props;
    if (routeName === 'drawer') {
      await navigation.openDrawer();
    } else {
      await navigation.navigate(routeName);
    }
  };

  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        {this.state.loader ? (
          // <View
          //   style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          //   <ActivityIndicator
          //     size="large"
          //     color="#00ff00"
          //     animating={this.state.loader}
          //   />
          // </View>
          <CustomLoader />
        ) : (
          <UpdatePasswordScreen
            navigate={this.navigate}
            goBack={this.goBack}
            handleChange={this.handleChange}
            updatePassword={this.updatePassword}
          />
        )}
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: whiteColor,
  },
});

export default UpdatePasswordContainer;
