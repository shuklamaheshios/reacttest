import React, {Component} from 'react';
import {SafeAreaView} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {SplashScreen} from '../Screens';

class SplashContainer extends Component {
  constructor(props) {
    super(props);
  }

  navigate = async (routeName) => {
    const {navigation} = this.props;
    if (routeName === 'drawer') {
      await navigation.openDrawer();
    } else {
      await navigation.navigate(routeName);
    }
  };
  componentDidMount() {
    setTimeout(() => {
      this.navigate('Home');
    }, 3000);
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <SplashScreen />
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
});

export default SplashContainer;
