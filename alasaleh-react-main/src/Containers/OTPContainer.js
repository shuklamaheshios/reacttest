import React, {Component} from 'react';
import {SafeAreaView, ActivityIndicator, View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {CustomLoader} from '../Components';
import {OTPScreen} from '../Screens';
import {redColor, whiteColor} from '../Theme/colors';
import config from '../Utils/config';

class OTPContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otp: 0,
      loader: false,
    };
  }

  componentDidMount() {
    const {params} = this.props.route;
  }

  navigate = async (routeName) => {
    const {navigation} = this.props;
    if (routeName === 'drawer') {
      await navigation.openDrawer();
    } else {
      await navigation.navigate(routeName);
    }
  };

  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  handleChange = (name, value) => {
    this.setState({
      [name]: value,
    });
    console.log(name, value);
  };

  signIn = async () => {
    this.setState({
      loader: true,
    });
    const {params} = this.props.route;
    console.log(`${config.apiUrl}/register`,this.state.otp, params, params.otp === this.state.otp)
    if (params.otp == this.state.otp)
      await fetch(`${config.apiUrl}/register`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          name: params.name,
          last_name: params.last_name,
          email: params.email,
          password: params.password,
          c_password: params.password,
        })
      })
        .then((response) => {
          console.log(response)
          return response.json()})
        .then((responseJson) => {
          this.props.navigation.goBack()

          console.log('resp', responseJson);
          // SyncStorage.set('user', responseJson.data);
          if (responseJson.status) {
            this.navigate('Home');
            this.setState({
              loader: false,
            });
          } else console.log('invalid credentials');
        }).catch((err) => {
          console.log('error while registering.. ', err)
        });
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        {this.state.loader ? (
          // <View
          //   style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          //   <ActivityIndicator
          //     size="large"
          //     color="#00ff00"
          //     animating={this.state.loader}
          //   />
          // </View>
          <CustomLoader />
        ) : (
          <OTPScreen
            navigate={this.navigate}
            goBack={this.goBack}
            handleChange={this.handleChange}
            signIn={this.signIn}
          />
        )}
      </SafeAreaView>
    );
  }
}

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: whiteColor,
  },
});

export default OTPContainer;
